"use client"
import React from 'react'
import { Backdrop, CircularProgress } from '@mui/material';

export function PageLoader() {
    var [open, setOpen] = React.useState(true);
    return (
    <div>
        <Backdrop
        sx={{ color: '#fff', zIndex: (theme) => theme.zIndex.drawer + 1 }}
        open={open}
        >
        <CircularProgress color="inherit" />
        </Backdrop>
    </div>
    );
}