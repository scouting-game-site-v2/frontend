"use client"
import React from 'react';
import { ThemeProvider } from '@mui/material/styles';
import { pageTheme } from "@/app/theme";
import Container from "@mui/material/Container";
import Box from "@mui/material/Box";
import NavBar from './navbar';
import CssBaseline from '@mui/material/CssBaseline';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import * as notify from '@/app/notify';
import { useSearchParams } from 'next/navigation'
import Icon from '@mui/material/Icon';
import { QueryNotifications } from '@/tools/notification';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardActions from '@mui/material/CardActions';
import { CardHeader, Divider } from '@mui/material';

const LazyFooter = React.lazy(() => import("@/app/footer"));

export default function App() {
    // show query notifications
    const searchParams = useSearchParams();
    React.useEffect(() => {
        QueryNotifications(searchParams);
    }, [searchParams]);

    const [token, setToken] = React.useState("");

    const isLocalStorageDefined = typeof localStorage !== "undefined";

    React.useEffect(() => {
        if (isLocalStorageDefined){
            setToken(localStorage.getItem("jwt_token") || "");
        }
    }, [isLocalStorageDefined]);
    
    return(
        <>   
        <ThemeProvider theme={pageTheme}>
        <CssBaseline>
            <NavBar/>
            <Container maxWidth="xl" sx={{minHeight: "calc(100vh - 5rem)"}}>
                <Box sx={{ my: 4, display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
                    <Typography variant="h4" component="h1" gutterBottom>
                        Welcome to the online Scouting Game website!
                    </Typography>

                    <Box maxWidth="sm">
                        <Card>
                            <CardContent>
                                <Typography component="h2" gutterBottom>
                                    The Scouting Game is an online game that is heavily under development. With the current game, you can claim locations. Want to play?
                                </Typography>
                            </CardContent>
                            <CardActions sx={{justifyContent: 'space-between'}}>
                                    <Button variant="contained" color="primary" onClick={() => window.location.href = '/game/join'}>
                                        Join a game
                                    </Button>
                                    {token !== "" && <Button variant="outlined" color="primary" onClick={() => window.location.href = '/game'}>
                                        Create a game
                                    </Button>}
                            </CardActions>
                        </Card>
                        
                        <br/>

                        <Card>
                            <CardHeader title="About"/>
                            <Divider/>
                            <CardContent>
                                <Typography component="h2" gutterBottom>
                                    Want to know more about this game website?<br/>Check out the about page.
                                </Typography>
                            </CardContent>
                            <CardActions>
                                <Button variant="contained" color="secondary" onClick={() => window.location.href = '/about'}>
                                    About
                                </Button>
                            </CardActions>
                        </Card>
                    </Box>
                </Box>
            </Container>
            <Box sx={{ mt: 'auto' }}>
                <LazyFooter />
            </Box>
            </CssBaseline>
        </ThemeProvider>
        </>
    )
}