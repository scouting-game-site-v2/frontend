import React from 'react';
import { ThemeProvider } from '@mui/material/styles';
import { pageTheme } from "@/app/theme";
import Container from "@mui/material/Container";
import NavBar from '@/app/navbar';
import CssBaseline from '@mui/material/CssBaseline';
import Typography from '@mui/material/Typography';
import { Box, Card, CardActions, CardContent, CardHeader, Divider, Link } from '@mui/material';
import Button from '@mui/material/Button';

const LazyFooter = React.lazy(() => import("@/app/footer"));

export default function App()
{
    return(
        <>   
        <ThemeProvider theme={pageTheme}>
            <CssBaseline>
            <NavBar/>
            <Container maxWidth="sm" sx={{minHeight: 'calc(100vh - 5rem)'}}>
                <Card>
                    <CardHeader title="About this website" />
                    <Divider />
                    <CardContent>
                        <Typography variant="body1" sx={{ marginBottom: '1rem' }}>
                            This website is a personal project developed in my spare time. 
                            It was created to help organize and manage games that I host, while also allowing me to participate and control the game simultaneously.
                        </Typography>
                        <Typography variant="body1" sx={{ marginBottom: '1rem' }}>
                            As a passionate scout, I dedicate a significant amount of time to scouting activities, which limits the time I can spend on maintaining this site.
                        </Typography>
                        <Typography variant="body1" sx={{ marginBottom: '1rem' }}>
                            Currently, the website is under active development. I am continuously working on adding new features, improving the user experience, and ensuring the site remains up-to-date with the latest technologies. 
                            For any inquiries, suggestions, or feedback, please reach out to me at <Link href="mailto:info@tommydewever.nl" underline="hover" color='secondary'>info@tommydewever.nl</Link>. 
                            Your input is highly appreciated and will help me improve the website further.
                        </Typography>
                        <Typography variant="body1" sx={{ marginBottom: '1rem' }}>
                            If you have any questions or game ideas, feel free to contact me. I am always open to new ideas and suggestions.
                        </Typography>
                        <Typography variant="body1" sx={{ marginBottom: '1rem' }}>
                            Thank you for visiting my website!
                        </Typography>
                        <Typography variant="body1" sx={{ marginBottom: '1rem' }}> 
                            More information about me can be found on my personal website
                        </Typography>
                    </CardContent>
                    <CardActions>
                        <Box display="flex" justifyContent="space-between" width="100%" px={2}>
                            <Button size="small" color="primary" href="mailto:info@tommydewever.nl">Contact me</Button>
                            <Button size="small" color="secondary" href="https://info.tommydewever.nl">Personal website</Button>
                            <Button size="small" color="primary" href="/">Home</Button>                  
                        </Box>
                    </CardActions>
                </Card>
            </Container>
            <Box sx={{ mt: 'auto' }}>
                <LazyFooter />
            </Box>
            </CssBaseline>
        </ThemeProvider>
        </>
    )
}