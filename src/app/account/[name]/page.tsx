"use client"
import * as React from 'react';
import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import CssBaseline from '@mui/material/CssBaseline';
import TextField from '@mui/material/TextField';
import Box from '@mui/material/Box';
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import { ThemeProvider } from '@mui/material/styles';
import { pageTheme } from '@/app/theme';
import NavBar from '@/app/navbar';
import { updateMember, getMember, deleteMember } from '@/tools/api/memberAPI';
import * as notify from '@/app/notify';
import Checkbox from '@mui/material/Checkbox';
import { PageLoader } from '@/app/loader';
import IconButton from '@mui/material/IconButton';
import InputAdornment from '@mui/material/InputAdornment';
import Visibility from '@mui/icons-material/Visibility';
import VisibilityOff from '@mui/icons-material/VisibilityOff';
import OutlinedInput from '@mui/material/OutlinedInput';
import InputLabel from '@mui/material/InputLabel';
import FormControl from '@mui/material/FormControl';
import Accordion from '@mui/material/Accordion';
import AccordionSummary from '@mui/material/AccordionSummary';
import AccordionDetails from '@mui/material/AccordionDetails';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import CircularProgress from '@mui/material/CircularProgress';
import { green } from '@mui/material/colors';
import { Card } from '@mui/material';
import { Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, Slide, SlideProps, Grid2} from '@mui/material';
import { TransitionProps } from '@mui/material/transitions/transition';
import { useRouter, useSearchParams } from 'next/navigation'

const Transition = React.forwardRef(function Transition(
    props: TransitionProps & {
        children: React.ReactElement<any, any>;
    },
    ref: React.Ref<unknown>,
) {
    return <Slide direction="up" ref={ref} {...props} />;
});

export default function SignUp({ params }: { params: Promise<{ name: string }> }) {
    const router = useRouter();
    const searchParams = useSearchParams();
    const retunUrl = searchParams.get('next_page') || "/account";
    const param = React.useMemo(() => new URLSearchParams(), []);

    const [submitting, setSubmitting] = React.useState(false);
    const handleSubmit = async (event: React.FormEvent<HTMLFormElement>) => {
        if (username === null) {
            notify.ShowNotification(notify.location.Top, notify.Severity.Error, "No username provided", "Error")
            return
        }

        setSubmitting(true);
        event.preventDefault();
        notify.CloseNotification();

        // get admin status from checkbox

        const target = event.target as typeof event.target & {
            firstName: { value: string };
            lastName: { value: string };
            password: { value: string };
            admin: { checked: boolean };
        };

        const firstName = target.firstName.value;
        const lastName = target.lastName.value;
        const password = target.password.value === null ? "" : target.password.value;
        const admin = target.admin === undefined ? false : target.admin.checked;

        // update the member
        const resp = await updateMember(username, password, firstName, lastName, admin);
        if (resp === null) {
            setSubmitting(false);
            notify.ShowNotification(notify.location.Top, notify.Severity.Error, "Somthing went wrong", "Error")
            return
        }
        if (resp.status === 200) {
            if (resp.data === null) {
                setSubmitting(false);
                notify.ShowNotification(notify.location.Top, notify.Severity.Error, "Somthing went wrong", "Error")
                return
            }
            else {
                // only store data is the username is the same
                if (username === localStorage.getItem('username')) {
                    // if the update was successful, add the new data to localstorage
                    if (resp.data != null) {
                        localStorage.setItem('firstname', firstName);
                        localStorage.setItem('lastname', lastName);
                        localStorage.setItem('admin', admin.toString());
                        // check if the data is correct, else show notify
                        if (firstName == resp['data']['firstname']
                            && lastName == resp['data']['lastname']
                            && admin == resp['data']['admin']) {
                            // redirect to account page
                            param.set("severity", "success");
                            param.set("message", "Account is updated");
                            param.set("title", "Success");
                            param.set("location", "top");
                            router.push(retunUrl + "?" + param.toString());
                            // window.location.href = retunUrl + "?severity=Success&message=Account updated&location=top"
                            return
                        }
                        else {
                            notify.ShowNotification(notify.location.Top, notify.Severity.Error, "Somthing went wrong", "Error")
                        }
                    }
                return
                }
                else
                {
                    param.set("severity", "success");
                    param.set("message", "Account " + username + " is updated");
                    param.set("title", "Success");
                    param.set("location", "bottom");
                    router.push(retunUrl + "?" + param.toString());
                    // window.location.href = retunUrl + "?severity=Success&message=Account " + username + " is updated&title=Success&location=bottom"
                }
            }
        } else {
            setSubmitting(false);
            notify.ShowNotification(notify.location.Top, notify.Severity.Error, resp['message']['error'], "Error: " + resp.status + " " + resp.text)
            return
        }
    };
    const [showPassword, setShowPassword] = React.useState(false);
    const handleClickShowPassword = () => setShowPassword((show) => !show);

    const [username, setUsername] = React.useState<string | null>(null);
    const { name } = React.use(params)
    React.useEffect(() => {
        setUsername(name);
    }, [name]);
    
    // get the current user from the server
    const [member, setMember] = React.useState<any>(null);
    const [isAdmin, setIsAdmin] = React.useState<boolean>(false);
    const [loading, setLoading] = React.useState<boolean>(true);
    
    
    const isLocalStorageDefined = typeof localStorage !== "undefined";

    React.useEffect(() => {
        const getMemberData = async () => {
            if (username === null) {
                return
            }
            getMember(username).then((resp) => {
                if (resp === null) {
                    notify.ShowNotification(notify.location.Top, notify.Severity.Error, "Somthing went wrong", "Error")
                    return
                } else if (resp.status === 200) {
                    if (resp.data === null) {
                        notify.ShowNotification(notify.location.Top, notify.Severity.Error, "Somthing went wrong", "Error")
                        return
                    }
                    else {
                        setMember(resp.data);
                        setLoading(false);
                        return 
                    }
                } else {
                    // notify.ShowNotification(notify.location.Top, notify.Severity.Error, resp['message']['error'], "Error: " + resp.status + " " + resp.text)
                    param.append("severity", "error");
                    param.append("message", "Error: " + resp.status + " " + resp.text);
                    param.append("title", resp['message']['error']);
                    param.append("location", "top");
                    router.push(retunUrl + "?" + param.toString());
                    return
                }       
            });
        }

        if (isLocalStorageDefined){
            setIsAdmin(localStorage.getItem('admin') === 'true');
            getMemberData();
        }
    }, [isLocalStorageDefined, username, param, retunUrl, router]);

    const [openAlert, setOpenAlert] = React.useState(false);
    const handleCloseAlert = () => {
        setOpenAlert(false);
    };

    const [delMember, setDelMember] = React.useState<any>(null);
    const HandleDelete = async () => {
        if (username === null) {
            notify.ShowNotification(notify.location.Top, notify.Severity.Error, "No username provided", "Error")
            return
        }

        // event.preventDefault();
        setDelMember(true);

        // delete the member
        const resp = await deleteMember(username);
        if (resp === null) {
            notify.ShowNotification(notify.location.Top, notify.Severity.Error, "Somthing went wrong", "Error")
            return
        }

        if (resp.status === 200) {
            // redirect to account page
            param.append("severity", "Success");
            param.append("message", "Account " + username + " is deleted");
            param.append("title", "Success");
            param.append("location", "top");
            localStorage.clear();
            router.push(retunUrl + "?" + param.toString());
            // window.location.href = "/?severity=Success&message=Account " + username + " is deleted&title=Success&location=top"
            return
        } else {
            notify.ShowNotification(notify.location.Top, notify.Severity.Error, resp['message']['error'], "Error: " + resp.status + " " + resp.text)
            setDelMember(false);
            handleCloseAlert();
            return
        }
    }

    function AlertDialogSlide() {
        return (
            <div>
                <Dialog
                    open={openAlert}
                    TransitionComponent={Transition}
                    keepMounted
                    onClose={handleCloseAlert}
                    aria-describedby="alert-dialog-slide-description"
                >
                    <DialogTitle><strong>{"Delete"} {username}</strong></DialogTitle>
                    <DialogContent>
                        <DialogContentText id="alert-dialog-slide-description">
                            Do you really want to <strong>permanently delete</strong> my account, including all of games?
                        </DialogContentText>
                    </DialogContent>
                    <DialogActions sx={{ justifyContent: 'space-between', px: 3, mb: 2 }}>
                        {/* <Button variant='outlined' color='error' onClick={handleDelete} disabled={delMember}>Agree</Button> */}
                        <Button variant='outlined' color='error' onClick={HandleDelete} disabled={delMember}>Agree</Button>
                        <Button variant='contained' color='success' onClick={handleCloseAlert}>Cancel</Button>
                    </DialogActions>
                </Dialog>
            </div>
        );
    }

    // if the user is not loaded yet, show loading
    if (loading) {
        return (
            <ThemeProvider theme={pageTheme}>
                <CssBaseline />
                <NavBar />
                <PageLoader />
            </ThemeProvider>
        )
    } else {
        // if the user is not logged in, redirect to login page
        if (member === null) {
            window.location.href = "/login";
            return (
                <></>
            )
        } else {
            return (
                <ThemeProvider theme={pageTheme}>
                    <NavBar />
                    {AlertDialogSlide()}
                    <Container component="main" maxWidth="xs">
                        <CssBaseline />
                        <Box sx={{ marginTop: 8 }}>
                            <Card sx={{
                                p: 2,
                                display: 'flex',
                                flexDirection: 'column',
                                alignItems: 'center',
                            }}>
                                <Avatar sx={{ m: 1, bgcolor: 'secondary.main', width: 56, height: 56 }}>
                                    <LockOutlinedIcon />
                                </Avatar>
                                <Typography component="h1" variant="h5">
                                    Edit user: {username}
                                </Typography>
                                <Box component="form" onSubmit={handleSubmit} sx={{ mt: 3 }}>
                                    <Grid2 container spacing={2}>
                                        <Grid2 size={{xs: 12, sm: 6}}>
                                            <TextField
                                                required
                                                fullWidth
                                                id="firstName"
                                                label="First Name"
                                                name="firstName"
                                                autoComplete="given-name"
                                                slotProps={{ htmlInput: { maxLength: 20 } }}
                                                // inputRef={(input) => {
                                                //     if (input != null) {
                                                //         input.value = member.firstname;
                                                //     }

                                                // }}
                                                defaultValue={member.firstname}
                                                disabled={submitting}
                                            />
                                        </Grid2>
                                        <Grid2 size={{xs: 12, sm: 6}}>
                                            <TextField
                                                fullWidth
                                                id="lastName"
                                                label="Last Name"
                                                name="lastName"
                                                autoComplete="family-name"
                                                slotProps={{ htmlInput: { maxLength: 20 } }}
                                                // inputRef={(input) => {
                                                //     if (input != null) {
                                                //         input.value = member.lastname;
                                                //     }
                                                // }}
                                                defaultValue={member.lastname}
                                                disabled={submitting}
                                            />
                                        </Grid2>
                                        {/* <Grid item xs={12}>
                                    <TextField
                                        fullWidth
                                        id="username"
                                        label="Username"
                                        name="username"
                                        autoComplete="username"
                                        disabled={true}
                                        inputProps={{ maxLength: 20 }}
                                        inputRef={(input) => {
                                            if (input != null) {
                                                input.value = member.username;
                                            }
                                        }}
                                    />
                                </Grid> */}
                                        <Grid2 size={{xs: 12}}>
                                            <Accordion sx={{ bgcolor: pageTheme.palette.action.disabledBackground }}>
                                                <AccordionSummary
                                                    expandIcon={<ExpandMoreIcon />}
                                                    aria-controls="change_password-content"
                                                    id="change_password"
                                                >
                                                    <Typography>Change password</Typography>
                                                </AccordionSummary>
                                                <AccordionDetails sx={{ px: 3 }}>
                                                    <Typography>
                                                        If you want to change your password, enter it here. Otherwise, leave it blank.
                                                    </Typography>
                                                    <br />
                                                    <FormControl
                                                        variant="outlined"
                                                        fullWidth
                                                    >
                                                        <InputLabel htmlFor="password-field">New password</InputLabel>
                                                        <OutlinedInput
                                                            name="password"
                                                            label="New password"
                                                            id="password-field"
                                                            autoComplete="new-password"
                                                            inputProps={{ maxLength: 20 }}
                                                            type={showPassword ? 'text' : 'password'}
                                                            disabled={submitting}
                                                            endAdornment={
                                                                <InputAdornment position="end">
                                                                    <IconButton
                                                                        aria-label="toggle password visibility"
                                                                        onClick={handleClickShowPassword}
                                                                        // onMouseDown={handleMouseDownPassword}
                                                                        edge="end"
                                                                    >
                                                                        {showPassword ? <VisibilityOff /> : <Visibility />}
                                                                    </IconButton>
                                                                </InputAdornment>
                                                            }
                                                        />
                                                    </FormControl>
                                                </AccordionDetails>
                                            </Accordion>
                                        </Grid2>
                                        {/* schow only when admin*/}
                                        {isAdmin && (
                                            <Grid2 size={{xs: 12}} sx={{ display: 'flex', alignItems: 'center' }}>
                                                <Checkbox
                                                    color="primary"
                                                    inputProps={{ 'aria-label': 'secondary checkbox' }}
                                                    name="admin"
                                                    id="admin"
                                                    disabled={submitting}
                                                    defaultChecked={member.admin}
                                                />
                                                <Typography variant="body2">Admin privileges</Typography>
                                            </Grid2>
                                        )}
                                        {/* show only when admin */}

                                    </Grid2>
                                    <Box sx={{ position: 'relative', justifyContent: '' }}>
                                        <Button
                                            type="submit"
                                            disabled={submitting}
                                            fullWidth
                                            variant="contained"
                                            sx={{ mt: 3, mb: 2 }}
                                        >
                                            Update account
                                        </Button>
                                        {submitting && (
                                            <CircularProgress
                                                size={32}
                                                sx={{
                                                    color: green[500],
                                                    position: 'absolute',
                                                    top: '50%',
                                                    left: '50%',
                                                    marginTop: '-12px',
                                                    marginLeft: '-12px',
                                                }}
                                            />
                                        )}
                                    </Box>
                                    <Grid2 container justifyContent="space-between">
                                        <Button disabled={delMember} variant='outlined' color='error' onClick={() => setOpenAlert(true)}>
                                            Delete
                                        </Button>
                                        <Button href={retunUrl}>
                                            Cancel
                                        </Button>
                                    </Grid2>
                                </Box>
                            </Card>
                        </Box>
                    </Container>
                </ThemeProvider>
            )
        };
    }
}