
"use client"
import React, { use } from 'react';
import Card from '@mui/material/Card';
import Typography from '@mui/material/Typography';
import Paper from '@mui/material/Paper';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TablePagination from '@mui/material/TablePagination';
import TableRow from '@mui/material/TableRow';
import styled from '@mui/material/styles/styled';
import TableCell, { tableCellClasses } from '@mui/material/TableCell';
import { pageTheme } from '@/app/theme';
import Button from '@mui/material/Button';
import Box from '@mui/material/Box';
import * as Icon from '@mui/icons-material';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import Slide from '@mui/material/Slide';
import { TransitionProps } from '@mui/material/transitions';
import { deleteGame, getGames } from '@/tools/api/gamesAPI';
import * as notify from '@/app/notify';
import { useNow, useFormatter, useTimeZone } from 'next-intl';
import {  GameData, gameList } from '@/tools/gameTypes';
import { LinearProgress } from '@mui/material';
import { useRouter, useSearchParams } from 'next/navigation'
import dayjs from 'dayjs';
import { DataGrid, GridApi, GridColDef, GridEditCellValueParams, GridExpandMoreIcon } from '@mui/x-data-grid'
import { PageLoader } from '@/app/loader';


const StyledGridOverlay = styled('div')(({ theme }) => ({
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    height: '100%',
    '& .ant-empty-img-1': {
        fill: theme.palette.mode === 'light' ? '#aeb8c2' : '#262626',
    },
    '& .ant-empty-img-2': {
        fill: theme.palette.mode === 'light' ? '#f5f5f7' : '#595959',
    },
    '& .ant-empty-img-3': {
        fill: theme.palette.mode === 'light' ? '#dce0e6' : '#434343',
    },
    '& .ant-empty-img-4': {
        fill: theme.palette.mode === 'light' ? '#fff' : '#1c1c1c',
    },
    '& .ant-empty-img-5': {
        fillOpacity: theme.palette.mode === 'light' ? '0.8' : '0.08',
        fill: theme.palette.mode === 'light' ? '#f5f5f5' : '#fff',
    },
}));


function CustomNoRowsOverlay() {
    return (
        <StyledGridOverlay>
            <svg
                style={{ flexShrink: 0 }}
                width="240"
                height="200"
                viewBox="0 0 184 152"
                aria-hidden
                focusable="false"
            >
                <g fill="none" fillRule="evenodd">
                    <g transform="translate(24 31.67)">
                        <ellipse
                            className="ant-empty-img-5"
                            cx="67.797"
                            cy="106.89"
                            rx="67.797"
                            ry="12.668"
                        />
                        <path
                            className="ant-empty-img-1"
                            d="M122.034 69.674L98.109 40.229c-1.148-1.386-2.826-2.225-4.593-2.225h-51.44c-1.766 0-3.444.839-4.592 2.225L13.56 69.674v15.383h108.475V69.674z"
                        />
                        <path
                            className="ant-empty-img-2"
                            d="M33.83 0h67.933a4 4 0 0 1 4 4v93.344a4 4 0 0 1-4 4H33.83a4 4 0 0 1-4-4V4a4 4 0 0 1 4-4z"
                        />
                        <path
                            className="ant-empty-img-3"
                            d="M42.678 9.953h50.237a2 2 0 0 1 2 2V36.91a2 2 0 0 1-2 2H42.678a2 2 0 0 1-2-2V11.953a2 2 0 0 1 2-2zM42.94 49.767h49.713a2.262 2.262 0 1 1 0 4.524H42.94a2.262 2.262 0 0 1 0-4.524zM42.94 61.53h49.713a2.262 2.262 0 1 1 0 4.525H42.94a2.262 2.262 0 0 1 0-4.525zM121.813 105.032c-.775 3.071-3.497 5.36-6.735 5.36H20.515c-3.238 0-5.96-2.29-6.734-5.36a7.309 7.309 0 0 1-.222-1.79V69.675h26.318c2.907 0 5.25 2.448 5.25 5.42v.04c0 2.971 2.37 5.37 5.277 5.37h34.785c2.907 0 5.277-2.421 5.277-5.393V75.1c0-2.972 2.343-5.426 5.25-5.426h26.318v33.569c0 .617-.077 1.216-.221 1.789z"
                        />
                    </g>
                    <path
                        className="ant-empty-img-3"
                        d="M149.121 33.292l-6.83 2.65a1 1 0 0 1-1.317-1.23l1.937-6.207c-2.589-2.944-4.109-6.534-4.109-10.408C138.802 8.102 148.92 0 161.402 0 173.881 0 184 8.102 184 18.097c0 9.995-10.118 18.097-22.599 18.097-4.528 0-8.744-1.066-12.28-2.902z"
                    />
                    <g className="ant-empty-img-4" transform="translate(149.65 15.383)">
                        <ellipse cx="20.654" cy="3.167" rx="2.849" ry="2.815" />
                        <path d="M5.698 5.63H0L2.898.704zM9.259.704h4.985V5.63H9.259z" />
                    </g>
                </g>
            </svg>
            <Box sx={{ mt: 1 }}>No locations</Box>
        </StyledGridOverlay>
    );
}


const Transition = React.forwardRef(function Transition(
    props: TransitionProps & {
        children: React.ReactElement<any, any>;
    },
    ref: React.Ref<unknown>,
) {
    return <Slide direction="up" ref={ref} {...props} />;
});


interface TabelData {
    id: number;
    id_num: number;
    game_name: string;
    game_type: string;
    active: boolean;
    date_edit: Date;
    time_until: string;
}

function addTabelData(
    id: number,
    id_num: number, 
    game_name: string, 
    game_type: string,
    active: boolean, 
    date_edit: Date,
    time_until: string,
): TabelData {
    const data: TabelData = {
        id: id,
        id_num: id_num,
        game_name: game_name,
        game_type: game_type,
        active: active,
        date_edit: date_edit,
        time_until: time_until,
    };
    return data;
}

export default function MyGameList() {
    
    const column: GridColDef[] = [
        {
            field: "edit",
            headerName: "Edit",
            headerAlign: 'center',
            disableColumnMenu: true,
            sortable: false,
            filterable: false,
            width: 90,
            renderCell: (params) => {
            const onClick = (e: React.MouseEvent) => {
                e.stopPropagation(); // don't select this row after clicking
        
                const api: GridApi = params.api;
                const thisRow: Record<string, GridEditCellValueParams> = {};
        
                api
                .getAllColumns()
                .filter((c) => c.field !== "__check__" && !!c)
                    .forEach((c) => {
                        thisRow[c.field] = {
                        id: params.id,
                        field: c.field,
                        value: params.row[c.field],
                        };
                    });
                    
            };
        
            return tabelButton(params.row.id_num.toString());
            }
        },
        {  
            field: 'id_num',     
            headerName: 'ID',
            type: 'number',
            minWidth: 50,
            maxWidth: 100,
            flex: 1,
            align: 'center',
            headerAlign: 'center',
        },
        {  
            field: 'game_name',  
            headerName: 'Game name',
            minWidth: 150 ,
            flex: 1,
            align: 'center',
            headerAlign: 'center',
        },
        {  
            field: 'game_type',  
            headerName: 'Game type',     
            minWidth: 100,
            flex: 1,
            align: 'center', 
            headerAlign: 'center',
        },
        {
            field: 'active',
            headerName: 'Active',
            type: 'boolean',
            minWidth: 100,
            maxWidth: 200,
            flex: 1,
            align: 'center',
            headerAlign: 'center',
            renderCell: (params) => {
                const isActive = params.value as boolean;
                const cellStyle = {
                    color: isActive ? 'green' : 'red',
                };
                return (
                    <div style={cellStyle}>
                        {isActive ? <Icon.Check/> : <Icon.Close/>}
                    </div>
                );
            },
        },
        {  
            field: 'date_edit',  
            headerName: 'Date edit',     
            type: 'dateTime',
            width: 200,
            align: 'left',
            headerAlign: 'left',
        },
        {  
            field: 'time_until', 
            headerName: '',              
            width: 150,
            align: 'center',
            headerAlign: 'center',
        },
        {
            field: "remove",
            headerName: "Remove",
            headerAlign: 'center',
            sortable: false,
            filterable: false,
            disableColumnMenu: true,
            width: 90,
            renderCell: (params) => { 
            return (
                <Button
                variant="contained"
                color='error'
                onClick={() => handleConfirm(params.row.id_num.toString())}
                sx={{
                    width: '10px',
                    mx: 1,
                }}
            >
                <Icon.DeleteForeverRounded />
            </Button>
            );
            }
        }
    ];

        
    const [rows, setRows] = React.useState<TabelData[]>([]);
    const timeZone = useTimeZone();
    const formatter = useFormatter();
    const router = useRouter();
    const searchParams = useSearchParams();
    const [rtnParms, setRtnParms] = React.useState(new URLSearchParams(useSearchParams()));

    const now = useNow();


    const [open, setOpen] = React.useState(false);
    const handleClose = () => {
        setOpen(false);
    };

    const [gameID, setGameID] = React.useState("");

    function handleConfirm(gameID: string) {
        setGameID(gameID);
        setOpen(true);
    }

    function editButtonClicked(gameID: string) {
        router.push("game/" + gameID + "/edit");
    }

    function tabelButton(info: string) {
        return (
            <Button
                variant="contained"
                color='info'
                onClick={() => editButtonClicked(info)}
                sx={{
                    width: '10px',
                    mx: 1,
                }}
            >
                <Icon.Edit />
            </Button>
        );
    }

    const [loading, setLoading] = React.useState(false);
    const handleRemove = async () => {
        const gameName = games !== null ? games.find((game: GameData) => game.id === Number(gameID))?.name : "";
        console.log("Removing game " + gameName + " with ID: " + gameID);

        notify.CloseNotification();
        const resp = await deleteGame(Number(gameID));
        var title = "Error";
            var message = "Something went wrong";
            if (resp == null) {
                notify.ShowNotification(notify.location.Top, notify.Severity.Error, "Response is null", "No status");
                return;
            }
            if (resp.data) {
                // status OK
                // window.location.href = "/game/" + resp.data["id"] + "/edit";
                rtnParms.set("severity", "success");
                rtnParms.set("message", "Game " + gameName + " is succesfuly removed");
                rtnParms.set("title", "Game removed");
                rtnParms.set("location", "top");
                router.push(window.location.pathname + "?" + rtnParms.toString());
                setOpen(false);
                setLoading(true);
                return;
            } else if (resp.message) {
                // other error
                notify.ShowNotification(notify.location.Top, notify.Severity.Error, (resp?.message || "Response error"), (String(resp?.status) || "No status"));
                return;
            }
            notify.ShowNotification(notify.location.Bottom, notify.Severity.Error, message, title);
        
    }

    function AlertDialogSlide() {
        const gameName = games !== null ? games.find((game: GameData) => game.id === Number(gameID))?.name : "";
        return (
            <div>
                <Dialog
                    open={open}
                    TransitionComponent={Transition}
                    keepMounted
                    onClose={handleClose}
                    aria-describedby="alert-dialog-slide-description"
                >

                    <DialogTitle><strong>{"Remove"} {gameName}</strong></DialogTitle>
                    <DialogContent>
                        <DialogContentText id="alert-dialog-slide-description">
                            Do you want to remove <strong>{gameName}</strong>?
                        </DialogContentText>
                    </DialogContent>
                    <DialogActions sx={{ justifyContent: 'space-between', px: 2 }}>
                        <Button variant='contained' color='success' onClick={handleClose}>Cancel</Button>
                        <Button variant='outlined' color='error' onClick={handleRemove}>Agree</Button>
                    </DialogActions>
                </Dialog>
            </div>
        );
    }

      // get the current user from the server
    const [games, setGames] = React.useState<null | any>(null);
    const [loadingData, setLoadingData] = React.useState<boolean>(true);
    async function getMyGames(){
        const resp = await getGames()

        if (resp === null) {
            notify.ShowNotification(notify.location.Top, notify.Severity.Error, "Somthing went wrong", "Error")
            return
        }

        if (resp.status === 200) {
            if (resp.data === null) {
                notify.ShowNotification(notify.location.Top, notify.Severity.Error, "Somthing went wrong", "Error")
                return
            }
            else {
                setGames(resp.data);
                setLoadingData(false);
                return
            }
        }else {
            notify.ShowNotification(notify.location.Top, notify.Severity.Error, resp['message']['error'], "Error: " + resp.status + " " + resp.text)
            return
        }       
    }   
    
    React.useEffect(() => {
        if (!loadingData) {
            setRows([]);
            games.forEach((game: GameData) => {
                const timeDate = new Date(game['date_edit']);
                // const dateTime = timeDate.toLocaleString(navigator.language);
                const timeUntil = formatter.relativeTime(dayjs(timeDate.toLocaleString("en-US")).toDate(), now);

                setRows((prev) => [...prev, addTabelData(
                    prev.length + 1,
                    game.id,
                    game.name,
                    gameList[game.game_type].name,
                    game.settings.active,
                    timeDate,
                    timeUntil,
                )]);
            });
        } else {
            if (typeof localStorage !== "undefined") {
                getMyGames();
            }
        }
    }, [loadingData, games, now, formatter]);

    return (
        <>
            {AlertDialogSlide()}
            {loading && <PageLoader/>}
            <Card sx={{ p: 1, m: 1 }}>
                <Typography variant="h5" fontWeight={'bold'}>My Games</Typography>
                <Paper sx={{
                    width: '100%',
                    overflow: 'hidden',
                    mt: 2
                }}>
                    {loadingData && <LinearProgress/>}
                    {!loadingData && <>
                        <DataGrid
                            slots={{
                                noRowsOverlay: CustomNoRowsOverlay,
                                columnSortedDescendingIcon: () => <Icon.ExpandMore className="icon" />,
                                columnSortedAscendingIcon: () => <Icon.ExpandLess className="icon" />,
                                columnUnsortedIcon: () => <Icon.Sort className="icon" />,
                            }}
                            rows={rows}
                            columns={column}
                            initialState={{
                                pagination: {
                                    paginationModel: {
                                        pageSize: 10
                                    }
                                },
                                sorting: {
                                    sortModel: [{ field: 'id_num', sort: 'desc' }],
                                }
                            }}
                            sx={{ '--DataGrid-overlayHeight': '400px', height: rows.length == 0 ? '400px' : '' }}
                            pageSizeOptions={[10, 25, 100]}
                            hideFooterSelectedRowCount
                            // onRowSelectionModelChange={(ids: any) => onRowsSelectionHandler(ids)}
                            // rowSelectionModel={selected}
                            // TODO: go to te page where the row is selected
                        />
                    </>}
                </Paper>
            </Card>
        </>
    );
}