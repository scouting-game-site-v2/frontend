"use client"
import React, { Suspense, lazy } from 'react';
import { ThemeProvider } from '@mui/material/styles';
import { pageTheme } from "@/app/theme";
import Container from "@mui/material/Container";
import NavBar from '@/app/navbar';
import CssBaseline from '@mui/material/CssBaseline';
import Typography from '@mui/material/Typography';
// import MyGamesList from '@/app/account/games';
import Card from '@mui/material/Card';
import Button from '@mui/material/Button';
import * as notify from '@/app/notify';
import { useSearchParams } from 'next/navigation'
import { QueryNotifications } from '@/tools/notification';
import { signal } from '@preact/signals';
import { LinearProgress } from '@mui/material';

const MyGamesList = lazy(() => import('@/app/account/games'));


export default function App()
{
    // TODO: check if authenticated

    // show query notifications
    const searchParams = useSearchParams();
    React.useEffect(() => {
        QueryNotifications(searchParams);
    }, [searchParams]);

    const [firstname, setFirstname] = React.useState("");
    const [lastname, setLastname] = React.useState("");
    const [username, setUsername] = React.useState("");

    const isLocalStorageDefined = typeof localStorage !== "undefined";

    React.useEffect(() => {
        if (isLocalStorageDefined){
            setFirstname(localStorage.getItem("firstname") || "");
            setLastname(localStorage.getItem("lastname") || "");
            setUsername(localStorage.getItem("username") || "");
        }
    }, [isLocalStorageDefined]);
    
    return(
        <>   
        <ThemeProvider theme={pageTheme}>
            <CssBaseline>
            <NavBar/>
            <Container maxWidth="xl">
                <Card sx={{ p: 1, m: 1}}>
                <Typography variant="h4">Welcome <strong>{firstname} {lastname}</strong></Typography>
                <p>Here you can see your games</p>
                <Button disabled={username == ""} variant="contained" onClick={() => window.location.href = "/account/" + username }>Edit account</Button>
                </Card>
                <Suspense fallback={<LinearProgress />}>
                    <MyGamesList/>
                </Suspense>
            </Container>
            </CssBaseline>
        </ThemeProvider>
        </>
    )
}