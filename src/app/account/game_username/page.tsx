"use client";
import React from 'react';
import { ThemeProvider } from '@mui/material/styles';
import { pageTheme } from "@/app/theme";
import Container from "@mui/material/Container";
import NavBar from '@/app/navbar';
import CssBaseline from '@mui/material/CssBaseline';
import Typography from '@mui/material/Typography';
import { Box, Button, LinearProgress, Paper, TextField } from '@mui/material';
import { signal } from '@preact/signals';
import * as notify from '@/app/notify';
import { useRouter, useSearchParams } from 'next/navigation'


export default function App()
{
    const [name, setName] = React.useState("");
    const [loading, setLoading] = React.useState(true);
    const searchParams = useSearchParams();
    const [rtnParms, setRtnParms] = React.useState(new URLSearchParams(useSearchParams()));
    const retunUrl = searchParams.get('next_page') || "/";
    const router = useRouter();

    

    const isLocalStorageDefined = typeof localStorage !== "undefined";

    React.useEffect(() => {
        if (isLocalStorageDefined)
        {
            setName(localStorage.getItem("game_username") || "");
            setLoading(false);
        }
    }, [isLocalStorageDefined]);

    // when after 10 seconds localStorage is still empty, show a notification
    React.useEffect(() => {
        setTimeout(() => {
            if (typeof localStorage === "undefined"){
                notify.ShowNotification(notify.location.Top, notify.Severity.Error, "Localstorage error", "Error while loading localstorage");
            }
        }, 10000);
    }, []);

    const handleSave = async (event: React.FormEvent<HTMLFormElement>) => {
        event.preventDefault();
        notify.CloseNotification();
        const data = new FormData(event.currentTarget);

        if (typeof localStorage === "undefined"){
            notify.ShowNotification(notify.location.Top, notify.Severity.Error, "Localstorage error", "Error while loading localstorage");
        } 
        else {
            var newName = data.get('game_username') as string;
            if (newName == "" || newName == null){
                notify.ShowNotification(notify.location.Bottom, notify.Severity.Warning, "No name provided");
            }
            else if (newName == name){
                notify.ShowNotification(notify.location.Bottom, notify.Severity.Warning, "New game username has the same name");
            }
            else {
                localStorage.setItem("game_username", newName);
                rtnParms.set("severity", "success");
                rtnParms.set("message", "Your game username is: " + newName);
                rtnParms.set("title", "Success");
                rtnParms.set("location", "bottom");
                router.push(retunUrl + "?" + rtnParms.toString());
            }
        }
    }
    

    return(
        <>   
        <ThemeProvider theme={pageTheme}>
            <CssBaseline>
            <NavBar/>
            <Container maxWidth="sm">
                <Paper 
                    square={false}
                    sx={{
                        p: 2,
                        display: 'flex',
                        flexDirection: 'column',
                        alignItems: 'center',
                    }}>
                    <Typography variant='h4' sx={{py: 2}}><strong>Edit game username</strong></Typography>
                    <Typography variant='body1'>Edit your username what will be used in the game.</Typography>
                    <Typography variant='body1'>This name will be stored at a location point.</Typography>
                    <hr style={{width: '90%'}}/>
                    <Box component="form" onSubmit={handleSave}>

                        <TextField
                            margin="normal"
                            required
                            fullWidth
                            label="Game username"
                            name="game_username"
                            id='game_username'
                            autoComplete="off"
                            autoFocus
                            slotProps={{ htmlInput: { maxLength: 20 }, inputLabel: { shrink: true } }}
                            defaultValue={name}
                            disabled={loading}
                        />
                        <Button variant='contained' color='success' sx={{mt: 1, width: '100%'}} type='submit' disabled={loading}>
                            Save
                        </Button>
                    </Box>
                </Paper>
                {loading && <LinearProgress sx={{width: '100%'}}/>}
            </Container>
            </CssBaseline>
        </ThemeProvider>
        </>
    )
}