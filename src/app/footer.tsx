"use client";
import React from "react";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";

export default function Footer() {
    const [currentYear, setCurrentYear] = React.useState<number>(0);
    React.useEffect(() => {
        setCurrentYear(new Date().getFullYear());
    }, []);

    return (
    <>
        {currentYear > 0 && (
            <Box sx={{ bgcolor: 'background.paper', 
                p: 2, 
                mt: 5,
                textAlign: 'center',
                opacity: 0,
                animation: 'fadeIn 1s forwards 1s'}}>
                    <style jsx global>{`
                        @keyframes fadeIn {
                            to {
                                opacity: 1;
                            }
                        }
                    `}</style>
                <Typography variant="body2" color="textSecondary">
                    Made with ❤️ by Tommy de Wever - ©{currentYear}
                </Typography>
            </Box>
            )}
        </>
    );
}
