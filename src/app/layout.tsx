import { NextIntlClientProvider } from 'next-intl'
import React, { Suspense } from 'react'
import Loading from '@/app/loading'

export const metadata = {
  title: 'Scouting game',
  description: 'Online scouting game',
}

export default function RootLayout({
  children,
}: {
  children: React.ReactNode
}) {


  return (
    <html lang="en">
      <body>
        <NextIntlClientProvider timeZone={Intl.DateTimeFormat().resolvedOptions().timeZone} locale='en' messages={{}}>
          <Suspense fallback={<Loading />}>{children}</Suspense>
        </NextIntlClientProvider>
      </body>
    </html>
  )
}
