"use client"
import React from "react";
import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import Toolbar from "@mui/material/Toolbar";
import IconButton from "@mui/material/IconButton";
import Typography from "@mui/material/Typography";
import Menu from "@mui/material/Menu";
import MenuIcon from "@mui/icons-material/Menu";
import Container from "@mui/material/Container";
import Avatar from "@mui/material/Avatar";
import Button from "@mui/material/Button";
import Tooltip from "@mui/material/Tooltip";
import MenuItem from "@mui/material/MenuItem";
import AdbIcon from "@mui/icons-material/Adb";
import { styled, ThemeProvider, getContrastRatio } from "@mui/material/styles";
import { pageTheme, themeOptions } from "@/app/theme";
import * as Icons from "@mui/icons-material";
import { Collapse, Divider, ListItemIcon } from "@mui/material";
import * as notify from "@/app/notify";
import Image from "next/image";


export default function NavBar() {
    const [next_page, setNext_page] = React.useState('');
    const navBarPages_default = React.useMemo(() => {
        return {
            name: [
                "About",
                "Join game",
            ],
            icon: [
                <Icons.Info key="About" />,
                <Icons.VideogameAsset key="Join game" />
            ],
            url: [
                "/about",
                "/game/join"
            ]
        }
    }, []);
    
    const navBarPages_singedIn = React.useMemo(() => {
        return {
            name: [
                "Create game", 
            ],
            icon: [
                <Icons.Extension key="create game" />
            ],
            url: [
                "/game"
            ]
        };
    }, []);
    
    const notSingedIn = React.useMemo(() => {
        return {
            name: [
                "Login", 
                "Sign Up", 
                "-",
                "Edit username"
            ],
            icon: [
                <Icons.Login key="login" />, 
                <Icons.PersonAdd key="signup" />, 
                <></>,
                <Icons.EditNote key="Edit username"/>
            ],
            url: [
                "/login", 
                "/signup", 
                "-",
                "/account/game_username" + next_page,
            ]
        }
    }, [next_page]);
    
    const singedIn = React.useMemo(() => {
        return {
            name: [
                "My account", 
                "Edit username",
                "-",
                "Logout"
            ],
            icon: [
                <Icons.Person key="account" />, 
                <Icons.EditNote key="Edit username"/>,
                <></>,
                <Icons.Logout key="logout" />
            ],
            url: [
                "/account", 
                "/account/game_username" + next_page, 
                "-",
                "/logout"
            ]
        }
    }, [next_page]);
    
    const Admin = React.useMemo(() => {
        return {
            name: [
                "Admin",
                "-"
            ],
            icon: [
                <Icons.AdminPanelSettings key="admin" />,
                <></>
            ],
            url: [
                "/admin",
                "-"
            ]
        }
    }, []);
    
    const Conditional = ({
        showWhen,
        children,
    }: {
        showWhen: boolean;
        children: React.ReactNode;
    }) => {
        return showWhen ? <>{children}</> : null;
    };
    const [accountInfo_menu, setAccountInfo_menu] = React.useState(notSingedIn);
    const [navBarItems, setNavBarItems] = React.useState(navBarPages_default);
    const [firstname, setFirstname] = React.useState("");
    const [lastname, setLastname] = React.useState("");
    const [admin, setAdmin] = React.useState(false);
    
    React.useEffect(() => {
        if (typeof window !== 'undefined'){
            setNext_page("?next_page=" + window.location.pathname);
        }
        console.log(next_page);
    }, [next_page]);
    

    const isLocalStorageDefined = typeof localStorage !== "undefined";

    React.useEffect(() => {
        if (isLocalStorageDefined){
            setFirstname(localStorage.getItem("firstname") || "");
            setLastname(localStorage.getItem("lastname") || "");
            setAdmin(localStorage.getItem("admin") == "true" || false);
        }
    }, [isLocalStorageDefined]);


    const [menuOpen, menuSetOpen] = React.useState(false);
    const [anchorElUser, setAnchorElUser] = React.useState<null | HTMLElement>(
        null
    );

    const handleToggelNavMenu = (event: React.MouseEvent<HTMLElement>) => {
        menuSetOpen(!menuOpen);
    };
    const handleOpenUserMenu = (event: React.MouseEvent<HTMLElement>) => {
        setAnchorElUser(event.currentTarget);
    };

    const handleCloseNavMenu = () => {
        menuSetOpen(false);
    };

    const handleCloseUserMenu = () => {
        setAnchorElUser(null);
    };

    function goToLink(url: string) {
        window.location.assign(url);

        if (menuOpen) {
            handleCloseNavMenu();
        }

        if (anchorElUser) {
            handleCloseUserMenu();
        }
    };

    function stringToColor(string: string) {
        let hash = 0;
        let i;

        /* eslint-disable no-bitwise */
        for (i = 0; i < string.length; i += 1) {
            hash = string.charCodeAt(i) + ((hash << 5) - hash);
        }

        let color = '#';

        for (i = 0; i < 3; i += 1) {
            const value = (hash >> (i * 8)) & 0xff;
            color += `00${value.toString(16)}`.slice(-2);
        }
        /* eslint-enable no-bitwise */

        return color;
    }

    function shadeColor(color: string, percent: number) {
        var R = parseInt(color.substring(1, 3), 16);
        var G = parseInt(color.substring(3, 5), 16);
        var B = parseInt(color.substring(5, 7), 16);

        R = parseInt(String(R * (100 + percent) / 100));
        G = parseInt(String(G * (100 + percent) / 100));
        B = parseInt(String(B * (100 + percent) / 100));

        R = (R < 255) ? R : 255;
        G = (G < 255) ? G : 255;
        B = (B < 255) ? B : 255;

        R = Math.round(R)
        G = Math.round(G)
        B = Math.round(B)

        var RR = ((R.toString(16).length == 1) ? "0" + R.toString(16) : R.toString(16));
        var GG = ((G.toString(16).length == 1) ? "0" + G.toString(16) : G.toString(16));
        var BB = ((B.toString(16).length == 1) ? "0" + B.toString(16) : B.toString(16));

        return "#" + RR + GG + BB;
    }

    const StyledAvatar = styled(Avatar)`
        ${({ theme }) => `
            cursor: pointer;
            background-color: ${theme.palette.primary.main};
            transition: ${theme.transitions.create(['background-color', 'color', 'transform'], {
        duration: theme.transitions.duration.standard,
    })};
            &:hover {
                background-color: ${shadeColor(stringToColor(firstname), 40)};
                color: ${getContrastRatio(shadeColor(stringToColor(firstname), 40), "#FFF") > 4.5 ? "#FFF" : "#111"};
                transform: scale(1.3);
            }
            border: 2px solid ${theme.palette.primary.dark};
            box-shadow: ${theme.shadows[3]};
            `}
        `;


    function stringAvatar(name: string) {
        return {
            sx: {
                color: getContrastRatio(stringToColor(name), "#FFF") > 4.5 ? "#FFF" : "#111",
                bgcolor: stringToColor(name),
            },
            children: `${name[0]}`,
        };
    }

    // set navbar items based on login state
    React.useEffect(() => {
        if (firstname != "") {
            setNavBarItems((prev) => ({
                name: [...prev.name, ...navBarPages_singedIn.name],
                icon: [...prev.icon, ...navBarPages_singedIn.icon],
                url: [...prev.url, ...navBarPages_singedIn.url],
            }));
        }
    },[firstname, navBarPages_singedIn]);

    // set account info menu items based on login state
    React.useEffect(() => {
        if (firstname != "") {
            setAccountInfo_menu(singedIn);
            if (admin){
                setAccountInfo_menu((prev) => ({
                    name: [...Admin.name, ...prev.name],
                    icon: [...Admin.icon, ...prev.icon],
                    url: [...Admin.url, ...prev.url],
                }));
            }
        }
    }, [firstname, admin, Admin, singedIn]);



    return (
        <ThemeProvider theme={pageTheme}>
            <AppBar position="static" color="primary" enableColorOnDark>
                <Container maxWidth="xl">
                    <Toolbar disableGutters>
                        {/* Items on mobiel */}
                        <Box sx={{ flexGrow: 1, display: { xs: "flex", md: "none" } }}>
                            <IconButton
                                size="large"
                                aria-label="account of current user"
                                aria-controls="menu-appbar"
                                aria-haspopup="true"
                                onClick={handleToggelNavMenu}
                                color="inherit"
                            >
                                <MenuIcon />
                            </IconButton>
                        </Box>

                        {/* Logo on Mobile */}
                        {/* <AdbIcon sx={{ display: { xs: "flex", md: "none" }, mr: 1 }} /> */}
                        <Box
                            component="img"
                            src="/static/favicon.ico"
                            alt="logo"
                            width={50}
                            height={50}
                            sx={{ display: { xs: "flex", md: "none" }, mr: 1 }}
                            onClick={() => goToLink("/")}
                        />
                        <Typography
                            variant="h5"
                            noWrap
                            component="a"
                            href="/"
                            sx={{
                                mr: 2,
                                display: { xs: "flex", md: "none" },
                                flexGrow: 1,
                                fontFamily: "monospace",
                                fontWeight: 700,
                                letterSpacing: "0rem",
                                color: "inherit",
                                textDecoration: "none",
                            }}
                        >
                            Scouting game
                        </Typography>

                        {/* Logo on desktop */}
                        {/* <AdbIcon sx={{ display: { xs: "none", md: "flex" }, mr: 1 }} /> */}
                        <Box
                            component="img"
                            src="/static/favicon.ico"
                            alt="logo"
                            width={50}
                            height={50}
                            sx={{ display: { xs: "none", md: "flex" }, mr: 1 }}
                            onClick={() => goToLink("/")}
                        />
                        <Typography
                            variant="h6"
                            noWrap
                            component="a"
                            href="/"
                            sx={{
                                mr: 2,
                                display: { xs: "none", md: "flex" },
                                fontFamily: "monospace",
                                fontWeight: 700,
                                letterSpacing: "0rem",
                                color: "inherit",
                                textDecoration: "none",
                            }}
                        >
                            Scouting game
                        </Typography>

                        {/* Items on desktop */}
                        <Box sx={{ flexGrow: 1, display: { xs: "none", md: "flex" } }}>
                            {navBarItems.name.map((page, index) => (
                                <Button
                                    href={navBarItems.url[index]}
                                    key={index}
                                    onClick={handleCloseNavMenu}
                                    sx={{
                                        my: 2,
                                        mx: 1,
                                        color: "white",
                                        // display: "block",
                                        textAlign: "center",
                                        transition: pageTheme.transitions.create(["transform", "color"], {
                                            duration: pageTheme.transitions.duration.standard,
                                        }),
                                        ':hover': {
                                            bgcolor: pageTheme.palette.primary.light, //'primary.main', // theme.palette.primary.main
                                            color: pageTheme.palette.getContrastText(pageTheme.palette.primary.light),
                                            transform: "scale(1.2)",
                                        },
                                    }}
                                    startIcon={navBarItems.icon[index]}
                                >
                                    {page}
                                </Button>
                            ))}
                        </Box>

                        {/* Display "getStorageValue(firstname", "")  */}
                        <Typography
                            variant="h5"
                            noWrap
                            component="a"
                            href={singedIn.url[0]}
                            sx={{
                                mr: 2,
                                flexGrow: 0,
                                display: { xs: "none", md: "flex" },
                                color: "inherit",
                                textDecoration: "none",
                            }}
                        >{firstname} {lastname}</Typography>

                        {/* Display avatar */}
                        <Box sx={{ flexGrow: 0 }}>

                            <Tooltip title={firstname == "" ? "Login" : "My account"}>
                                <IconButton onClick={handleOpenUserMenu} sx={{ p: 0 }}>
                                    <Conditional showWhen={firstname == ""}>
                                        <StyledAvatar sx={{ bgcolor: pageTheme.palette.secondary.dark, color: pageTheme.palette.common.white }}>
                                            <Icons.Login fontSize="small" />
                                        </StyledAvatar>
                                    </Conditional>
                                    <Conditional showWhen={firstname != ""}>
                                        <StyledAvatar {...stringAvatar(firstname )} />
                                    </Conditional>
                                </IconButton>
                            </Tooltip>

                            {/* Account info dropdown */}
                            <Menu
                                sx={{ mt: "45px" }}
                                id="menu-appbar"
                                anchorEl={anchorElUser}
                                anchorOrigin={{
                                    vertical: "top",
                                    horizontal: "right",
                                }}
                                keepMounted
                                transformOrigin={{
                                    vertical: "top",
                                    horizontal: "right",
                                }}
                                open={Boolean(anchorElUser)}
                                onClose={handleCloseUserMenu}
                            >
                                {accountInfo_menu.name.map((page, index) => {
                                    if (page === "-") {
                                        return <Divider key={index} />
                                    }
                                    const Icon = accountInfo_menu.icon[index];
                                    return (
                                        <MenuItem key={index} onClick={() => goToLink(accountInfo_menu.url[index])} sx={{ py: 1.5 }}>
                                            <ListItemIcon key={index}>
                                                {Icon}
                                            </ListItemIcon>
                                            <Container maxWidth="xl">
                                                <Typography textAlign="center">{page}</Typography>
                                            </Container>
                                        </MenuItem >
                                    )}
                                )}
                            </Menu>
                        </Box>
                    </Toolbar>
                </Container>
            </AppBar>
            {/* Items on mobile */}
            <AppBar position="static" color="primary" enableColorOnDark>
                <Collapse in={menuOpen}>
                    <Box 
                    sx={{ 
                        // position: "fixed", 
                        flexDirection: "column", 
                        justifyContent: "center",
                        flexGrow: 1, 
                        display: { xs: "flex", md: "none" },
                        // width: "100vw",
                        p: 1                
                    }} >
                        {/* {navBarItems.name.map((page, index) => (
                            <Button
                                variant="contained"
                                href={navBarItems.url[index]}
                                key={index}
                                onClick={handleCloseNavMenu}
                                sx={{
                                    my: 1,
                                    mx: 1,
                                    color: "white",
                                    // display: "block",
                                    textAlign: "center",
                                    // width: "90%"
                                }}
                                startIcon={navBarItems.icon[index]}
                            >
                                {page}
                            </Button>
                        ))} */}
                        {navBarItems.name.map((page, index) => {
                            if (page === "-") {
                                return <Divider key={index} />
                            }
                            const Icon = navBarItems.icon[index];
                            return (
                                <MenuItem key={index} onClick={() => goToLink(navBarItems.url[index])} sx={{ py: 1.5, display: 'flex'}}>
                                    <ListItemIcon key={index}>
                                        {Icon}
                                    </ListItemIcon>
                                    <Container maxWidth="xl">
                                        <Typography textAlign="center">{page}</Typography>
                                    </Container>
                                </MenuItem >
                            )}
                        )}
                    </Box>
                </Collapse>
            </AppBar>                       
            
            <Box sx={{display: 'inline'}}>
                <notify.TopNotification/>
            </Box>
            <notify.BottomNotification/>
        </ThemeProvider>
    );
}

