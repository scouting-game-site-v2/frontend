"use client"
import * as React from 'react';
import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import CssBaseline from '@mui/material/CssBaseline';
import TextField from '@mui/material/TextField';
import { Grid2 } from '@mui/material';
import Box from '@mui/material/Box';
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import { ThemeProvider } from '@mui/material/styles';
import { pageTheme } from '@/app/theme';
import NavBar from '@/app/navbar';
import * as notify from '@/app/notify';
import { login } from '@/tools/api/memberAPI';
import Card from '@mui/material/Card';

const LazyFooter = React.lazy(() => import("@/app/footer"));

export default function SignIn() {
    const handleSubmit = async (event: React.FormEvent<HTMLFormElement>) => {
        event.preventDefault();
        notify.CloseNotification();
        const data = new FormData(event.currentTarget);

        const resp = await login(data.get('username') as string, data.get('password') as string)

        var title = "Error";
        var message = "Something went wrong";
        if (resp == null) {
            notify.ShowNotification(notify.location.Top, notify.Severity.Error, "Response is null", "No status");
            return;
        }
        if (resp === true) {
            // status OK
            window.location.href = "/account";
            return;
        } else if (resp?.status === 403) {
            // password incorrect
            title = "Password incorrect";
            message = "The password you entered is incorrect.";
        } else if (resp?.status === 404) {
            // username not found
            title = "Username not found";
            message = "The username you entered is not found.";
        } else {
            // other error
            notify.ShowNotification(notify.location.Top, notify.Severity.Error, (resp?.message || "Response error"), (String(resp?.status) || "No status"));
            return;
        }
        notify.ShowNotification(notify.location.Bottom, notify.Severity.Error, message, title);
    };


    return (
        <ThemeProvider theme={pageTheme}>
            <NavBar />
            <Container component="main" maxWidth="xs" sx={{ minHeight: 'calc(100vh - 5rem)', display: 'flex', flexDirection: 'column' }}>
            <CssBaseline />
            <Box sx={{ marginTop: 8, flexGrow: 1 }}>
                <Card sx={{
                    p: 2,
                    display: 'flex',
                    flexDirection: 'column',
                    alignItems: 'center',
                }}>
                    <Avatar sx={{ m: 1, bgcolor: 'secondary.main', width: 56, height: 56 }}>
                        <LockOutlinedIcon />
                    </Avatar>
                    <Typography component="h1" variant="h5">
                        Sign in
                    </Typography>
                    <Box component="form" onSubmit={handleSubmit} sx={{ mt: 1 }}>
                        <TextField
                            margin="normal"
                            required
                            fullWidth
                            id="username"
                            label="Username"
                            name="username"
                            autoComplete="off"
                            autoFocus
                            slotProps={{ htmlInput: { maxLength: 20 } }}
                        />
                        <TextField
                            margin="normal"
                            required
                            fullWidth
                            name="password"
                            label="Password"
                            type="password"
                            id="password"
                            autoComplete="current-password"
                            slotProps={{ htmlInput: { maxLength: 20 } }}
                        />
                        <Button
                            type="submit"
                            fullWidth
                            variant="contained"
                            sx={{ mt: 3, mb: 2 }}
                        >
                            Sign In
                        </Button>

                        <Grid2 container>
                            <Grid2 size="grow">
                                <Button 
                                    color='secondary' 
                                    onClick={() => 
                                        notify.ShowNotification(notify.location.Top, notify.Severity.Info, "Please contact an administrator. Go to the About page for more information", "Forgot your password?")
                                    }
                                >
                                    Forgot password?
                                </Button>
                            </Grid2>
                            <Grid2>
                                <Button color='secondary' href="/signup">
                                    Create account
                                </Button>
                            </Grid2>
                        </Grid2>
                    </Box>
                </Card>
            </Box>
            <Box sx={{ mt: 'auto' }}>
                <LazyFooter />
            </Box>
            </Container>
        </ThemeProvider>
    );
}
