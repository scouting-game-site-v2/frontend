"use client"
import React from 'react';
import { ThemeProvider } from '@mui/material/styles';
import { pageTheme } from "@/app/theme";
import Container from "@mui/material/Container";
import NavBar from '@/app/navbar';
import CssBaseline from '@mui/material/CssBaseline';
import Typography from '@mui/material/Typography';
import { Button } from '@mui/material';
import * as notify from '@/app/notify'
import { useSearchParams } from 'next/navigation'

export default function Apps({ params }: { params: Promise<{ slug: string }> }){  
    const { slug } = React.use(params);   
    const [data, setData] = React.useState<string>('');
    React.useEffect(() => {
        setData(slug);
    }, [slug]);

    notify.ShowNotification(notify.location.Top, notify.Severity.Info, "Message: " + data, "Code: " + data);

    return(
        <>   
        <ThemeProvider theme={pageTheme}>
            <CssBaseline>
            <NavBar/>
            <Container maxWidth="sm">
                <Typography>Dynamic page</Typography>
                <Typography>Slug: {data}</Typography>
            </Container>
            </CssBaseline>
        </ThemeProvider>
        </>
    )
}
