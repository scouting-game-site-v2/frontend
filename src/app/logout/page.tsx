"use client"

import { useEffect } from "react";
import { signal } from '@preact/signals'
import { Button, Container, CssBaseline, Divider, LinearProgress, Paper, Typography } from "@mui/material";
import { ThemeProvider } from "@emotion/react";
import { pageTheme } from "../theme";
import NavBar from "../navbar";

export default function LogoutPage() {	
    const localstorageActive = signal(false);
    const loginOut = signal(false);
    function logOut() {
        // clear local storage
        if (localstorageActive.value === true){
            loginOut.value = true;
            localStorage.clear();
            // redirect to home page
            window.location.href = '/';

        }
    
        if (typeof localStorage !== "undefined")
        {
            localstorageActive.value = true;
        }
    }
    
    return (
        <ThemeProvider theme={pageTheme}>
            <CssBaseline>
                <NavBar/>
                <Container maxWidth="sm">
                    <Paper square={false}  sx={{p: 2, display: 'flex', alignItems: 'center', justifyItems: 'center', flexDirection: 'column'}}>
                        <Typography variant='h5' sx={{padding: 1}}>
                            Are you sure you want to sign out?
                        </Typography>
                        <Typography variant='body1'>
                            You will be redirected to the home page.
                        </Typography>
                        <Divider sx={{width: '100%', m: 2}}/>
                        <Button variant="outlined" color="warning" onClick={logOut} disabled={loginOut.value}>
                            logout
                        </Button>
                        {loginOut.value && <LinearProgress sx={{width: '80%'}}/>}
                    </Paper>
                </Container>
            </CssBaseline>
        </ThemeProvider>
    )
}