"use client"
import * as React from 'react';
import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import CssBaseline from '@mui/material/CssBaseline';
import TextField from '@mui/material/TextField';
import { Grid2 } from '@mui/material';
import Box from '@mui/material/Box';
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import { ThemeProvider } from '@mui/material/styles';
import { pageTheme } from '@/app/theme';
import NavBar from '@/app/navbar';
import { signup } from '@/tools/api/memberAPI';
import * as notify from '@/app/notify';
import Card from '@mui/material/Card';

const LazyFooter = React.lazy(() => import("@/app/footer"));

export default function SignUp() {
    const handleSubmit = async (event: React.FormEvent<HTMLFormElement>) => {
        event.preventDefault();
        notify.CloseNotification();
        const data = new FormData(event.currentTarget);

        const resp = await signup(data.get('username') as string, data.get('password') as string, data.get('firstName') as string, data.get('lastName') as string);

        var title = "Error";
        var message = "Something went wrong";
        if (resp == null) {
            notify.ShowNotification(notify.location.Top, notify.Severity.Error, "Response is null", "No status");
            return;
        }
        if (resp === true) {
            // status OK
            // redirect to account page
            window.location.href = "/account";
            return
        } else if (resp.status === 400) {
            // user already exists
            title = "User already exists";
            message = "The username you entered already exists.";
        } else {
            // other error
            notify.ShowNotification(notify.location.Top, notify.Severity.Error, resp['message']['error'], "Error: " + resp.status + " " + resp.text)
            return;
        }
        notify.ShowNotification(notify.location.Bottom, notify.Severity.Error, message, title);
    };

    return (
        <ThemeProvider theme={pageTheme}>
            <NavBar />
            <Container component="main" maxWidth="xs" sx={{ minHeight: 'calc(100vh - 5rem)', display: 'flex', flexDirection: 'column' }}>
                <CssBaseline />
                <Box sx={{ marginTop: 8 }}>
                    <Card sx={{
                        p: 2,
                        display: 'flex',
                        flexDirection: 'column',
                        alignItems: 'center',
                    }}>
                        <Avatar sx={{ m: 1, bgcolor: 'secondary.main', width: 56, height: 56 }}>
                            <LockOutlinedIcon />
                        </Avatar>
                        <Typography component="h1" variant="h5">
                            Sign up
                        </Typography>
                        <Box component="form" onSubmit={handleSubmit} sx={{ mt: 3 }}>
                            <Grid2 container spacing={2}>

                                <Grid2 size={{xs: 12, sm: 6}}>
                                    <TextField
                                        autoComplete="given-name"
                                        name="firstName"
                                        required
                                        fullWidth
                                        id="firstName"
                                        label="First Name"
                                        autoFocus
                                        slotProps={{ htmlInput: { maxLength: 20 } }}
                                    />
                                </Grid2>

                                <Grid2 size={{xs: 12, sm: 6}}>
                                    <TextField
                                        required
                                        fullWidth
                                        id="lastName"
                                        label="Last Name"
                                        name="lastName"
                                        autoComplete="family-name"
                                        slotProps={{ htmlInput: { maxLength: 20 } }}
                                    />
                                </Grid2>
                                <Grid2 size={{xs: 12}}>
                                    <TextField
                                        required
                                        fullWidth
                                        id="username"
                                        label="Username"
                                        name="username"
                                        autoComplete="username"
                                        slotProps={{ htmlInput: { maxLength: 20 } }}
                                    />
                                </Grid2>
                                <Grid2 size={{xs: 12}}>
                                    <TextField
                                        required
                                        fullWidth
                                        name="password"
                                        label="Password"
                                        type="password"
                                        id="password"
                                        autoComplete="new-password"
                                        slotProps={{ htmlInput: { maxLength: 20 } }}
                                    />
                                </Grid2>
                            </Grid2>
                            <Button
                                type="submit"
                                fullWidth
                                variant="contained"
                                sx={{ mt: 3, mb: 2 }}
                            >
                                Sign Up
                            </Button>
                            <Grid2 container justifyContent="flex-end">
                                <Grid2>
                                    <Button href="/login" color="secondary">
                                        Log in instead
                                    </Button>
                                </Grid2>
                            </Grid2>
                        </Box>
                    </Card>
                </Box>
                <Box sx={{ mt: 'auto' }}>
                    <LazyFooter />
                </Box>
            </Container>
        </ThemeProvider>
    );
}
