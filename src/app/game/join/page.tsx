"use client"
import React from 'react';
import { ThemeProvider } from '@mui/material/styles';
import { pageTheme } from "@/app/theme";
import Container from "@mui/material/Container";
import NavBar from '@/app/navbar';
import CssBaseline from '@mui/material/CssBaseline';
import Typography from '@mui/material/Typography';
import { useRouter, useSearchParams } from 'next/navigation'
import { Box, Button, Card, CardActions, CardContent, CardHeader, Divider, LinearProgress, Paper, TextField } from '@mui/material';
import * as notify from '@/app/notify';
import { getGameByID } from '@/tools/api/gamesAPI';

export default function App()
{
    const [gameID, setGameID] = React.useState<number | null>(null);
    const [checkingGame, setCheckingGame] = React.useState(false);
    const router = useRouter();

    const handleSave = async (event: React.FormEvent<HTMLFormElement>) => {
        event.preventDefault();
        notify.CloseNotification();
        // const data = new FormData(event.currentTarget);
        if (gameID === null || gameID <= 0){
            notify.ShowNotification(notify.location.Bottom, notify.Severity.Warning, "No valid game ID provided");
        }
        else {
            setCheckingGame(true);

            getGameByID(gameID).then((resp) => {
                if (resp === null) {
                    notify.ShowNotification(notify.location.Top, notify.Severity.Error, "Somthing went wrong", "Error");
                } else if (resp.status === 200) {
                    if (resp.data === null) {
                        notify.ShowNotification(notify.location.Top, notify.Severity.Error, "Somthing went wrong", "Error");
                    }
                    else {
                        router.push("/game/" + gameID);
                        setCheckingGame(false);
                        return 
                    }
                } else if (resp.status === 404){
                    notify.ShowNotification(notify.location.Bottom, notify.Severity.Warning, "Game not found!");
                    setGameID(null);
                    
                } else {
                    notify.ShowNotification(notify.location.Top, notify.Severity.Error, resp['message']['error'], "Error: " + resp.status + " " + resp.text);
                }          
            })

            setCheckingGame(false);
        }
    }
 
    return(
        <ThemeProvider theme={pageTheme}>
            <CssBaseline>
            <NavBar/>
            <Container maxWidth="sm">
                <Card
                    square={false}
                    sx={{
                        p: 2,
                        display: 'flex',
                        flexDirection: 'column',
                        alignItems: 'center',
                    }}
                >
                    <CardHeader title="Join a game" titleTypographyProps={{variant: 'h4'}}/>
                    <Divider sx={{width: "100%", size: 2}}/>

                    <CardContent>
                        <Typography variant='body1'>Enter the ID of the game you want to join.</Typography>
                        <Typography variant='body1'>The game ID is a number you&apos;ve got from the game host.</Typography>
                    </CardContent>

                    <CardActions>
                        <Box component="form" onSubmit={handleSave}>
                            <TextField
                                margin="normal"
                                required
                                fullWidth
                                label="Game ID"
                                name="gameID"
                                id='gameID'
                                autoComplete="off"
                                autoFocus
                                onChange={(event) => {
                                    const value = event.target.value.replace(/\D/g, '');
                                    var tempNumber = parseInt(value);
                                    if (!isNaN(tempNumber) && value.length <= 20) {
                                        setGameID(tempNumber);
                                    } else if (value === '') {
                                        setGameID(null);
                                    }
                                }}
                                value={gameID || ""}
                                disabled={checkingGame}
                                slotProps={{ inputLabel: { shrink: true } }}
                                error={gameID === -1}
                            />
                            <Button variant='contained' color='success' sx={{mt: 1, width: '100%'}} type='submit' disabled={checkingGame}>
                                Join
                            </Button>
                            {checkingGame && <LinearProgress/>}
                        </Box>
                    </CardActions>
                </Card>
            </Container>
            </CssBaseline>
        </ThemeProvider>
    )
}