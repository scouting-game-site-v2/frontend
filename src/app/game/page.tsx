"use client"
import React from 'react';
import { ThemeProvider } from '@mui/material/styles';
import { pageTheme } from "@/app/theme";
import Container from "@mui/material/Container";
import NavBar from '@/app/navbar';
import CssBaseline from '@mui/material/CssBaseline';
import Typography from '@mui/material/Typography';
import Card from '@mui/material/Card';
import Button from '@mui/material/Button';
import * as notify from '@/app/notify';
import { useRouter, useSearchParams } from 'next/navigation'
import { QueryNotifications } from '@/tools/notification';
import { Box, CardActions, CardContent, CardHeader, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, Divider, ListItem, Slide, TextField, Grid2 } from '@mui/material';
import { gameList, gameType } from '@/tools/gameTypes';
import { createGame } from '@/tools/api/gamesAPI';
import { TransitionProps } from '@mui/material/transitions';
import AddIcon from '@mui/icons-material/Add';


const Transition = React.forwardRef(function Transition(
    props: TransitionProps & {
        children: React.ReactElement<any, any>;
    },
    ref: React.Ref<unknown>,
) {
    return <Slide direction="up" ref={ref} {...props} />;
});

export default function App()
{
    // TODO: check if authenticated
    const router = useRouter();

    // show query notifications
    const searchParams = useSearchParams();
    React.useEffect(() => {
        QueryNotifications(searchParams);
    }, [searchParams]);

    const [open, setOpen] = React.useState(false);
    const [gameTitle, setGameTitle] = React.useState("");
    const [gameType, setGateType] = React.useState(0);

    const  handelCreategame = (gameTitle: string, gameType: gameType) => {
        setGameTitle(gameTitle);
        setGateType(gameType)
        setOpen(true);
    }

    const handleClose = () => {
        setOpen(false);
    };

    function AlertDialogSlide() {
        const handelSubmit = async (event: React.FormEvent<HTMLFormElement>) => {
            event.preventDefault();
            const data = new FormData(event?.currentTarget);
            const resp = await createGame(data.get("gameTitle") as string, gameType);

            var title = "Error";
            var message = "Something went wrong";
            if (resp == null) {
                notify.ShowNotification(notify.location.Top, notify.Severity.Error, "Response is null", "No status");
                return;
            }
            if (resp.data) {
                // status OK
                // window.location.href = "/game/" + resp.data["id"] + "/edit";
                router.push("/game/" + resp.data["id"] + "/edit");
                setOpen(false);
                return;
            } else if (resp.message) {
                // other error
                notify.ShowNotification(notify.location.Top, notify.Severity.Error, (resp?.message || "Response error"), (String(resp?.status) || "No status"));
                return;
            }
            notify.ShowNotification(notify.location.Bottom, notify.Severity.Error, message, title);
        };
        
    
        return (
            <div>
                <Dialog
                    open={open}
                    TransitionComponent={Transition}
                    keepMounted
                    onClose={handleClose}
                    aria-describedby="alert-dialog-slide-description"
                >
                    <DialogTitle>Create {gameTitle}</DialogTitle>
                        <Box component="form" onSubmit={handelSubmit} sx={{ mt: 1, flexFlow: 1}}>
                    <DialogContent>
                        <DialogContentText id="alert-dialog-slide-description">
                        Enter a name for {gameTitle}
                        </DialogContentText>
                            <TextField
                                margin="normal"
                                required
                                fullWidth
                                id="gameTitle"
                                label="Game name"
                                name="gameTitle"
                                autoComplete="off"
                                autoFocus
                                inputProps={{ maxLength: 30 }}
                                />
        
                    </DialogContent>
                    <DialogActions sx={{ justifyContent: 'space-between', px: 2 }}>
                        <Button variant='contained' color='error' onClick={handleClose}>Cancel</Button>
                        <Button variant='contained' color='success' type="submit">Create</Button>
                    </DialogActions>
                        </Box>
                </Dialog>
            </div>
        );
    }
    
    function ListGames()
    {
    
        return (
            <>
                <Grid2 container spacing={3}>
                    {Object.values(gameList).map((game) => (
                        <Grid2 size={{ xs: 12, sm: 6, md: 4 }} key={game.type}>
                            <Card sx={{ px: 2, pb: 1, m: 1}}>
                                <CardHeader title={
                                    <Typography variant="h4" align="center">{game.name}</Typography>
                                }/>
                                <Divider/>
                                <CardContent>
                                    <Typography variant="body2" color="textSecondary" component="p">
                                        {game.description}
                                    </Typography>
                                </CardContent>
                                <CardActions>
                                    <Box display="flex" justifyContent="center" width="100%">
                                        <Button variant="contained" endIcon={<AddIcon/>} onClick={() => handelCreategame(game.name, game.type)}>Create {game.name}</Button>
                                    </Box>
                                </CardActions>
                            </Card>
                        </Grid2>
                    ))}
                </Grid2>
                
            </>
        )
    }
    return(
        <>   
        <ThemeProvider theme={pageTheme}>
            <CssBaseline>
            <NavBar/>
            {AlertDialogSlide()}
            <Container maxWidth="xl">
                <Card sx={{ p: 2, m: 1}}>
                <Typography variant="h4">Create a game</Typography>
                <Typography variant="body1" gutterBottom>
                    Here you can create a game. Select a game type from the list below and click on the &quot;Create&quot; button to start creating a new game.
                </Typography>
                </Card>
                <ListGames/>
            </Container>
            </CssBaseline>
        </ThemeProvider>
        </>
    )
}