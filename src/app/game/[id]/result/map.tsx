import React, { useMemo } from 'react';
import "leaflet/dist/leaflet.css";
import { Box, LinearProgress, Paper, Typography } from '@mui/material';
import { MapContainer, TileLayer } from 'react-leaflet';
import { circleFill, GamePoint, LocationData } from '@/tools/gameData/game_1_types';
import { defaultLocation } from "@/tools/gameTypes";

var circels: LocationData[] = [];
var setCircels: React.Dispatch<React.SetStateAction<LocationData[]>>;

export default function Map({ points }: { points: null | GamePoint[] }) {
    const mapRef = React.useRef<any>(null);
    const [loadingMapData, setLoadingMapData] = React.useState(true);
    const [loading, setLoading] = React.useState(false);
    [circels, setCircels] = React.useState<LocationData[]>([]);

    var L = require('leaflet');
    const defaultZoom = 13;
    
    var mapIsLoaded = mapRef.current !== null;
    var windowsIsLoaded = typeof window !== 'undefined';

    React.useEffect(() => {
        if (windowsIsLoaded) {
            setLoading(true);
        }
    }, [windowsIsLoaded]);

    React.useEffect(() => {
        if (mapIsLoaded === true && windowsIsLoaded === true && points !== null) {
            // Remove old circles from the map
            const circlesToRemove = circels.filter(circle => !points.map(point => point).includes(circle.data));
            circlesToRemove.forEach(circle => circle.leaflet.remove());

            const newCircles = points.map((point: GamePoint) => {
                const existingCircle = circels.find(circle => circle.data === point);
                if (existingCircle) {
                    return existingCircle;
                } else if (point.settings.disabled === true) {
                    return;
                } else if (point.settings.visible === true) {
                    const leaflet: L.Circle = L.circle([point.latitude, point.longitude], {
                        color: point.color,
                        fillColor: point.color,
                        fillOpacity: circleFill.default,
                        radius: point.radius,
                    });

                    leaflet.on('click', () => {
                        const popupContent = `
                            <div style="display: flex; flex-direction: column; flex-wrap: wrap; align-content: center; align-items: center;">
                                ${point.owner === "" || point.owner == null ? `
                                    <h5 style="margin: 0; font-size: medium; font-weight: bold">No owner yet</h5>
                                ` : `
                                    <h6 style="margin: 0; font-size: medium; font-weight: normal">Owners name:</h6>
                                    <h5 style="margin: 0; font-size: medium; font-weight: bold">${point.owner}</h5>
                                `}
                            </div>
                        `;
                        const popup = L.popup()
                            .setLatLng([point.latitude, point.longitude])
                            .setContent(popupContent)
                            .openOn(mapRef.current);
                    });

                    const groupLayer = mapRef.current?.getPane('circles');
                    if (groupLayer) {
                        leaflet.addTo(groupLayer);
                    } else {
                        const newGroupLayer = L.layerGroup([leaflet]);
                        newGroupLayer.addTo(mapRef.current);
                        newGroupLayer.options.id = 'circles';
                    }
                    return { data: point, leaflet: leaflet };
                } else {
                    return;
                }
            });

            // remove undefined objects of the array
            const newCircleArray = newCircles.filter((point: LocationData | undefined): point is LocationData => {
                return typeof point !== 'undefined';
            });

            setCircels(newCircleArray);
            
            // Zoom map to fit all circles in the view
            if (newCircleArray.length > 0 && mapIsLoaded) {
                const bounds = new L.LatLngBounds(newCircleArray.map(circle => circle.leaflet.getLatLng()));
                mapRef.current.fitBounds(bounds);
            }

            setLoadingMapData(false);
        }
    }, [L, points, mapIsLoaded, windowsIsLoaded]);


    const map = (
        <div style={{ 
            minHeight: '400px', 
            height: '60vh', 
            visibility: loadingMapData ? 'hidden' : 'visible',
        }}>
            <MapContainer
                center={[defaultLocation.lat, defaultLocation.lng]} 
                zoom={defaultZoom} 
                style={{ height: '100%', width: '100%' }}
                ref={mapRef}
            >
                <TileLayer
                url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
                />
            </MapContainer>
        </div>
    );

    return (
        <Paper square={false} sx={{marginBottom: 2, overflow: 'hidden'}}>
            {/* Loading box */}
            {loadingMapData && <Box 
                sx={{
                    display: 'flex', 
                    justifyContent: 'center', 
                    alignItems: 'center', 
                    height: '60vh',
                    width: '100%',
                    position: 'absolute',
                    flexDirection: 'column',
                }}>
                    <Typography variant='h5'>Loading map...</Typography>
                    <LinearProgress sx={{width: '300px', maxWidth: '80vw'}}/>
                </Box>}
            
            {/* The map */}
            {map}
        </Paper>
    );
}