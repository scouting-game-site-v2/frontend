"use client"
import React, { Suspense, lazy } from 'react';
import { ThemeProvider } from '@mui/material/styles';
import { pageTheme } from "@/app/theme";
import Container from "@mui/material/Container";
import NavBar from '@/app/navbar';
import CssBaseline from '@mui/material/CssBaseline';
import Typography from '@mui/material/Typography';
import { Box, Button, Card, LinearProgress, Paper } from '@mui/material';
import * as notify from '@/app/notify'
import { getGameByID } from '@/tools/api/gamesAPI';
import { useRouter, useSearchParams } from 'next/navigation'
import { PageLoader } from '@/app/loader';
import { useFormatter, useTimeZone } from 'next-intl';
import { GameData, defaultColor, gameList, gameType } from '@/tools/gameTypes';
import { QueryNotifications } from '@/tools/notification';
import { getGamePoints } from '@/tools/api/gamePointsAPI';
import useWebSocket, { ReadyState } from 'react-use-websocket';
import { JoinGame, WebSocketMessage } from "@/tools/websocketTypes";
import { GamePoint } from '@/tools/gameData/game_1_types';
import dayjs from 'dayjs';
import { sanitizeFilterModel } from '@mui/x-data-grid/hooks/features/filter/gridFilterUtils';
// import Map from '@/app/game/[id]/result/map';

const LazyNavbar = lazy(() => import('@/app/navbar'));
const ResultTable = lazy(() => import('@/app/game/[id]/result/resultList'));
const LazyMap = lazy(() => import('@/app/game/[id]/result/map'));


export default function Apps({ params }: { params: Promise<{ id: string }> }){   
    // show query notifications
    const searchParams = useSearchParams();
    React.useEffect(() => {
        QueryNotifications(searchParams);
    }, [searchParams]);

    const [gameID, setGameID] = React.useState<number | null>(null);
    const { id } = React.use(params);
    React.useEffect(() => {
        if (id !== null) {
            setGameID(parseInt(id));
        }
    }, [id]);
    const router = useRouter();
    const retunUrl = searchParams.get('next_page') || "/";
    const formatter = useFormatter();


    // get the current game from the server
    const [game, setGame] = React.useState<null | GameData>(null);
    const [points, setPoints] = React.useState<null | GamePoint[]>(null);
    const [loading, setLoading] = React.useState<boolean>(true);
    const [loadingWS, setLoadingWS] = React.useState<boolean>(true);
    const [rtnParms, setRtnParms] = React.useState(new URLSearchParams(useSearchParams()));
    const [editName, setEditName] = React.useState<boolean>(false);
    const [hoverOverGameSettings, setHoverOverGameSettings] = React.useState<boolean>(false);
    const timeZone = useTimeZone();
    const [countdown, setCountdown] = React.useState(0);
    const [owenerCount, setOwnerCount] = React.useState(0);
    const [activeUsers, setActiveUsers] = React.useState(0);

    
    const calcOwnerCount = React.useCallback(() => {
        if (points === null) return;
        var ownerList: string[] = [];
        points.forEach((point: GamePoint) => {
            // check if the owner already exists
            const index = ownerList.findIndex((row) => row === point.owner);
            
            if (index === -1) {
                // add the owner
                ownerList.push(point.owner.toLowerCase());
            } else {
                // update the owner
            }
        });

        // remove the empty strings
        const index = ownerList.findIndex((row) => row === "");
        if (index !== -1) {
            ownerList.splice(index, 1);
        }

        setOwnerCount(ownerList.length);
    }, [points]);

    // Update point index value for visualisation
    React.useEffect(() => {
        if (points !== null) {
            points.forEach((point, index) => {
                point.index = index + 1;
            });
        }
    }, [points]);
    
    const getNewPoints = React.useCallback(() => {
        if (gameID === null) return;

        getGameByID(gameID).then((resp) => {
            if (resp === null) {
                return
            }

            if (resp.status === 200) {
                const gameData: GameData = resp.data;
                getAllGamePoints(gameData.id);
                setGame(gameData);
                setLoading(false);
            } else {
                rtnParms.set("severity", "error");
                rtnParms.set("message", resp['message']['error']);
                rtnParms.set("title", resp.status + " " + resp.text);
                rtnParms.set("location", "top");
                router.push(retunUrl + "?" + rtnParms.toString());
                // window.location.href = retunUrl + "?severity=error&message=" + data['message']['error'] + "&title=" + data.status + " " + data.text + "&location=top";
                return
            }
        })
        const getAllGamePoints = (gameID: number) => {
            getGamePoints(gameID).then((resp) => {
                if (resp === null) {
                    notify.ShowNotification(notify.location.Top, notify.Severity.Error, "Somthing went wrong", "Error")
                    return
                } else if (resp.status === 200) {
                    if (resp.data === null) {
                        notify.ShowNotification(notify.location.Top, notify.Severity.Error, "Somthing went wrong", "Error")
                        return
                    }
                    else {
                        notify.ShowNotification(notify.location.Bottom, notify.Severity.Success, "Locations loaded", "", 1000)
                        setPoints(resp.data);
                        return 
                    }
                }else {
                    notify.ShowNotification(notify.location.Top, notify.Severity.Error, resp['message']['error'], "Error: " + resp.status + " " + resp.text)
                    return
                }          
            })
        }

        
    }, [gameID, retunUrl, router, rtnParms]);
    
    
    const [socketUrl, setSocketUrl] = React.useState<string | null>(null);
    
    const windowIsDefined = typeof window !== 'undefined';

    React.useEffect(() => {
        if (windowIsDefined) {
            var protocol = window.location.protocol === 'https:' ? 'wss' : 'ws'
            setSocketUrl(protocol + '://' + window.location.host + '/ws');
        }
    }, [windowIsDefined]);

    const { sendMessage, lastMessage, readyState } = useWebSocket(socketUrl, {
        onOpen: () => {
            notify.ShowNotification(notify.location.Bottom, notify.Severity.Info, "Connecting websocket", "", 2000);
        },
        shouldReconnect: (closeEvent) => true,
        reconnectAttempts: 10,
        reconnectInterval: 5000,
        onError: (event) => console.error('error', event),
        onReconnectStop: (event) => {
            if (windowIsDefined) {
                window.location.reload();
            }
        },
        // share: true,
    });
    const connectionStatus = {
        [ReadyState.CONNECTING]: 'Connecting',
        [ReadyState.OPEN]: 'Connected',
        [ReadyState.CLOSING]: 'Closing',
        [ReadyState.CLOSED]: 'Closed',
        [ReadyState.UNINSTANTIATED]: 'Uninstantiated',
    }[readyState];


    const websocketHandler = React.useCallback((message: WebSocketMessage) => {
        console.log(message);
        
        switch (message.action) {
            case 'user_count':
                setActiveUsers(message.data);
                break;

            case 'new_location':
                setPoints((prevPoints: GamePoint[] | null) => {
                    return [...(prevPoints || []), message.data];
                });
                calcOwnerCount();
                break;

            case 'update_location':
                setPoints((prevPoints: GamePoint[] | null) => {
                    const index = prevPoints?.findIndex((point: GamePoint) => point.id === message.data.id) ?? -1;
                    if (index !== -1) {
                        if (prevPoints) {
                            prevPoints[index] = message.data;
                        }
                    }
                    return prevPoints ? [...prevPoints] : [];
                });
                calcOwnerCount();
                break;

            case 'remove_location':
                setPoints((prevPoints: GamePoint[] | null) => {
                    const index = prevPoints?.findIndex((point: GamePoint) => point.id === message.data.id) ?? -1;
                    if (index !== -1) {
                        if (prevPoints) {
                            prevPoints.splice(index, 1);
                        }
                    }
                    return [...(prevPoints || [])];
                });
                calcOwnerCount();
                break;  
                
            case 'join_responce':
                if (message.data.game_id === gameID){
                    setLoadingWS(false);
                    calcOwnerCount();
                }
                break;

            case 'game_update':
                if (message.data.id === gameID){
                    setGame(message.data);
                    console.log("New game date has been set");
                }
                break;

            default:
                // handle unknown message
                break;
        }
    }, [gameID, calcOwnerCount]);

    
    React.useEffect(() => {
        if (lastMessage !== null) {
            const message: WebSocketMessage = JSON.parse(lastMessage.data);
            websocketHandler(message);            
        }
    }, [lastMessage, websocketHandler]);

    const handleLeaveGame = React.useCallback(() => {
        if (readyState === ReadyState.OPEN && game !== null) {
            const message: WebSocketMessage = {
                action: 'leave_game',
                data: {
                    game_id: game.id,
                    close_conn: true    
                },
            };
            sendMessage(JSON.stringify(message));
        }
    }, [sendMessage, game, readyState]);

    const localStorageIsDefined = typeof localStorage !== "undefined";

    React.useEffect(() => {
        if (game !== null && localStorageIsDefined) {
            if (readyState === ReadyState.OPEN) {
                const joinGameMessage: JoinGame = {
                    game_id: game.id,
                    color: '',
                    game_username: localStorage.getItem('game_username') || '',
                };
                const message: WebSocketMessage = {
                    action: 'join_game',
                    data: joinGameMessage,
                };
                sendMessage(JSON.stringify(message));
            }

            // Add event listener for beforeunload event
            window.addEventListener('beforeunload', handleLeaveGame);
            
            // Remove event listener on cleanup
            return () => {
                window.removeEventListener('beforeunload', handleLeaveGame);
            };
        }
    }, [readyState, localStorageIsDefined, game, handleLeaveGame, sendMessage]);
    
    React.useEffect(() => {
        if (windowIsDefined && gameID !== null) {
            getNewPoints();
        }
    }, [gameID, getNewPoints, windowIsDefined]);
        
    React.useEffect(() => {
        if (game?.settings.enable_start_time === true) {
            const timer = setInterval(() => {
                if (game.settings.start_time !== null) {
                    const currentTime = new Date().getTime();
                    const gameStartTime = new Date(game.settings.start_time).getTime();
                    const remainingTime = gameStartTime - currentTime;
                    setCountdown(remainingTime);
                    if (remainingTime <= 0) {
                        clearInterval(timer);
                    }
                }
            }, 1000);

            return () => {
                clearInterval(timer);
            };
        }
    }, [game]);

    
    const [startTimeString, setStartTimeString] = React.useState<string>("Loading");
    const [stopTimeString, setStopTimeString] = React.useState<string>("Loading");

    const recreateTimeStrings = React.useCallback(() => {
        if (game !== null) {
            const startTime = new Date(game.settings.start_time);
            const stopTime = new Date(game.settings.stop_time);
            const startTimeString = startTime.toLocaleString(navigator.language);
            const stopTimeString = stopTime.toLocaleString(navigator.language);
            const now = new Date(); // Add this line to get the current time
            const timeUntilStart = formatter.relativeTime(dayjs(startTime.toLocaleString("en-US")).toDate(), now);
            const timeUntilStop = formatter.relativeTime(dayjs(stopTime.toLocaleString("en-US")).toDate(), now);

            if (game.settings.enable_start_time === true) {
                setStartTimeString(timeUntilStart);
            } else {
                setStartTimeString("Not set");
            }

            if (game.settings.enable_stop_time === true) {
                setStopTimeString(timeUntilStop);
            } else {
                setStopTimeString("Not set");
            }
        }
    }, [formatter, game]);

    React.useEffect(() => {
        const interval = setInterval(() => {
            recreateTimeStrings();
        }, 1000); 

        recreateTimeStrings();

        return () => {
            clearInterval(interval);
        };
    }, [game, recreateTimeStrings]);

    const LoadingGameWindow = (
        
        <Container maxWidth="sm">
            <Paper square={false} sx={{p: 4}}>
                <Typography variant='h5' sx={{padding: 1, display: 'flex', justifyContent: 'center'}}>
                    Loading the game results...
                </Typography>
                <Typography variant='body1'sx={{padding: 1, display: 'flex', justifyContent: 'center'}}>
                    The game results are loading in the background, please wait.
                </Typography>
                <LinearProgress/>
            </Paper>
        </Container>
    );

    const GameInfo = (
        <Box sx={{padding: 1, display: 'flex', justifyContent: 'space-around'}}>
            <Box sx={{display: 'flex', justifyContent: 'center', flexWrap: 'wrap'}}>
                <Typography variant='body1' sx={{mx: 0.5, textAlign: 'center'}}>Amount of locations:</Typography>
                <Typography variant='body1' sx={{mx: 0.5, fontWeight: 'bold'}}>{points?.length}</Typography>
            </Box>

            <Box sx={{display: 'flex', justifyContent: 'center', flexWrap: 'wrap'}}>
                <Typography variant='body1' sx={{mx: 0.5, textAlign: 'center'}}>Amount of players:</Typography>
                <Typography variant='body1' sx={{mx: 0.5, fontWeight: 'bold'}}>{owenerCount}</Typography>
            </Box>

            <Box sx={{display: 'flex', justifyContent: 'center', flexWrap: 'wrap'}}>
                <Typography variant='body1' sx={{mx: 0.5, textAlign: 'center'}}>Active users:</Typography>
                <Typography variant='body1' sx={{mx: 0.5, fontWeight: 'bold'}}>{activeUsers}</Typography>
            </Box>
        </Box>
    );

    const DisplayResult = (
        <>
            <ResultTable points={points}/>
        </>
    );

    const DisplayMap = (
        <>
            {windowIsDefined && <LazyMap points={points}/>}
        </>
    );

    const GameIsNotActive = (
        <>
            <Typography variant='h5' sx={{padding: 1, display: 'flex', justifyContent: 'center'}}>
                The game is not active
            </Typography>
            <Typography variant='body1'sx={{padding: 1, display: 'flex', justifyContent: 'center'}}>
                The game is not active, please try again later.
            </Typography>
        </>
    );

    const GameIsNotYetStarted = (
        <>
            <Typography variant='h5' sx={{padding: 1, display: 'flex', justifyContent: 'center'}}>
                The game has not yet started
            </Typography>
            <Typography variant='body1'sx={{padding: 1, display: 'flex', justifyContent: 'center'}}>
                The game will start {startTimeString}.
            </Typography>
        </>
    );

    const GameIsEnded = (
        <>
            <Typography variant='h5' sx={{padding: 1, display: 'flex', justifyContent: 'center'}}>
                The game has ended
            </Typography>
            <Typography variant='body1'sx={{padding: 1, display: 'flex', justifyContent: 'center'}}>
                The game has ended, view the final results below.
            </Typography>
        </>
    );

    const GameIsRunning = (
        <>
            <Typography variant='h5' sx={{padding: 1, display: 'flex', justifyContent: 'center'}}>
                The game is running
            </Typography>
            <Typography variant='body1'sx={{padding: 1, display: 'flex', justifyContent: 'center'}}>
                The game still is running, view the current results below. {game?.settings.enable_stop_time === true ? "The game will end " + stopTimeString + "." : ""}
            </Typography>
            <Typography variant='body1' sx={{padding: 1, pt: 0, display: 'flex', justifyContent: 'center'}}>
                The results can change while the game is running.
            </Typography>
        </>
    );
    
    const [isGameEnded, setIsGameEnded] = React.useState(false);

    React.useEffect(() => {
        const checkStopTime = () => {
            if (game?.settings.enable_stop_time && new Date(game.settings.stop_time).getTime() < new Date().getTime()) {
                setIsGameEnded(true);
            }
        };
    
        const timer = game?.settings.enable_stop_time === true ? setInterval(checkStopTime, 1000) : null;

        return () => {
            if (timer) {
                clearInterval(timer);
            }
        };
    }, [game]);

    // Loading paper
    const loadingPaper = (
        <Paper 
            square={false} 
            sx={{
                padding: 2, 
                marginBottom: 2,
                minWidth: 'fit-content',
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center',
                justifyContent: 'center',
            }}
            >
            <Typography variant='h6'>loading...</Typography>
            <LinearProgress sx={{width: '50%', my: 2, minWidth: 200}}/>
        </Paper>
    );

    return(
        <ThemeProvider theme={pageTheme}>
            <CssBaseline>
                <Suspense fallback={loadingPaper}>
                    <LazyNavbar />
                </Suspense>
                <Container maxWidth="xl">
                    {(loading || loadingWS || game === null) ? LoadingGameWindow :
                        <Box>
                            <Paper square={false}  sx={{p: 2}}>
                                <Typography variant='h4' sx={{padding: 1, display: 'flex', justifyContent: 'center'}}>
                                    {game.name}
                                </Typography>
                                <Typography variant='body1' sx={{padding: 1, display: 'flex', justifyContent: 'center'}}>
                                    {gameList[game.game_type].name + ": " + gameList[game.game_type].description}
                                </Typography>
                                <hr style={{backgroundColor: pageTheme.palette.divider, color: pageTheme.palette.divider, height: '2px', borderWidth: 0}} />

                                {(game.settings.active === false) ? GameIsNotActive :
                                    (game.settings.enable_start_time === true && countdown > 0 ) ? GameIsNotYetStarted :
                                    (game.settings.enable_stop_time === true && isGameEnded) ? GameIsEnded :
                                    GameIsRunning
                                }
                        
                                <hr style={{backgroundColor: pageTheme.palette.divider, color: pageTheme.palette.divider, height: '2px', borderWidth: 0}} />
                                {GameInfo}
                            </Paper>

                            <Paper square={false} sx={{mt: 2}}>
                                <Suspense fallback={
                                    <Box sx={{padding: 1, minHeight: '10rem', display: 'flex', justifyContent: 'center', alignItems: 'center', flexDirection: 'column'}}>
                                        <Typography variant='h5'>Loading the results table...</Typography>
                                        <LinearProgress sx={{width: '60%', minWidth: 200}}/>
                                    </Box>
                                }>
                                    {DisplayResult}
                                </Suspense>
                            </Paper>
                        </Box>
                    }
                    <Paper square={false} sx={{mt: 2, position: 'relative'}}>
                        <Suspense fallback={
                            <Box 
                            sx={{
                                display: 'flex', 
                                justifyContent: 'center', 
                                alignItems: 'center', 
                                height: '60vh',
                                width: '100%',
                                position: 'absolute',
                                flexDirection: 'column',
                            }}>
                                <Typography variant='h5'>Loading map...</Typography>
                                <LinearProgress sx={{width: '300px', maxWidth: '80vw'}}/>
                            </Box>
                        }>
                            {DisplayMap}
                        </Suspense>
                        
                    </Paper>
                </Container>
            </CssBaseline>
        </ThemeProvider>
    );
}
