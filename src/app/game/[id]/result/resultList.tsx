// "use client"
import React, { use } from 'react';
import Card from '@mui/material/Card';
import Typography from '@mui/material/Typography';
import Paper from '@mui/material/Paper';
import Box from '@mui/material/Box';
import { useNow, useFormatter, useTimeZone } from 'next-intl';
import { DataGrid, GridColDef, GridExpandMoreIcon } from '@mui/x-data-grid'
import { styled } from '@mui/material/styles';
import { ExpandLess, ExpandMore, Sort, TempleBuddhist } from '@mui/icons-material';
import { pageTheme } from '@/app/theme';
import { colorList, defaultColor } from '@/tools/gameTypes';
import { GamePoint } from '@/tools/gameData/game_1_types';
import * as Icon from '@mui/icons-material';

var rows: TabelData[] = [];
var setRows: React.Dispatch<React.SetStateAction<TabelData[]>>;

const colorArray = [defaultColor, ...Object.values(colorList)];


const column: GridColDef[] = [
    { 
        field: 'place',     
        headerName: 'Place',            
        type: 'number',
        minWidth: 100,
        maxWidth: 200,
        flex: 1,
        align: 'center',
        headerAlign: 'center',
        renderCell: (params) => {
            return (
                <>
                <span style={{ fontSize: "20px" }}>#<span style={{ fontWeight: 'bold' }}>{params.value}</span></span>
                </>
            );
        }
    },
    { 
        field: 'owner',   
        headerName: 'Name',      
        type: 'string',
        minWidth: 170,
        flex: 1,
        align: 'center',
        headerAlign: 'center',
    },
    { 
        field: 'color',       
        headerName: 'Color',
        minWidth: 150,
        maxWidth: 200,
        flex: 1,
        headerAlign: 'center',    
        align: 'center',
        renderCell: (params) => {
            const colorIndex = colorArray.findIndex(color => color.name === params.value);
            const color = colorIndex !== -1 ? colorArray[colorIndex].color : params.value;
            const colorName = colorIndex !== -1 ? colorArray[colorIndex].name : params.value;
            return (
                <>
                    <span style={{ backgroundColor: color, width: "20px", height: "20px", borderRadius: "50%", display: "inline-block", marginRight: "5px", verticalAlign: "middle" }}></span>
                    <span style={{ verticalAlign: "middle" }}>{colorName}</span>
                </>
            );
        }
    },
    {
        field: 'locationCount',
        headerName: 'Amount of locations',
        type: 'number',
        minWidth: 170,
        flex: 1,
        align: 'center',
        headerAlign: 'center',
    },
];

interface TabelData {
    id: number
    place: number
    owner: string
    color: string
    locationCount: number
}

function addTabelData(
    id: number,
    point: GamePoint,
): TabelData {
    const colorIndex = colorArray.findIndex(color => color.color === point.color);
    const colorName = colorIndex !== -1 ? colorArray[colorIndex].name : point.color;

    const data: TabelData = {
        id: id,
        place: 0,
        owner: point.owner,
        color: colorName,
        locationCount: 1,
    };
    return data;    
}

export default function ResultTable({points} : {points: null | GamePoint[]}) {
    // console.log(points);
    [rows, setRows] = React.useState<TabelData[]>([]);
    const formatter = useFormatter();
    const now = useNow();
    const timeZone = useTimeZone();

    // format the date
    // console.log(points.points);
    React.useEffect(() => {
        if (points === null) return;
        // setRows([]);
        var tempRows: TabelData[] = [];
        points.forEach((point: GamePoint) => {
            // check if the owner already exists
            const index = tempRows.findIndex((row) => row.owner === point.owner);
            
            if (index === -1) {
                // add the owner
                tempRows.push(addTabelData(tempRows.length, point));
            } else {
                // update the owner
                tempRows[index].locationCount += 1;
            }
        });

        // remove the default color
        const index = tempRows.findIndex((row) => row.color === defaultColor.name);
        if (index !== -1) {
            tempRows.splice(index, 1);
        }
        
        // calculate the place
        tempRows.sort((a, b) => {
            return b.locationCount - a.locationCount;
        });

        // set the place
        tempRows.forEach((row, index) => {
            row.place = index + 1;
        });

        setRows(tempRows);
    }, [points]);

    // console.log(this.props.onRowSelection)
    return (
        <>
            {/* {AlertDialogSlide()} */}
        <Paper square={false} sx={{padding: 2, marginBottom: 2}}>
            <Typography variant="h5" sx={{ fontWeight: 'bold' }}>
                Results
            </Typography>
            
            {/* <hr style={{backgroundColor: pageTheme.palette.divider, color: pageTheme.palette.divider, height: '2px', borderWidth: 0}} /> */}
            
            <Box sx={{
                width: '100%',
                overflow: 'hidden',
                mt: 2
            }}>
                <Box>
                    <DataGrid
                        slots={{
                            columnSortedDescendingIcon: () => <ExpandMore className="icon" />,
                            columnSortedAscendingIcon: () => <ExpandLess className="icon" />,
                            columnUnsortedIcon: () => <Sort className="icon" />,
                        }}
                        rows={rows}
                        columns={column}
                        initialState={{
                            pagination: {
                                paginationModel: {
                                    pageSize: 10
                                }
                            },
                            sorting: {
                                sortModel: [{ field: 'place', sort: 'asc' }],
                            }
                        }}
                        sx={{ '--DataGrid-overlayHeight': '400px', height: rows.length == 0 ? '400px' : '' }}
                        pageSizeOptions={[10, 25, 100]}
                        hideFooterSelectedRowCount
                        // TODO: go to te page where the row is selected
                    />
                </Box>
            </Box>
        </Paper>
        </>
    );
}	