"use client"
import React, { Suspense, lazy } from 'react';
import { ThemeProvider } from '@mui/material/styles';
import { pageTheme } from "@/app/theme";
import Container from "@mui/material/Container";
import NavBar from '@/app/navbar';
import CssBaseline from '@mui/material/CssBaseline';
import Typography from '@mui/material/Typography';
import { Button, Card, CardActions, CardContent, CardHeader, Divider, Box, LinearProgress, Paper } from '@mui/material';
import CelebrationIcon from '@mui/icons-material/Celebration';
import * as notify from '@/app/notify'
import { getGameByID } from '@/tools/api/gamesAPI';
import { useRouter, useSearchParams } from 'next/navigation'
import { PageLoader } from '@/app/loader';
import { useTimeZone } from 'next-intl';
import { GameData, gameType } from '@/tools/gameTypes';
import { QueryNotifications } from '@/tools/notification';
import { getGamePoints } from '@/tools/api/gamePointsAPI';
import useWebSocket, { ReadyState } from 'react-use-websocket';
import { JoinGame, WebSocketMessage } from "@/tools/websocketTypes";
import { GamePoint } from '@/tools/gameData/game_1_types';
import LoadingGameWindow from '@/app/loadingGame';


const Game_1 = lazy(() => import('@/app/game/[id]/game_1'));
const Game_2 = lazy(() => import('@/app/game/[id]/game_2'));


export interface TheWebsocket {
    sendMessage: any;
    lastMessage: any;
    readyState: any;
    connectionStatus: any;
}

export default function Apps({ params }: { params: Promise<{ id: string }> }){    
    // show query notifications
    const searchParams = useSearchParams();
    React.useEffect(() => {
        QueryNotifications(searchParams);
    }, [searchParams]);
    
    const [gameID, setGameID] = React.useState<number | null>(null);
    const { id } = React.use(params);
    React.useEffect(() => {
        if (id !== null) {
            setGameID(parseInt(id));
        }
    }, [id]);
    const router = useRouter();
    const retunUrl = searchParams.get('next_page') || "/";

    // get the current game from the server
    const [game, setGame] = React.useState<null | GameData>(null);
    const [points, setPoints] = React.useState<null | GamePoint[]>(null);
    const [loading, setLoading] = React.useState<boolean>(true);
    const [loadingWS, setLoadingWS] = React.useState<boolean>(true);
    const [rtnParms, setRtnParms] = React.useState(new URLSearchParams(useSearchParams()));
    const [editName, setEditName] = React.useState<boolean>(false);
    const timeZone = useTimeZone();
    const [countdown, setCountdown] = React.useState(0);

    // Update point index value for visualisation
    React.useEffect(() => {
        if (points !== null) {
            points.forEach((point, index) => {
                point.index = index + 1;
            });
        }
    }, [points]);
    
    // check if authenticated
    const getNewPoints = React.useCallback(() => {
        if (gameID === null) return;

        getGameByID(gameID).then((resp) => {
            if (resp === null) {
                return
            }

            if (resp.status === 200) {
                const gameData: GameData = resp.data;
                getAllGamePoints(gameData.id);
                setGame(gameData);
                setLoading(false);
            } else {
                rtnParms.set("severity", "error");
                rtnParms.set("message", resp['message']['error']);
                rtnParms.set("title", resp.status + " " + resp.text);
                rtnParms.set("location", "top");
                router.push(retunUrl + "?" + rtnParms.toString());
                // window.location.href = retunUrl + "?severity=error&message=" + data['message']['error'] + "&title=" + data.status + " " + data.text + "&location=top";
                return
            }
        })
        const getAllGamePoints = (gameID: number) => {
            getGamePoints(gameID).then((resp) => {
                if (resp === null) {
                    notify.ShowNotification(notify.location.Top, notify.Severity.Error, "Somthing went wrong", "Error")
                    return
                } else if (resp.status === 200) {
                    if (resp.data === null) {
                        notify.ShowNotification(notify.location.Top, notify.Severity.Error, "Somthing went wrong", "Error")
                        return
                    }
                    else {
                        notify.ShowNotification(notify.location.Bottom, notify.Severity.Success, "New locations loaded", "", 1000)
                        setPoints(resp.data);
                        return 
                    }
                }else {
                    notify.ShowNotification(notify.location.Top, notify.Severity.Error, resp['message']['error'], "Error: " + resp.status + " " + resp.text)
                    return
                }          
            })
        }
    }, [gameID, retunUrl, router, rtnParms]);
    
    
    const [socketUrl, setSocketUrl] = React.useState<string | null>(null);

    const windowIsDefined = typeof window !== 'undefined';

    React.useEffect(() => {
        if (windowIsDefined) {
            const protocol = window.location.protocol === 'https:' ? 'wss' : 'ws';
            setSocketUrl(protocol + '://' + window.location.host + '/ws');
        }
    }, [windowIsDefined]);

    const { sendMessage, lastMessage, readyState } = useWebSocket(socketUrl, {
        onOpen: () => {
            notify.ShowNotification(notify.location.Bottom, notify.Severity.Info, "Connecting websocket", "", 2000);
        },
        shouldReconnect: (closeEvent) => true,
        reconnectAttempts: 10,
        reconnectInterval: 5000,
        onError: (event) => console.error('error', event),
        onReconnectStop: (event) => {
            if (windowIsDefined) {
                window.location.reload();
            }
        },
        // share: true,
    });
    const connectionStatus = {
        [ReadyState.CONNECTING]: 'Connecting',
        [ReadyState.OPEN]: 'Connected',
        [ReadyState.CLOSING]: 'Closing',
        [ReadyState.CLOSED]: 'Closed',
        [ReadyState.UNINSTANTIATED]: 'Uninstantiated',
    }[readyState];

    const theWebsocket: TheWebsocket = {
        connectionStatus: connectionStatus,
        lastMessage: lastMessage,
        readyState: readyState,
        sendMessage: sendMessage
    }

    // React.useEffect(() => {
    //     console.log("Current websocket state: ", connectionStatus);

    //     const retryInterval = setInterval(() => {
    //         if (readyState !== ReadyState.CONNECTING && readyState !== ReadyState.OPEN) {
    //             const protocol = window.location.protocol === 'https:' ? 'wss' : 'ws';
    //             setSocketUrl(protocol + '://' + window.location.host + '/ws');
    //             notify.ShowNotification(notify.location.Bottom, notify.Severity.Info, "Connecting websocket", "", 2000);
    //         }
    //     }, 10000);
        
    //     if (readyState === ReadyState.CLOSED) {
    //         notify.ShowNotification(notify.location.Bottom, notify.Severity.Warning, "Websocket connection is closed");
    //     }
    //     return () => clearInterval(retryInterval);
    // }, [connectionStatus, readyState]);
    


    const websocketHandler = React.useCallback((message: WebSocketMessage) => {
        console.log(message);
        
        switch (message.action) {
            case 'new_location':
                setPoints((prevPoints: GamePoint[] | null) => {
                    return [...(prevPoints || []), message.data];
                });
                break;

            case 'update_location':
                setPoints((prevPoints: GamePoint[] | null) => {
                    const index = prevPoints?.findIndex((point: GamePoint) => point.id === message.data.id) ?? -1;
                    if (index !== -1) {
                        if (prevPoints) {
                            prevPoints[index] = message.data;
                        }
                    }
                    return prevPoints ? [...prevPoints] : [];
                });
                break;

            case 'remove_location':
                setPoints((prevPoints: GamePoint[] | null) => {
                    const index = prevPoints?.findIndex((point: GamePoint) => point.id === message.data.id) ?? -1;
                    if (index !== -1) {
                        if (prevPoints) {
                            prevPoints.splice(index, 1);
                        }
                    }
                    return [...(prevPoints || [])];
                });
                break;  
                
            case 'join_responce':
                if (message.data.game_id === gameID){
                    setLoadingWS(false);
                }
                break;

            case 'game_update':
                if (message.data.id === gameID){
                    setGame(message.data);
                    console.log("New game date has been set");
                }
                break;

            default:
                // handle unknown message
                break;
        }
    }, [gameID]);

    
    React.useEffect(() => {
        if (lastMessage !== null) {
            const message: WebSocketMessage = JSON.parse(lastMessage.data);
            websocketHandler(message);            
        }
    }, [lastMessage, websocketHandler]);

    const handleLeaveGame = React.useCallback(() => {
        if (readyState === ReadyState.OPEN && game !== null) {
            const message: WebSocketMessage = {
                action: 'leave_game',
                data: {
                    game_id: game.id,
                    close_conn: true    
                },
            };
            sendMessage(JSON.stringify(message));
        }
    }, [sendMessage, game, readyState]);

    const localStorageIsDefined = typeof localStorage !== "undefined";

    React.useEffect(() => {
        if (game !== null && localStorageIsDefined) {
            // Add event listener for beforeunload event
            window.addEventListener('beforeunload', handleLeaveGame);
            
            // Remove event listener on cleanup
            return () => {
                window.removeEventListener('beforeunload', handleLeaveGame);
            };
        }
    }, [readyState, localStorageIsDefined, game, handleLeaveGame, sendMessage]);
    
    const wsReady = readyState === ReadyState.OPEN;
    React.useEffect(() => {
        if (windowIsDefined && wsReady) {
            console.log("--- Getting point data ---");
            getNewPoints();
        }
    }, [gameID, getNewPoints, windowIsDefined, wsReady]);
        
    React.useEffect(() => {
        if (game?.settings.enable_start_time === true) {
            const timer = setInterval(() => {
                if (game.settings.start_time !== null) {
                    const currentTime = new Date().getTime();
                    const gameStartTime = new Date(game.settings.start_time).getTime();
                    const remainingTime = gameStartTime - currentTime;
                    setCountdown(remainingTime);
                    if (remainingTime <= 0) {
                        clearInterval(timer);
                    }
                }
            }, 1000);

            return () => {
                clearInterval(timer);
            };
        }
    }, [game]);

    const formatTime = (time: number) => {
        const seconds = Math.floor((time % 60000) / 1000);
        const minutes = Math.floor(time / 60000);
        const hours = Math.floor(minutes / 60);
        if (hours > 24) {
            return `${Math.floor(hours / 24)} days`;
        } else if (hours > 0) {
            return `${hours.toString().padStart(2, '0')}:${minutes.toString().padStart(2, '0')}:${seconds.toString().padStart(2, '0')}`;
        }
        return `${minutes.toString().padStart(2, '0')}:${seconds.toString().padStart(2, '0')}`;
    };

    const GameIsNotActive = (
        <Container maxWidth="sm">
            <Paper square={false}  sx={{p: 2}}>
                <Typography variant='h5' sx={{padding: 1, display: 'flex', justifyContent: 'center'}}>
                    The game is not active
                </Typography>
                <Divider sx={{width: '100%', m: 2}}/>
                <Typography variant='body1'sx={{px: 1, pt: 1, display: 'flex', justifyContent: 'center'}}>
                    The game is not active, you can not play this game.
                </Typography>
                <Typography variant='body1'sx={{px: 1, display: 'flex', justifyContent: 'center'}}>
                    Please contact&nbsp;<strong>{game?.owner_name ? '"' + game.owner_name.trim() + '"' : "the game owner"}</strong>&nbsp;to activate the game.
                </Typography>
            </Paper>
        </Container>
    );

    const TheGame = (
        <Box maxWidth="xl" width="100%">
            {game?.game_type === gameType.Game_1 ? (
            <Suspense fallback={<LoadingGameWindow/>}>
                <Game_1 game={game} router={router} points={points || []} websocketConn={theWebsocket} loadingWS={loadingWS} />           
            </Suspense>
            ) : game?.game_type === gameType.Game_2 ? (
            <Suspense fallback={<LoadingGameWindow/>}>
                <Game_2 game={game} />
            </Suspense>
            ) : (
            <Typography variant='h6'>Game type {game?.game_type} not implemented</Typography>
            )}
        </Box>
    );

    const GameIsNotYetStarted = (
        <Container maxWidth="sm">
            <Paper square={false}  sx={{p: 2}}>
                <Typography variant='h5' sx={{padding: 1, display: 'flex', justifyContent: 'center'}}>
                    The game is not yet started
                </Typography>
                <Typography variant='body1'sx={{px: 1, pt: 1, display: 'flex', justifyContent: 'center'}}>
                    The game is not yet started, you can not play till the timer is expired.
                </Typography>
                </Paper>
            <Paper square={false}  sx={{p: 2, mt: 2}}>
                <Typography variant='h5' sx={{ padding: 1, display: 'flex', justifyContent: 'center' }}>
                    Game starts in:
                </Typography>
                <Typography variant='h1' sx={{ padding: 1, display: 'flex', justifyContent: 'center' }}>
                    {formatTime(countdown)}
                </Typography>
            </Paper>
        </Container>
    );

    const GameIsEnded = (
        <Container maxWidth="sm">
            <Card square={false} sx={{ p: 1, display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
                <CardHeader title="The game has ended" />
                <Divider sx={{width: '100%'}}/>
                <CardContent>
                    <Typography variant='body1' sx={{ px: 1, pt: 1, display: 'flex', justifyContent: 'center' }}>
                        The game has ended, you can not play this game anymore.
                    </Typography>
                    <Typography variant='body2' sx={{ px: 1, display: 'flex', justifyContent: 'center' }}>
                        Please contact&nbsp;<strong>{game?.owner_name ? '"' + game.owner_name.trim() + '"' : "the game owner"}</strong>&nbsp;to activate the game.
                    </Typography>
                </CardContent>
                <CardActions sx={{ justifyContent: 'center' }}>
                    <Button variant='contained' color='primary' onClick={() => router.push("/game/" + gameID + "/result")}>
                        View Results! <CelebrationIcon sx={{ml: 1}}/>
                    </Button>
                </CardActions>
            </Card>
        </Container>
    );

    
    const [isGameEnded, setIsGameEnded] = React.useState(false);

    React.useEffect(() => {
        const checkStopTime = () => {
            if (game?.settings.enable_stop_time && new Date(game.settings.stop_time).getTime() < new Date().getTime()) {
                setIsGameEnded(true);
            } else {
                setIsGameEnded(false);
            }
        };
    
        const timer = game?.settings.enable_stop_time === true ? setInterval(checkStopTime, 1000) : null;
        checkStopTime();

        return () => {
            if (timer) {
                clearInterval(timer);
            }
        };
    }, [game]);

    return(
        <ThemeProvider theme={pageTheme}>
            <CssBaseline>
                <NavBar/>
                <Box sx={{display: 'flex', justifyContent: 'center', alignItems: 'center'}}>
                    {(loading || game === null) ? <LoadingGameWindow/> :
                        (game.settings.active === false) ? GameIsNotActive :
                            (game.settings.enable_start_time === true && countdown > 0 ) ? GameIsNotYetStarted :
                                (game.settings.enable_stop_time === true && isGameEnded) ? GameIsEnded :
                                    (game.settings.enable_start_time === true && countdown <= 0 || game.settings.enable_start_time === false) ? TheGame :
                                        <LoadingGameWindow/>
                    }
                </Box>
            </CssBaseline>
        </ThemeProvider>
    );
}
