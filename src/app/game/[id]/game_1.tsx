import React, { Suspense } from "react";
import { colorList, GameData } from "@/tools/gameTypes";
import { Box, Button, Container, Grid2, LinearProgress, Paper, Typography, getContrastRatio, Checkbox, FormControlLabel, Menu, MenuItem, Icon, IconButton, Divider } from "@mui/material";
import { signal } from "@preact/signals"
import { darken } from "@mui/material/styles";
// import Map from "@/app/game/[id]/map";
import { TheWebsocket } from "@/app/game/[id]/page";
import { JoinGame, WebSocketMessage } from "@/tools/websocketTypes";
import { ReadyState } from 'react-use-websocket';
import { GamePoint, LocationData } from "@/tools/gameData/game_1_types";
import { GridMenuIcon } from "@mui/x-data-grid";
import dayjs from "dayjs";
import { useFormatter } from "next-intl";
import LoadingGameWindow from "@/app/loadingGame";

var theWebsocket: TheWebsocket;


const Map = React.lazy(() => import("@/app/game/[id]/map"));

interface menuOptions {
    showUsername: boolean;
    showGameName: boolean;
    showConnectionStatus: boolean;
    lockGeo: boolean;
    showTimeRemaining: boolean;
}


export const updateLocation = (location: LocationData) => {
    if (theWebsocket.readyState === ReadyState.OPEN) {
        const message: WebSocketMessage = {
            action: 'update_location',
            data: {
                game_id: location.data.game_id,
                location_id: location.data.id
            },
        };
        theWebsocket.sendMessage(JSON.stringify(message));
    }
}

export default function Game_1({ game, points, router, websocketConn, loadingWS }: { game: GameData, points: GamePoint[], router: any, websocketConn: TheWebsocket, loadingWS: boolean}) {
    const [userColor, setUserColor] = React.useState<null | string>(null);
    const [gameUsername, setGameUsername] = React.useState("");
    const [activeColors, setActiveColors] = React.useState<string[]>([]);

    const formatter = useFormatter();
    theWebsocket = websocketConn;

    const [menuOptions, setMenuOptions] = React.useState<menuOptions>({
        showUsername: true,
        showGameName: true,
        showConnectionStatus: true,
        lockGeo: true,
        showTimeRemaining: true,
    });
    const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);

    const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    // Load menu options from localStorage
    function loadMenuOptions() {
        if (typeof window !== "undefined" && typeof localStorage !== "undefined") {
            const tmp = localStorage.getItem("menuOptions");
            if (tmp !== null) {
                setMenuOptions(JSON.parse(tmp));
            }
        }
    }

    React.useEffect(() => {
        if (theWebsocket.lastMessage !== null) {
            const message: WebSocketMessage = JSON.parse(theWebsocket.lastMessage.data);
            websocketHandler(message);            
        }
    }, [websocketConn.lastMessage]);

    function websocketHandler(message: WebSocketMessage){
        switch (message.action) {
                case 'active_colors':
                    setActiveColors(message.data)
                    break;
            default:
                // handle unknown message
                break;
        }
    }

    const sendGetActiveColors = React.useCallback(() => {
        if (theWebsocket.readyState === ReadyState.OPEN) {
            const message: WebSocketMessage = {
                action: 'get_active_colors',
                data: {
                    game_id: game.id   
                },
            };
            theWebsocket.sendMessage(JSON.stringify(message));
        }
    }, [game.id]);

    const setTheUserColor = (color: string) => {
        setUserColor(color);
        if (theWebsocket.readyState === ReadyState.OPEN) {
            const message: WebSocketMessage = {
                action: 'set_active_color',
                data: {
                    game_id: game.id,
                    color: color
                },
            };
            theWebsocket.sendMessage(JSON.stringify(message));
        }
    }

    const isLocalStorageDefined = typeof window !== "undefined" && typeof localStorage !== "undefined";
    const wsReady = theWebsocket.readyState === ReadyState.OPEN;

    const sendJoin = React.useCallback((username: string) => {
        if (wsReady) {
            const joinGameMessage: JoinGame = {
                game_id: game.id,
                color: userColor || '',
                game_username: username,
            };
            const message: WebSocketMessage = {
                action: 'join_game',
                data: joinGameMessage,
            };
            theWebsocket.sendMessage(JSON.stringify(message));
        }
    }, [wsReady, game.id, userColor]);

    React.useEffect(() => {
        if (isLocalStorageDefined && wsReady) {
            const game_username = localStorage.getItem("game_username");
            if (game_username === null || game_username === "") {
                router.push("/account/game_username?next_page=/game/" + game.id)
            } 
            else {
                console.log("init")
                sendJoin(game_username);
                setGameUsername(game_username);
                sendGetActiveColors();
                loadMenuOptions();
            }
        }
    }, [isLocalStorageDefined, wsReady, game.id, router, sendGetActiveColors, sendJoin]);

    
    const [availibleColors, setAvailibleColors] = React.useState(Object.values(colorList));
    const [loadingActiveColors, setLoadingActiveColors] = React.useState(true);

    React.useEffect(() => {
        if (activeColors.length !== 0){
            var tmp = Object.values(colorList);
            
            activeColors.forEach(color => {
                const index = tmp.findIndex((colorObj: { color: string; name: string; }) => colorObj.color === color);
                // remove color from colorArray
                if (index !== -1) {
                    tmp.splice(index, 1);
                    // setAvailibleColors([...availibleColors]);	
                }
            });

            setAvailibleColors(tmp);
            setLoadingActiveColors(false);
        }
    }, [activeColors]);
    
    // Calculate the time remaining
    const [timeRemainingString, setTimeRemainingString] = React.useState<string>("Loading...");

    
    const recreateTimeStrings = React.useCallback(() => {
        if (game !== null) {
            const stopTime = new Date(game.settings.stop_time);
            const now = new Date();
            const timeUntilStop = formatter.relativeTime(dayjs(stopTime.toLocaleString("en-US")).toDate(), now);

            if (game.settings.enable_stop_time === true) {
                setTimeRemainingString(timeUntilStop);
            } else {
                setTimeRemainingString("Not set");
            }
        }
    }, [formatter, game]);

    React.useEffect(() => {
        const interval = setInterval(() => {
            recreateTimeStrings();
        }, 1000); 

        recreateTimeStrings();

        return () => {
            clearInterval(interval);
        };
    }, [game, recreateTimeStrings]);

    function SelectColorWindow() {
        return (
        <>
            <Container maxWidth="sm">
            <Typography variant="h4" sx={{mb: 1}}>Welcome to the game: <strong>{game.name}</strong></Typography>
            {loadingActiveColors ? 
            <LoadingGameWindow/>
            :
            <>
                <Paper square={false} sx={{p:2}}>
                    <Typography variant="h5">Choose your color</Typography>
                        <hr/>
                    <Grid2 container spacing={2}>
                        {availibleColors.length == 0 ? <>
                            <Typography variant="h5" p={2}>No colors avalible!</Typography>
                        </> 
                        : availibleColors.map((color: { color: string; name: string; }, index: number) => {
                            const contrast = getContrastRatio(color.color, "#fff");
                            const textColor = contrast >= 4.5 ? "#fff" : "#000";
                            return (
                                <Grid2 key={index}>
                                    <Button
                                        variant="contained"
                                        onClick={() => setTheUserColor(color.color)}
                                        sx={{
                                            backgroundColor: color.color,
                                            color: textColor,
                                            "&:hover": {
                                                backgroundColor: darken(color.color, 0.4) // darken the color by 20%
                                            }
                                        }}
                                    >
                                        {color.name}
                                    </Button>
                                </Grid2>
                            );
                        })}
                    </Grid2>
                </Paper>
            </>
            }
        </Container>
    </>
        )
    };


    const OptionsMenu = (
        <Menu
            anchorEl={anchorEl}
            open={Boolean(anchorEl)}
            onClose={handleClose}
        >
            <MenuItem>
                <FormControlLabel
                    control={
                        <Checkbox
                        checked={menuOptions.showUsername}
                        onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
                            setMenuOptions({...menuOptions, showUsername: event.target.checked});
                            if (typeof localStorage !== "undefined") {
                                localStorage.setItem("menuOptions", JSON.stringify({...menuOptions, showUsername: event.target.checked}));
                            }
                        }}
                        />
                    }
                    label="Show username"
                    />
            </MenuItem>

            <MenuItem>
                <FormControlLabel
                    control={
                        <Checkbox
                        checked={menuOptions.showGameName}
                        onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
                            setMenuOptions({...menuOptions, showGameName: event.target.checked});
                            if (typeof localStorage !== "undefined") {
                                localStorage.setItem("menuOptions", JSON.stringify({...menuOptions, showGameName: event.target.checked}));
                            }
                        }}
                        />
                    }
                    label="Show game name"
                    />
            </MenuItem>

            <MenuItem>
                <FormControlLabel
                    control={
                        <Checkbox
                        checked={menuOptions.showConnectionStatus}
                        onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
                            setMenuOptions({...menuOptions, showConnectionStatus: event.target.checked});
                            if (typeof localStorage !== "undefined") {
                                localStorage.setItem("menuOptions", JSON.stringify({...menuOptions, showConnectionStatus: event.target.checked}));
                            }
                        }}
                        />
                    }
                    label="Show Websocket Connection Status"
                    />
            </MenuItem>
            
            {game.settings.enable_stop_time === true && <MenuItem>
                <FormControlLabel
                    control={
                        <Checkbox
                        checked={menuOptions.showTimeRemaining}
                        onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
                            setMenuOptions({...menuOptions, showTimeRemaining: event.target.checked});
                            if (typeof localStorage !== "undefined") {
                                localStorage.setItem("menuOptions", JSON.stringify({...menuOptions, showTimeRemaining: event.target.checked}));
                            }
                        }}
                        />
                    }
                    label="Show time remaining"
                    />
            </MenuItem>}

            <Divider/>

            <MenuItem>
                <FormControlLabel
                    control={
                        <Checkbox
                        checked={menuOptions.lockGeo}
                        onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
                            setMenuOptions({...menuOptions, lockGeo: event.target.checked});
                            if (typeof localStorage !== "undefined") {
                                localStorage.setItem("menuOptions", JSON.stringify({...menuOptions, lockGeo: event.target.checked}));
                            }
                        }}
                        />
                    }
                    label="Lock map to geolocation"
                    />
            </MenuItem>
        </Menu>
    );

    const GameWindow = (
        <Paper 
            square={false} 
            sx={{
                height: 'calc(100vh - 90px)', // 64px is the height of the navbar
                borderStyle: 'solid',
                borderWidth: 5,
                borderRadius: 3,
                borderColor: userColor,
                display: 'flex',
                flexDirection: 'column',
                overflow: 'hidden',
                minHeight: '450px',
                position: 'relative'
            }}>
                <Box sx={{display: 'flex', alignItems: 'center', justifyContent: 'space-between', flexWrap: 'nowrap', mt: 1, mb: 1, px: 2}}>
                <Box sx={{display: 'flex', alignItems: 'center', justifyContent: 'space-around', flexWrap: 'wrap', mt: 1, mb: 1, px: 2, width: '100%'}}>
                    {menuOptions.showConnectionStatus && <Typography variant="subtitle1" sx={{mx: 1}}>
                        Connection: <strong style={{ 
                            color: theWebsocket.connectionStatus === 'Connected' ? 'green' : 
                                    theWebsocket.connectionStatus === 'Connecting' ? 'orange' : 'red' 
                        }}>{theWebsocket.connectionStatus}</strong>
                    </Typography>}
                    
                    {menuOptions.showGameName && <Typography variant="h5" sx={{mx: 1}}>
                        Play game: <strong>{game.name}</strong>
                    </Typography>}

                    {menuOptions.showTimeRemaining && game.settings.enable_stop_time && <Box sx={{mx: 1,display: 'flex', alignItems: 'center', flexDirection: 'column', justifyContent: 'center'}}>
                        <Typography variant="h6" sx={{mr: 1, color: timeRemainingString.includes("second") || timeRemainingString.includes("now") ? 'red' : timeRemainingString.includes("minute") && parseInt(timeRemainingString.match(/\d+/)?.[0] || '0') < 5 ? 'orange' : 'inherit'}}>
                            Game will end 
                        </Typography>
                        <Typography variant="h6" sx={{color: timeRemainingString.includes("second") || timeRemainingString.includes("now") ? 'red' : timeRemainingString.includes("minute") && parseInt(timeRemainingString.match(/\d+/)?.[0] || '0') < 5 ? 'orange' : 'inherit'}}>
                            <strong>{timeRemainingString}</strong>
                        </Typography>
                    </Box>}

                    {menuOptions.showUsername && <Typography variant="body1" sx={{mx: 1}}>
                        Game username: <strong>{gameUsername}</strong>
                    </Typography>}
                </Box>
            
               {menuOptions !== null &&  <>
               <IconButton onClick={handleClick} sx={{mx: 1}}>
                    <GridMenuIcon/>
                </IconButton>
                {OptionsMenu}
                </>}
            </Box>
            <Suspense fallback={
                <Box 
                sx={{
                    display: 'flex', 
                    justifyContent: 'center', 
                    alignItems: 'center', 
                    height: '60vh',
                    width: '100%',
                    position: 'absolute',
                    flexDirection: 'column',
                }}>
                    <Typography variant='h5'>Loading map...</Typography>
                    <LinearProgress sx={{width: '300px', maxWidth: '80vw'}}/>
                </Box>
            }>
                <Map points={points} userColor={userColor || ""} lockToGeo={menuOptions.lockGeo}/>
            </Suspense>
        </Paper>
    );

    return(
        <>
            {(loadingWS === true) ? <LoadingGameWindow/> :
                <>
                    {(userColor === null) && <SelectColorWindow/>}
                    <Box sx={{height: userColor !== null ? 'auto' : 0, overflow: "hidden"}}>
                        {GameWindow}
                    </Box>
                </>

            }
        </>
    )
}