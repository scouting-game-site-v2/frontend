import React from "react";
import "leaflet/dist/leaflet.css";
import { Box, Button, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, Divider, Paper, Typography } from "@mui/material";
import { currentLocation, GetGeolocation, loadingLocation, locationAge, locationDistance, locationSpeed } from '@/tools/geolocation';
import { MapContainer, TileLayer, Marker, Popup, useMapEvents, Circle, useMap } from 'react-leaflet';
import { Icon, LatLng, latLngBounds } from "leaflet";
import * as notify from '@/app/notify';
import { GamePoint, customMarker, LocationData, circleFill} from '@/tools/gameData/game_1_types'
import { defaultLocation } from "@/tools/gameTypes";
import { updateLocation } from "@/app/game/[id]/game_1";

var circels: LocationData[] = [];
var selectedCircle: LocationData | null;
var setCircels: React.Dispatch<React.SetStateAction<LocationData[]>>;
var setSelectedCircle: React.Dispatch<React.SetStateAction<LocationData | null>>;

export default function Map({ points, userColor, lockToGeo }: { points: GamePoint[], userColor: string, lockToGeo: boolean}) {
    const defaultZoom = 13; // default zoom level
    const mapRef = React.useRef<any>(null);
    const markerRef = React.useRef<any>(null);
    [circels, setCircels] = React.useState<LocationData[]>([]);
    [selectedCircle, setSelectedCircle] = React.useState<LocationData | null>(null);
    const [toMyLocOnStart, setToMyLocOnStart] = React.useState(false);
    const [gameUsername, setGameUsername] = React.useState("");
    const [timer, setTimer] = React.useState<any>(null);

    const [openQuestion, setOpenQuestion] = React.useState(false);
    const [questionText, setQuestionText] = React.useState("");
    const [questionAnswers, setQuestionAnswers] = React.useState<string[]>([]);
    const [questionCorrectAnswer, setQuestionCorrectAnswer] = React.useState("");
    const [questionCorrect, setQuestionCorrect] = React.useState<boolean | null>(null);
    const [questionActive, setQuestionActive] = React.useState<boolean>(false);

    var L = require('leaflet');
    
    // enable get geolocation
    GetGeolocation();

    const windowsIsDefined = typeof window !== 'undefined';

    // get the game username from local storage
    React.useEffect(() => {
        if (windowsIsDefined) {
            setGameUsername(localStorage.getItem('game_username') || '');
        }
    }, [windowsIsDefined]);

    const [oldSelected, setOldSelected] = React.useState<any>(null);
    var selectedIsChanged = oldSelected !== selectedCircle;
    // add the circle popup
    React.useEffect(() => {
        setOldSelected(selectedCircle);

        if (selectedCircle !== null) {
            const { data, leaflet } = selectedCircle;

            const popupContent = `
                <div style="display: flex; flex-direction: column; flex-wrap: wrap; align-content: center; align-items: center;">
                    ${selectedCircle?.data.owner === "" || selectedCircle?.data.owner == null ? `
                        <h5 style="margin: 0; font-size: medium; font-weight: bold">No owner yet</h5>
                    ` : `
                        <h6 style="margin: 0; font-size: medium; font-weight: normal">Owners name:</h6>
                        <h5 style="margin: 0; font-size: medium; font-weight: bold">${selectedCircle?.data.owner}</h5>
                    `}
                </div>
            `;
            leaflet.bindPopup(popupContent).openPopup();
        }
    }, [selectedIsChanged]);

    const selectTheCircle = React.useCallback((pointID: number) => {
        const circle = circels.find(circle => circle.data.id === pointID);
        if (circle) {
            setSelectedCircle(circle);
            goToLocation(circle.data.latitude, circle.data.longitude);
        }
    }, []);

    const goToLocation = (lat: number, lng: number) => {
        if (lat != null && lng != null && mapRef.current != null) {
            mapRef.current.flyTo([lat, lng], mapRef.current.getZoom());
        } else {
            console.log('No location found');
        }
    };

    const selectCircle = React.useCallback((circle: LocationData) => {
        // Update the selected circle
        selectTheCircle(circle.data.id);
        setMarkerInLocation(true);
    }, [selectTheCircle]);


    var mapIsLoaded = mapRef.current !== null;

    // add the circles to the map
    React.useEffect(() => {
        if (mapIsLoaded === true) {
            // Remove old circles from the map
            const circlesToRemove = circels.filter(circle => !points.map(point => point).includes(circle.data));
            circlesToRemove.forEach(circle => circle.leaflet.remove());

            const newCircles = points.map((point: GamePoint) => {
                const existingCircle = circels.find(circle => circle.data === point);
                if (existingCircle) {
                    return existingCircle;
                } else if (point.settings.disabled === true) {
                    return;
                } else if (point.settings.visible === true) {
                    const leaflet: L.Circle = L.circle([point.latitude, point.longitude], {
                        color: point.color,
                        fillColor: point.color,
                        fillOpacity: circleFill.default,
                        radius: point.radius,
                    });

                    leaflet.on('click', () => selectCircle({ data: point, leaflet: leaflet }));

                    const groupLayer = mapRef.current?.getPane('circles');
                    if (groupLayer) {
                        leaflet.addTo(groupLayer);
                    } else {
                        const newGroupLayer = L.layerGroup([leaflet]);
                        newGroupLayer.addTo(mapRef.current);
                        newGroupLayer.options.id = 'circles';
                    }
                    return { data: point, leaflet: leaflet };
                } else {
                    return;
                }
            });

            // remove undefined objects of the array
            const newCircleArray = newCircles.filter((point: LocationData | undefined): point is LocationData => {
                return typeof point !== 'undefined';
            });

            setCircels(newCircleArray);
        }
    }, [L, points, userColor, selectCircle, mapIsLoaded]);

    // add marker on click position
    const [markerInLocation, setMarkerInLocation] = React.useState(false);

    // Event handler for the map
    const HandleEvent = React.memo(function HandleEvent() {
        const map = useMapEvents({
            click(e){
                const latlng = e.latlng;

                // Check if a circle was clicked
                const clickedCircle = circels.find(circle => 
                    calculateDistance(
                        circle.data.latitude, 
                        circle.data.longitude, 
                        latlng.lat, 
                        latlng.lng) < circle.data.radius);

                if (clickedCircle) {
                    // Handle logic when a circle is clicked
                } else {
                    if (selectedCircle) {
                        setSelectedCircle(null);
                    }
                }

                if (lockToGeo && currentLocation !== null && map.getCenter().distanceTo(currentLocation) > 1) {
                    map.setView(currentLocation);
                }
            },

            // moveend(e) {
            //     if (selectedCircle != null && !map.getBounds().contains(selectedCircle.leaflet.getLatLng())) {
            //         if (selectedCircle) {
            //             setSelectedCircle(null);
            //         }
            //     }
            // },

            // drag() {
            //     if (lockToGeo && currentLocation !== null && map.getCenter().distanceTo(currentLocation) > 1) {
            //         map.setView(currentLocation);
            //     }
            // },

            // zoom() {
            //     if (lockToGeo && currentLocation !== null && map.getCenter().distanceTo(currentLocation) > 1) {
            //         map.setView(currentLocation);
            //     }
            // }
            
        }
        );
        return null
    });

    function calculateDistance(lat1: number, lon1: number,lat2: number,lon2: number){
        const toRadian = (n: number) => (n * Math.PI) / 180

        let R = 6371e3;  // km
        let x1 = lat2 - lat1;
        let dLat = toRadian(x1);
        let x2 = lon2 - lon1;
        let dLon = toRadian(x2);
        let a =
            Math.sin(dLat / 2) * Math.sin(dLat / 2) +
            Math.cos(toRadian(lat1)) * Math.cos(toRadian(lat2)) * 
            Math.sin(dLon / 2) * Math.sin(dLon / 2);
        let c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        let d = R * c;
        return d
    }

    const [inLocationStorage, setInLocationStorage] = React.useState<LocationData | null>(null);
    const [oldLocation, setOldLocation] = React.useState<LocationData | null>(null);

    const locationIsChanged = currentLocation !== oldLocation;

    // Check if the user is in a location
    React.useEffect(() => {
        const map = mapRef.current;
        if (map === null || circels.length == 0) return;

        setOldLocation(currentLocation);
        // On first load, go to the current location
        if (currentLocation !== null && toMyLocOnStart === false){
            map.setView(currentLocation);
            setToMyLocOnStart(true);
        }

        // lock the map to the current location
        if (lockToGeo === true && currentLocation !== null){
            map.setView(currentLocation);
            map.dragging.disable();
        } else if (lockToGeo === false){
            map.dragging.enable();
        }

        // Only continue when a usercolor is selected
        if (userColor === "") return;

        // Check the speed
        if (locationSpeed >= 100){
            notify.ShowNotification(notify.location.Bottom, notify.Severity.Warning, "Your speed was way to fast!");
        }

        // check if in a location
        const inLocation = circels.find(circle => {
        if (currentLocation !== null) {
            return calculateDistance(
                circle.data.latitude, 
                circle.data.longitude, 
                currentLocation.lat || 0, 
                currentLocation.lng || 0) < circle.data.radius
            } else {
                return false;
            }
        });

        const resetLocation = () => {
            // Disable question
            if (questionActive === true) {
                console.log("Disabling question dialog");
                setQuestionCorrect(false);
                setQuestionActive(false);
                setOpenQuestion(false);
            }

            // Reset the location
            if (inLocationStorage !== null) {
                console.log('Left location: %s (ID: %s)',inLocationStorage.data.index, inLocationStorage.data.id);
                inLocationStorage.leaflet.setStyle({ 
                    color: inLocationStorage.data.color,
                    fillColor: inLocationStorage.data.color,
                    fillOpacity: circleFill.default
                });
                setInLocationStorage(null);
            }

            // reset the timer
            if (timer !== null) {
                clearTimeout(timer);
                setTimer(null);
            }
        }
            
        if (inLocation && (inLocation.data.color != userColor || inLocation.data.owner !== gameUsername)) {
            if (inLocationStorage === null || inLocation.data.id !== inLocationStorage.data.id) {
                console.log('Enter location: %s (ID: %s)',inLocation.data.index, inLocation.data.id);

                // Reset the location if the user is in another location
                if (inLocationStorage !== null) {
                    resetLocation();
                    return;
                }

                // Set the location in the storage
                setInLocationStorage(inLocation);
                
                // Set the color of the circle
                inLocation.leaflet.setStyle({ 
                    color: userColor,
                    fillColor: 'dark-gray',
                    fillOpacity: circleFill.active
                });
                
                // check if a timer is running and cancel it
                if (timer !== null) {
                    clearTimeout(timer);
                    setTimer(null);
                }
                
                var questionAvailable = false;

                // Check if the location has a question
                if (inLocation.data.settings.enableQuestion === true) {
                    if (inLocation.data.settings.answers === undefined || inLocation.data.settings.correctAnswer === undefined) {
                        console.log('Error: Question is enabled but no answers or correct answer is set');
                        return;
                    }
                    questionAvailable = true;
                    console.log('Question: %s',inLocation.data.settings.question);
                    setQuestionText(inLocation.data.settings.question || "Error");
                    setQuestionAnswers(inLocation.data.settings.answers || ["Error"]);
                    setQuestionCorrectAnswer(inLocation.data.settings.answers[inLocation.data.settings.correctAnswer] || "Error");
                    setQuestionCorrect(null);
                } 
                    
                // Check if the location has a time to activate
                if (inLocation.data.settings.timeToActivate > 0) {
                    if (timer === null) {
                        // Set a timer to activate the location
                        console.log('Setting timer to activate location in %d ms', inLocation.data.settings.timeToActivate);
                        // setTimer(true);

                        const newTimer = setTimeout(() => {
                            if (questionAvailable) {
                                setOpenQuestion(true);
                                console.log("Waiting for answer");
                                setQuestionActive(true);

                            } else {
                                // Activate the location
                                console.log('Claiming location: %s (ID: %s)',inLocation.data.index, inLocation.data.id);
                                updateLocation(inLocation);
                                clearTimeout(timer);
                                setTimer(null);
                            }
                        }, inLocation.data.settings.timeToActivate);
                        setTimer(newTimer);
                    }
                } else {
                    // Activate the location
                    updateLocation(inLocation);
                }
            } else if (inLocationStorage !== null && inLocation.data.id !== inLocationStorage.data.id) {
                resetLocation();
            }
        } else if (!inLocation) {
            resetLocation();
        }
    },[points, gameUsername, inLocationStorage, lockToGeo, timer, toMyLocOnStart, userColor, locationIsChanged, questionCorrect, openQuestion, questionActive]);

    React.useEffect(() => {
        if (questionCorrect !== null && questionActive === true) {
            if (questionCorrect === true) {
                // Activate the location
                console.log('Correct answer, claiming location: %s (ID: %s)', inLocationStorage?.data.index, inLocationStorage?.data.id);
                if (inLocationStorage) {
                    updateLocation(inLocationStorage);
                } else console.error("Can't send update location");
                // setTimer(null);
                setQuestionActive(false);
            } else if (questionCorrect === false) {
                // Reset the location
                console.log('Incorrect answer, leaving location: %s (ID: %s)', inLocationStorage?.data.index, inLocationStorage?.data.id);
                if (inLocationStorage) {
                    inLocationStorage.leaflet.setStyle({
                        color: inLocationStorage.data.color,
                        fillColor: inLocationStorage.data.color,
                        fillOpacity: circleFill.default
                    });
                }
                // setTimer(null);
                setQuestionActive(false);          
            }
        }
    }, [questionCorrect, inLocationStorage, questionActive, timer]);

    const LeafletMap = (
        <MapContainer
            center={[defaultLocation.lat, defaultLocation.lng]} 
            zoom={defaultZoom} 
            style={{ height: '100%', width: '100%' }}
            ref={mapRef}
        >
            <TileLayer
            url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
            attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
            />
            <HandleEvent/>
            {currentLocation !== null && <Marker icon={customMarker} position={[currentLocation.lat, currentLocation.lng]}/>}
        </MapContainer>
    );

    const handleOpenQuestion = () => {
        setOpenQuestion(true);
    }

    const handleCloseQuestion = (event: React.SyntheticEvent) => {
        if ((event as any).type === 'click') {
            setOpenQuestion(true);
        } else {
            setOpenQuestion(false);
        }
    }
    
    function Question() {
        return (
            <Dialog
                open={openQuestion}
                onClose={handleCloseQuestion}
                aria-describedby="question"
                PaperProps={{
                    component: 'form',
                    onSubmit: (e: React.FormEvent) => {
                        e.preventDefault();
                        console.log('Submit');
                    }
                }}
                disableEscapeKeyDown
            >
                <DialogTitle id="question">Question</DialogTitle>
                <Divider />
                <DialogContent>
                    <DialogContentText id="question-text" color="textPrimary">
                        {questionText}
                    </DialogContentText>
                </DialogContent>
                <Typography variant="body2" sx={{ fontStyle: 'italic', px: 1, mt: -2}} color="gray">Click on the correct answer to claim the location</Typography>
                <Divider />
                <DialogActions>
                    <Box sx={{display: 'flex', justifyContent: "space-around", flexWrap: 'wrap', width: '100%', overflowWrap: 'break-word'}}>
                        {questionAnswers.map((answer, index) => (
                            <Box 
                                sx={{
                                    mx: 1,
                                    my: .5,
                                    p: 1,
                                    color: 'grey.300',
                                    bgcolor: 'grey.800',
                                    border: '1px solid',
                                    borderColor: 'grey.700',
                                    borderRadius: 2,
                                    fontSize: '0.875rem',
                                    fontWeight: '700',
                                    cursor: 'pointer', // Change cursor on hover
                                    '&:hover': {
                                        bgcolor: 'grey.700', // Change background color on hover
                                    },
                                }} 
                                key={index} 
                                onClick={() => {
                                    if (answer === questionCorrectAnswer) {
                                        setQuestionCorrect(true);
                                    } else {
                                        setQuestionCorrect(false);
                                    }
                                    setOpenQuestion(false);
                                }}
                            >
                                {answer}
                            </Box>
                        ))}
                    </Box>
                </DialogActions>
            </Dialog>
        )
    }

    return (
        <>
            {Question()}
            <div style={{ minHeight: '400px', display: 'flex', flexGrow: 1}}>
                {LeafletMap}
            </div>
        </>
    );
}