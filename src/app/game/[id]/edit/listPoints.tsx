// "use client"
import React, { use } from 'react';
import Card from '@mui/material/Card';
import Typography from '@mui/material/Typography';
import Paper from '@mui/material/Paper';
import Box from '@mui/material/Box';
import { useNow, useFormatter, useTimeZone } from 'next-intl';
import { DataGrid, GridColDef, GridExpandMoreIcon } from '@mui/x-data-grid'
import { styled } from '@mui/material/styles';
import { ExpandLess, ExpandMore, Sort, TempleBuddhist } from '@mui/icons-material';
import { pageTheme } from '@/app/theme';
import { colorList, defaultColor } from '@/tools/gameTypes';
import { GamePoint } from '@/tools/gameData/game_1_types';
import * as Icon from '@mui/icons-material';

var rows: TabelData[] = [];
var setRows: React.Dispatch<React.SetStateAction<TabelData[]>>;

let selected: number = 0;
let setSelected: React.Dispatch<React.SetStateAction<number>>;

const colorArray = [defaultColor, ...Object.values(colorList)];

const StyledGridOverlay = styled('div')(({ theme }) => ({
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    height: '100%',
    '& .ant-empty-img-1': {
        fill: theme.palette.mode === 'light' ? '#aeb8c2' : '#262626',
    },
    '& .ant-empty-img-2': {
        fill: theme.palette.mode === 'light' ? '#f5f5f7' : '#595959',
    },
    '& .ant-empty-img-3': {
        fill: theme.palette.mode === 'light' ? '#dce0e6' : '#434343',
    },
    '& .ant-empty-img-4': {
        fill: theme.palette.mode === 'light' ? '#fff' : '#1c1c1c',
    },
    '& .ant-empty-img-5': {
        fillOpacity: theme.palette.mode === 'light' ? '0.8' : '0.08',
        fill: theme.palette.mode === 'light' ? '#f5f5f5' : '#fff',
    },
}));

function CustomNoRowsOverlay() {
    return (
        <StyledGridOverlay>
            <svg
                style={{ flexShrink: 0 }}
                width="240"
                height="200"
                viewBox="0 0 184 152"
                aria-hidden
                focusable="false"
            >
                <g fill="none" fillRule="evenodd">
                    <g transform="translate(24 31.67)">
                        <ellipse
                            className="ant-empty-img-5"
                            cx="67.797"
                            cy="106.89"
                            rx="67.797"
                            ry="12.668"
                        />
                        <path
                            className="ant-empty-img-1"
                            d="M122.034 69.674L98.109 40.229c-1.148-1.386-2.826-2.225-4.593-2.225h-51.44c-1.766 0-3.444.839-4.592 2.225L13.56 69.674v15.383h108.475V69.674z"
                        />
                        <path
                            className="ant-empty-img-2"
                            d="M33.83 0h67.933a4 4 0 0 1 4 4v93.344a4 4 0 0 1-4 4H33.83a4 4 0 0 1-4-4V4a4 4 0 0 1 4-4z"
                        />
                        <path
                            className="ant-empty-img-3"
                            d="M42.678 9.953h50.237a2 2 0 0 1 2 2V36.91a2 2 0 0 1-2 2H42.678a2 2 0 0 1-2-2V11.953a2 2 0 0 1 2-2zM42.94 49.767h49.713a2.262 2.262 0 1 1 0 4.524H42.94a2.262 2.262 0 0 1 0-4.524zM42.94 61.53h49.713a2.262 2.262 0 1 1 0 4.525H42.94a2.262 2.262 0 0 1 0-4.525zM121.813 105.032c-.775 3.071-3.497 5.36-6.735 5.36H20.515c-3.238 0-5.96-2.29-6.734-5.36a7.309 7.309 0 0 1-.222-1.79V69.675h26.318c2.907 0 5.25 2.448 5.25 5.42v.04c0 2.971 2.37 5.37 5.277 5.37h34.785c2.907 0 5.277-2.421 5.277-5.393V75.1c0-2.972 2.343-5.426 5.25-5.426h26.318v33.569c0 .617-.077 1.216-.221 1.789z"
                        />
                    </g>
                    <path
                        className="ant-empty-img-3"
                        d="M149.121 33.292l-6.83 2.65a1 1 0 0 1-1.317-1.23l1.937-6.207c-2.589-2.944-4.109-6.534-4.109-10.408C138.802 8.102 148.92 0 161.402 0 173.881 0 184 8.102 184 18.097c0 9.995-10.118 18.097-22.599 18.097-4.528 0-8.744-1.066-12.28-2.902z"
                    />
                    <g className="ant-empty-img-4" transform="translate(149.65 15.383)">
                        <ellipse cx="20.654" cy="3.167" rx="2.849" ry="2.815" />
                        <path d="M5.698 5.63H0L2.898.704zM9.259.704h4.985V5.63H9.259z" />
                    </g>
                </g>
            </svg>
            <Box sx={{ mt: 1 }}>No locations</Box>
        </StyledGridOverlay>
    );
}

const column: GridColDef[] = [
    // format: (value: number) => value.toLocaleString('eu-EU'),
    { 
        field: 'pointIndex',          
        headerName: 'ID',
        type: 'number',
        headerAlign: 'center',
        align: 'center',
        width: 90,
    },
    { 
        field: 'color',       
        headerName: 'Color',
        minWidth: 150,
        maxWidth: 200,
        flex: 1,
        headerAlign: 'center',    
        align: 'center',
        renderCell: (params) => {
            const colorIndex = colorArray.findIndex(color => color.name === params.value);
            const color = colorIndex !== -1 ? colorArray[colorIndex].color : params.value;
            return (
                <>
                    <span style={{ backgroundColor: color, width: "20px", height: "20px", borderRadius: "50%", display: "inline-block", marginRight: "5px", verticalAlign: "middle" }}></span>
                    <span style={{ verticalAlign: "middle" }}>{params.value}</span>
                </>
            );
        }
    },
    { 
        field: 'owner',       
        headerName: 'Owner',
        type: 'string',     
        minWidth: 200,
        flex: 1,    
        align: 'center',    
        headerAlign: 'center',
        editable: false 
    },
    { 
        field: 'disabled',       
        headerName: 'Disabled',
        type: 'boolean',     
        minWidth: 100,
        flex: 1,    
        editable: false,
        renderCell: (params) => {
            return (
                <div className={params.value ? "admin-cell" : "non-admin-cell"}>
                    {params.value ? <span style={{ color: "orange" }}><Icon.Check/></span> : <></>}
                </div>
            );
        },
    },
    { 
        field: 'visible',       
        headerName: 'Visible',
        type: 'boolean',     
        minWidth: 100,
        flex: 1,    
        editable: false,
        renderCell: (params) => {
            return (
                <div className={params.value ? "admin-cell" : "non-admin-cell"}>
                    {params.value ? <span style={{ color: "green" }}><Icon.Check/></span> : <span style={{ color: "red" }}><Icon.Close/></span>}
                </div>
            );
        },
    },
    {
        field: 'timeToActivate',
        headerName: 'Time to activate',
        minWidth: 150,
        flex: 1,
        editable: false,
        align: 'center',
        headerAlign: 'center',
        renderCell: (params) => {
            const formattedValue = params.value / 1000 + " s";
            return (
                <div className="admin-cell">
                    {formattedValue}
                </div>
            );
        }
    },
    {
        field: 'question',
        headerName: 'Question',
        minWidth: 100,
        flex: 1,
        editable: false,
        type: 'boolean',
        align: 'center',
        headerAlign: 'center',
        renderCell: (params) => {
            return (
                <div className={params.value ? "admin-cell" : "non-admin-cell"}>
                    {params.value ? <span style={{ color: "green" }}><Icon.Check/></span> : <></>}
                </div>
            );
        }
    },
    { 
        field: 'radius',      
        headerName: 'Radius',
        minWidth: 140,
        flex: 0.9,  
        type: 'number',     
        editable: false,    
        align: 'center',    
        headerAlign: 'center',
        renderCell: (params) => {
            const formattedValue = params.value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
            return (
                <div className="admin-cell">
                    {formattedValue} m
                </div>
            );
        }
    },
    { 
        field: 'latitude',    
        headerName: 'Latitude',
        minWidth: 140,
        flex: 0.9 
    },
    { 
        field: 'longitude',   
        headerName: 'Longitude',
        minWidth: 140,
        flex: 0.9 
    },
];

interface TabelData {
    pointIndex: number,
    id: number,
    latitude: number,
    longitude: number,
    radius: number,
    owner: string,
    color: string,
    disabled: boolean,
    visible: boolean,
    question: boolean,
    timeToActivate: number
}

function addTabelData(
    point: GamePoint,
    // pointIndex: number
): TabelData {
    const colorIndex = colorArray.findIndex(color => color.color === point.color);
    const colorName = colorIndex !== -1 ? colorArray[colorIndex].name : point.color;
    const question = point.settings?.enableQuestion || false;
                
    const tabelData: TabelData = {
        pointIndex: point.index,
        id: point.id,
        latitude: point.latitude,
        longitude: point.longitude,
        radius: point.radius,
        owner: point.owner,
        color: colorName,
        disabled: point.settings?.disabled,
        visible: point.settings?.visible,
        question: question,
        timeToActivate: point.settings?.timeToActivate || 0
    }
    return tabelData;
}

// remove a row
export const removeRow = (id: number) => {
    setRows(rows.filter((row) => row.id !== id));
    console.log("Delete: %d", id);

};

// set the selected row
export const selectRow = (id: number) => {
    setSelected(id);
}

export default function PointsTable({points, setSelectedInTable} : {points: null | GamePoint[], setSelectedInTable: (id: number) => void}) {
    // console.log(points);
    [rows, setRows] = React.useState<TabelData[]>([]);
    [selected, setSelected] = React.useState<number>(0);
    const formatter = useFormatter();
    const now = useNow();
    const timeZone = useTimeZone();

    const onRowsSelectionHandler = (ids: any) => {
        const selectedRowsData = ids.map(
            (id: number) =>
                rows.find((row) => row.id === id)
        );
        if (selectedRowsData.length !== 0) {
            selectRow(selectedRowsData[0].id);
            setSelectedInTable(selectedRowsData[0].id);
        }
    };

    // format the date
    // console.log(points.points);
    React.useEffect(() => {
        if (points === null) return;
        setRows([]);
        points.forEach((point: any) => {
            setRows((prevRows) => {
                // check if the row already exists
                const index = prevRows.findIndex((row) => row.id === point.id);
                if (index === -1) {
                    // return [...prevRows, addTabelData(point, prevRows.length + 1)];
                    return [...prevRows, addTabelData(point)];
                } else {
                    return prevRows; // add default return value
                }
            });
        });
    }, [points]);

    // console.log(this.props.onRowSelection)
    return (
        <>
            {/* {AlertDialogSlide()} */}
            <Paper square={false} sx={{padding: 2, marginBottom: 2}}>
                <Typography variant="h5" sx={{ fontWeight: 'bold' }}>
                    Location table
                </Typography>
                
                <hr style={{backgroundColor: pageTheme.palette.divider, color: pageTheme.palette.divider, height: '2px', borderWidth: 0}} />
                
                <Box sx={{
                    width: '100%',
                    overflow: 'hidden',
                    mt: 2
                }}>
                    <Box>
                        <DataGrid
                            slots={{
                                noRowsOverlay: CustomNoRowsOverlay,
                                columnSortedDescendingIcon: () => <ExpandMore className="icon" />,
                                columnSortedAscendingIcon: () => <ExpandLess className="icon" />,
                                columnUnsortedIcon: () => <Sort className="icon" />,
                            }}
                            rows={rows}
                            columns={column}
                            initialState={{
                                pagination: {
                                    paginationModel: {
                                        pageSize: 10
                                    }
                                },
                                sorting: {
                                    sortModel: [{ field: 'pointIndex', sort: 'asc' }],
                                }
                            }}
                            sx={{ '--DataGrid-overlayHeight': '400px', height: rows.length == 0 ? '400px' : '' }}
                            pageSizeOptions={[10, 25, 100]}
                            hideFooterSelectedRowCount
                            onRowSelectionModelChange={(ids: any) => onRowsSelectionHandler(ids)}
                            rowSelectionModel={selected}
                            // TODO: go to te page where the row is selected
                        />
                    </Box>
                </Box>
            </Paper>
        </>
    );
}	