"use client"
import React, { Suspense } from 'react';
import { ThemeProvider } from '@mui/material/styles';
import { pageTheme, themeOptions } from "@/app/theme";
import Container from "@mui/material/Container";
// import NavBar from '@/app/navbar';
import CssBaseline from '@mui/material/CssBaseline';
import Typography from '@mui/material/Typography';
import { Accordion, AccordionDetails, AccordionSummary, Box, Button, Checkbox, Collapse, Grid2, IconButton, LinearProgress, Paper, SelectChangeEvent, TextField, Dialog, DialogTitle, DialogContent, DialogContentText, DialogActions, Divider, Icon } from '@mui/material';
import * as notify from '@/app/notify'
import { getGameByID, updateGame } from '@/tools/api/gamesAPI';
import { useRouter, useSearchParams } from 'next/navigation'
import { PageLoader } from '@/app/loader';
import { useFormatter, useNow, useTimeZone } from 'next-intl';
import { GameData, GameDataSettings, gameList, gameType } from '@/tools/gameTypes';
import { getGamePoints } from '@/tools/api/gamePointsAPI';
import PointsTable from '@/app/game/[id]/edit/listPoints';
import { ContentCopy, Share, Done, EditNote, EditOff, WhatsApp, Telegram, QrCode } from '@mui/icons-material';
import { QueryNotifications } from '@/tools/notification';
import useWebSocket, { ReadyState } from 'react-use-websocket';
import { JoinGame, WebSocketMessage } from "@/tools/websocketTypes";
import { GridExpandMoreIcon } from '@mui/x-data-grid';
import { DateTimePicker } from '@mui/x-date-pickers/DateTimePicker';
import { LocalizationProvider } from '@mui/x-date-pickers';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import 'dayjs/locale/nl'
import 'dayjs/locale/be'
import 'dayjs/locale/de'
import 'dayjs/locale/en'
import dayjs, { Dayjs } from 'dayjs';
import { GamePoint } from '@/tools/gameData/game_1_types';
import QRCode from 'react-qr-code';

const LazyNavbar = React.lazy(() => import('@/app/navbar'));
const Game_1 = React.lazy(() => import('@/app/game/[id]/edit/game_1'));
const Game_2 = React.lazy(() => import('@/app/game/[id]/edit/game_2'));

let selectedInTable = 0;
let setSelctedInTable: React.Dispatch<React.SetStateAction<number>>;

// Store the value when the user selects a row in the table
// export function setSelectedInTable(value: number) {
//     setSelctedInTable(value);
// }
const setSelectedInTable = (value: number) => {
    setSelctedInTable(value);
}

let activeUsers = 0;
let setActiveUsers: React.Dispatch<React.SetStateAction<number>>;

export default function EditPage({ params }: { params: Promise<{ id: string }> }){   
    // show query notifications
    const searchParams = useSearchParams();
    React.useEffect(() => {
        QueryNotifications(searchParams);
    }, [searchParams]);

    const [gameID, setGameID] = React.useState<number | null>(null);
    const { id } = React.use(params);
    React.useEffect(() => {
        if (id !== null) {
            setGameID(parseInt(id));
        }
    }, [id]);
    const router = useRouter();
    const retunUrl = searchParams.get('next_page') || "/";
    const formatter = useFormatter();
    const now = useNow();
    
    // get the current game from the server
    const [game, setGame] = React.useState<null | GameData>(null);
    const [gameSettings_copy, setGameSettings_copy] = React.useState<null | GameDataSettings>(null);
    const [points, setPoints] = React.useState<null | GamePoint[]>(null);
    const [loading, setLoading] = React.useState<boolean>(true);
    const [loadingPoints, setLoadingPoints] = React.useState<boolean>(true);
    const [rtnParms, setRtnParms] = React.useState(new URLSearchParams(useSearchParams()));
    const [editName, setEditName] = React.useState<boolean>(false);
    const [hoverOverGameName, setHoverOverGameName] = React.useState<boolean>(false);    
    const [hoverOverGameSettings, setHoverOverGameSettings] = React.useState<boolean>(false);
    const [editGameSettings, setEditGameSettings] = React.useState(false);
    const [changesPending, setChangesPending] = React.useState(false);
    const [shareLink, setShareLink] = React.useState<string>("Loading");
    [selectedInTable, setSelctedInTable] = React.useState<number>(0);
    const [openShareModal, setOpenShareModal] = React.useState(false);

    const timeZone = useTimeZone();
    [activeUsers, setActiveUsers] = React.useState<number>(0);

    // Update point index value for visualisation
    React.useEffect(() => {
        if (points !== null) {
            points.forEach((point, index) => {
                point.index = index + 1;
            });
        }
    }, [points]);
    
    // // check if authenticated
    // const [authenticated, setAuthenticated] = React.useState<boolean>(false);
    // React.useEffect(() => {
    //     const token = localStorage.getItem("token");
    //     if (token === null) {
    //         rtnParms.set("severity", "error");
    //         rtnParms.set("message", "You are not authenticated");
    //         rtnParms.set("location", "bottom");
    //         router.push(retunUrl + "?" + rtnParms.toString());
    //         return
    //     } else {
    //         setAuthenticated(true);
    //     }
    // }, [router, rtnParms, setAuthenticated]);


    const getNewPoints = React.useCallback(() => {
        if (gameID === null) return;

        getGameByID(gameID).then((resp) => {
            if (resp === null) {
                return
            }
            const game: GameData = resp.data;

            if (resp.status === 200) {
                getAllGamePoints(game.id);
                setGame(game);
                setGameSettings_copy(game.settings);
                setLoading(false);
            } else {
                rtnParms.set("severity", "error");
                rtnParms.set("message", resp['message']['error']);
                rtnParms.set("title", resp.status + " " + resp.text);
                rtnParms.set("location", "top");
                router.push(retunUrl + "?" + rtnParms.toString());
                // window.location.href = retunUrl + "?severity=error&message=" + data['message']['error'] + "&title=" + data.status + " " + data.text + "&location=top";
                return
            }
        })
        const getAllGamePoints = (id: number) => {
            getGamePoints(id).then((resp) => {
                if (resp === null) {
                    notify.ShowNotification(notify.location.Top, notify.Severity.Error, "Somthing went wrong", "Error")
                    return
                } else if (resp.status === 200) {
                    if (resp.data === null) {
                        notify.ShowNotification(notify.location.Top, notify.Severity.Error, "Somthing went wrong", "Error")
                        return
                    }
                    else {
                        // notify.ShowNotification(notify.location.Bottom, notify.Severity.Success, "New points loaded", "", 1000)
                        setLoadingPoints(false);
                        setPoints(resp.data);
                        return 
                    }
                }else {
                    notify.ShowNotification(notify.location.Top, notify.Severity.Error, resp['message']['error'], "Error: " + resp.status + " " + resp.text)
                    return
                }          
            })
        }
    }, [gameID, router, rtnParms, retunUrl]);

    const updateGameName = async (newName: string) => {
        if (game === null) return;
        setLoading(true);
        const resp = await updateGame(game.id, newName, game.settings as unknown as string);

        if (resp == null) {
            notify.ShowNotification(notify.location.Top, notify.Severity.Error, "Response is null", "No status");
        } else if (resp.status === 200) {
            setGame(resp.data);
            setGameSettings_copy(resp.data.settings);
            notify.ShowNotification(notify.location.Bottom, notify.Severity.Success, "Name updated successfully");
        } else {
            notify.ShowNotification(notify.location.Top, notify.Severity.Error, resp['message']['error'], "Error: " + resp.status + " " + resp.text)
        }
        setLoading(false);
    }
    
    const websocketHandler = React.useCallback((message: WebSocketMessage) => {
    // function websocketHandler(message: WebSocketMessage){
        console.log(message);
        
        switch (message.action) {
            case 'user_count':
                setActiveUsers(message.data);
                break;

            case 'new_location':
                setPoints((prevPoints: any) => {
                    return [...prevPoints, message.data];
                });
                break;

            case 'update_location':
                setPoints((prevPoints: any) => {
                    const index = prevPoints.findIndex((point: any) => point.id === message.data.id);
                    if (index !== -1) {
                        prevPoints[index] = message.data;
                    }
                    return [...prevPoints];
                });
                break;

            case 'remove_location':
                setPoints((prevPoints: any) => {
                    const index = prevPoints.findIndex((point: any) => point.id === message.data.id);
                    if (index !== -1) {
                        prevPoints.splice(index, 1);
                    }
                    return [...prevPoints];
                });
                break;

            case 'game_update':
                if (message.data.id === gameID){
                    setGame(message.data);
                    console.log("New game date has been set");
                }
                break;
            
            default:
                // handle unknown message
                break;
        }
    }, [gameID]);

    const [socketUrl, setSocketUrl] = React.useState<string | null>(null);

    const windowIsDefined = typeof window !== 'undefined';

    React.useEffect(() => {
        if (windowIsDefined) {
            const protocol = window.location.protocol === 'https:' ? 'wss' : 'ws';
            setSocketUrl(protocol + '://' + window.location.host + '/ws');
        }

        if (game !== null && windowIsDefined) {
            setShareLink("https://" + window.location.host + "/game/" + game?.id);
        }
    }, [windowIsDefined, game]);
    
    const { sendMessage, lastMessage, readyState } = useWebSocket(socketUrl, {
        onOpen: () => {
            notify.ShowNotification(notify.location.Bottom, notify.Severity.Info, "Connecting websocket", "", 2000);
        },
        shouldReconnect: (closeEvent) => true,
        reconnectAttempts: 10,
        reconnectInterval: 5000,
        onError: (event) => console.error('error', event),
        onReconnectStop: (event) => {
            if (windowIsDefined){
                window.location.reload();
            }
        },
        // share: true,
    });
    const connectionStatus = {
        [ReadyState.CONNECTING]: 'Connecting',
        [ReadyState.OPEN]: 'Connected',
        [ReadyState.CLOSING]: 'Closing',
        [ReadyState.CLOSED]: 'Closed',
        [ReadyState.UNINSTANTIATED]: 'Uninstantiated',
    }[readyState];

    React.useEffect(() => {
        if (lastMessage !== null) {
            const message: WebSocketMessage = JSON.parse(lastMessage.data);
            websocketHandler(message);            
        }
    }, [lastMessage, websocketHandler]);

    // React.useEffect(() => {
    //     console.log("Current websocket state: ", connectionStatus);

    //     const retryInterval = setInterval(() => {
    //         if (readyState !== ReadyState.CONNECTING && readyState !== ReadyState.OPEN) {
    //             const protocol = window.location.protocol === 'https:' ? 'wss' : 'ws';
    //             setSocketUrl(protocol + '://' + window.location.host + '/ws');
    //             notify.ShowNotification(notify.location.Bottom, notify.Severity.Info, "Connecting websocket", "", 2000);
    //         }
    //     }, 10000);
        
    //     if (readyState === ReadyState.CLOSED) {
    //         notify.ShowNotification(notify.location.Bottom, notify.Severity.Warning, "Websocket connection is closed");
    //     }

    //     return () => clearInterval(retryInterval);
    // }, [connectionStatus]);


    const handleLeaveGame = React.useCallback(() => {
        if (game === null) return;
        if (readyState === ReadyState.OPEN) {
            const message: WebSocketMessage = {
                action: 'leave_game',
                data: {
                    game_id: game.id,
                    close_conn: true    
                },
            };
            sendMessage(JSON.stringify(message));
        }
    }, [game, readyState, sendMessage]);

    const localStorageIsDefined = typeof localStorage !== "undefined";

    React.useEffect(() => {
        if (game !== null && localStorageIsDefined) {
            if (readyState === ReadyState.OPEN) {
                const joinGameMessage: JoinGame = {
                    game_id: game.id,
                    color: '',
                    game_username: localStorage.getItem('game_username') || '',
                };
                const message: WebSocketMessage = {
                    action: 'join_game',
                    data: joinGameMessage,
                };
                sendMessage(JSON.stringify(message));
            }

            // Add event listener for beforeunload event
            window.addEventListener('beforeunload', handleLeaveGame);
            
            // Remove event listener on cleanup
            return () => {
                window.removeEventListener('beforeunload', handleLeaveGame);
            };
        }
    }, [readyState, game, sendMessage, handleLeaveGame, localStorageIsDefined]);

    React.useEffect(() => {
        setChangesPending(JSON.stringify(game?.settings) !== JSON.stringify(gameSettings_copy));
    }, [gameSettings_copy, game])

    const updateTheGame = async () => {
        console.log("Update the game settings");
        if (game === null) return;
        setLoading(true);

        const resp = await updateGame(game.id, game.name, gameSettings_copy as unknown as string);

        if (resp == null) {
            notify.ShowNotification(notify.location.Top, notify.Severity.Error, "Response is null", "No status");
        } else if (resp.status === 200) {
            setGame(resp.data);
            setGameSettings_copy(resp.data.settings);
            notify.ShowNotification(notify.location.Bottom, notify.Severity.Success, "Game updated successfully");
        } else {
            notify.ShowNotification(notify.location.Top, notify.Severity.Error, resp['message']['error'], "Error: " + resp.status + " " + resp.text)
        }
        setLoading(false);
    };

    // Loading paper
    const loadingPaper = (
        <Paper 
            square={false} 
            sx={{
                padding: 2, 
                marginBottom: 2,
                minWidth: 'fit-content',
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center',
                justifyContent: 'center',
            }}
            >
            <Typography variant='h6'>loading...</Typography>
            <LinearProgress sx={{width: '50%', my: 2, minWidth: 200}}/>
        </Paper>
    );

    // Unkown game type paper
    const unknownGameTypePaper = (
        <Paper 
            square={false} 
            sx={{
                padding: 2, 
                marginBottom: 2,
                minWidth: 'fit-content',
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center',
                justifyContent: 'center',
            }}
            >
            <Typography variant='h4'>Game type {game?.game_type} is not supported</Typography>
        </Paper>
    );
    
    const editGame_active = (
        <Box sx={{display: 'flex', flexDirection: 'row', flexWrap: 'wrap', justifyContent: 'flex-start', alignItems: 'center'}}>
            <Typography variant='body1' sx={{ml: 2}}>Game is active:</Typography>
            <Checkbox
                color="primary"
                inputProps={{ 'aria-label': 'secondary checkbox' }}
                name="visible"
                checked={gameSettings_copy?.active || false}
                onChange={(event: SelectChangeEvent) => {
                        if (gameSettings_copy !== null){
                            const updateGame_Active = {
                                ...gameSettings_copy,
                                active: (event.target as HTMLInputElement).checked
                            }
                            setGameSettings_copy(updateGame_Active);
                        }
                    }
                }
                disabled={loading}
            />
        </Box>
    ); 

    const editGame_enbaleStartTime = (
        <Box sx={{display: 'flex', flexDirection: 'row', flexWrap: 'wrap', justifyContent: 'flex-start', alignItems: 'center'}}>
            <Typography variant='body1' sx={{ml: 2}}>Enable start time:</Typography>
            <Checkbox
                color="primary"
                inputProps={{ 'aria-label': 'secondary checkbox' }}
                name="visible"
                checked={gameSettings_copy?.enable_start_time || false}
                onChange={(event: SelectChangeEvent) => {
                        if (gameSettings_copy !== null){
                            const updateGame = {
                                ...gameSettings_copy,
                                enable_start_time: (event.target as HTMLInputElement).checked
                            }
                            setGameSettings_copy(updateGame);
                        }
                    }
                }
                disabled={loading}
            />
        </Box>
    ); 

    const editGame_enbaleStopTime = (
        <Box sx={{display: 'flex', flexDirection: 'row', flexWrap: 'wrap', justifyContent: 'flex-start', alignItems: 'center'}}>
            <Typography variant='body1' sx={{ml: 2}}>Enable stop time:</Typography>
            <Checkbox
                color="primary"
                inputProps={{ 'aria-label': 'secondary checkbox' }}
                name="visible"
                checked={gameSettings_copy?.enable_stop_time || false}
                onChange={(event: SelectChangeEvent) => {
                        if (gameSettings_copy !== null){
                            const updateGame = {
                                ...gameSettings_copy,
                                enable_stop_time: (event.target as HTMLInputElement).checked
                            }
                            setGameSettings_copy(updateGame);
                        }
                    }
                }
                disabled={loading}
            />
        </Box>
    ); 

    const editGame_setStartTime = (
        <Box
            sx={{
                display: 'flex',
                flexDirection: 'row',
                flexWrap: 'wrap',
                justifyContent: 'space-between',
                alignItems: 'center',
                mx: 2,
                mt: 1,
            }}
        >
            {gameSettings_copy !== null && (
                <DateTimePicker
                    sx={{mr: 1}}
                    disabled={gameSettings_copy.enable_start_time === false}
                    label="Set date and time"
                    value={dayjs(gameSettings_copy.start_time)}
                    views={['year', 'month', 'day', 'hours', 'minutes', 'seconds']}
                    onChange={(date: Dayjs | null) => {
                        if (gameSettings_copy !== null && date !== null) {
                            const dateObj = date.toDate();
                            // Check if the start time is before the stop time
                            if (dateObj < new Date(gameSettings_copy.stop_time) || gameSettings_copy.enable_stop_time === false) {
                                const updateGame = {
                                    ...gameSettings_copy,
                                    start_time: dateObj.toISOString(),
                                };
                                setGameSettings_copy(updateGame);
                            } else {
                                notify.ShowNotification(
                                    notify.location.Bottom,
                                    notify.Severity.Warning,
                                    "Start time is after stop time"
                                );
                            }
                        }
                    }}
                />
            )}
            <Button
                variant="contained"
                sx={{ mt: 1 }}
                onClick={() => {
                    if (gameSettings_copy === null) return;
                    const now = new Date();
                    // Check if the current time is before the stop time
                    if (now < new Date(gameSettings_copy.stop_time) || gameSettings_copy.enable_stop_time === false) {
                        const updateGame = {
                            ...gameSettings_copy,
                            start_time: now.toISOString(),
                        };
                        setGameSettings_copy(updateGame);
                    } else {
                        notify.ShowNotification(
                            notify.location.Bottom,
                            notify.Severity.Warning,
                            "Start time is after stop time"
                        );
                    }
                }}
                disabled={gameSettings_copy?.enable_start_time === false}
            >
                Get now time
            </Button>
        </Box>
    );
    
    const editGame_setStopTime = (
        <Box
            sx={{
                display: 'flex',
                flexDirection: 'row',
                flexWrap: 'wrap',
                justifyContent: 'space-between',
                alignItems: 'center',
                mx: 2,
                mt: 1,
            }}
        >
            {gameSettings_copy !== null && (
                <DateTimePicker
                    sx={{mr: 1}}
                    disabled={gameSettings_copy.enable_stop_time === false}
                    label="Set date and time"
                    value={dayjs(gameSettings_copy.stop_time)}
                    views={['year', 'month', 'day', 'hours', 'minutes', 'seconds']}
                    onChange={(date: Dayjs | null) => {
                        if (gameSettings_copy !== null && date !== null) {
                            const dateObj = date.toDate();
                            // Check if the stop time is after the start time
                            if (dateObj > new Date(gameSettings_copy.start_time)) {
                                const updateGame = {
                                    ...gameSettings_copy,
                                    stop_time: dateObj.toISOString(),
                                };
                                setGameSettings_copy(updateGame);
                            } else {
                                notify.ShowNotification(
                                    notify.location.Bottom,
                                    notify.Severity.Warning,
                                    "Stop time is before start time"
                                );
                            }
                        }
                    }}
                />
            )}
            <Button
                variant="contained"
                sx={{ mt: 1 }}
                onClick={() => {
                    if (gameSettings_copy === null) return;
                    const now = new Date();
                    // Check if the current time is after the start time
                    if (now > new Date(gameSettings_copy.start_time) || gameSettings_copy.enable_start_time === false) {
                        const updateGame = {
                            ...gameSettings_copy,
                            stop_time: now.toISOString(),
                        };
                        setGameSettings_copy(updateGame);
                    } else {
                        notify.ShowNotification(
                            notify.location.Bottom,
                            notify.Severity.Warning,
                            "Stop time is before start time"
                        );
                    }
                }}
                disabled={gameSettings_copy?.enable_stop_time === false}
            >
                Get now time
            </Button>
        </Box>
    );

    const [startTimeString, setStartTimeString] = React.useState<string>("Loading");
    const [stopTimeString, setStopTimeString] = React.useState<string>("Loading");

    const recreateTimeStrings = React.useCallback(() => {
        if (game !== null) {
            const startTime = new Date(game.settings.start_time);
            const stopTime = new Date(game.settings.stop_time);
            const startTimeString = startTime.toLocaleString(navigator.language);
            const stopTimeString = stopTime.toLocaleString(navigator.language);
            const now = new Date(); // Add this line to get the current time
            const timeUntilStart = formatter.relativeTime(dayjs(startTime.toLocaleString("en-US")).toDate(), now);
            const timeUntilStop = formatter.relativeTime(dayjs(stopTime.toLocaleString("en-US")).toDate(), now);

            if (game.settings.enable_start_time === true) {
                setStartTimeString(startTimeString + " - " + timeUntilStart);
            } else {
                setStartTimeString("Not set");
            }

            if (game.settings.enable_stop_time === true) {
                setStopTimeString(stopTimeString + " - " + timeUntilStop);
            } else {
                setStopTimeString("Not set");
            }
        }
    }, [formatter, game]);
    
    React.useEffect(() => {
        const interval = setInterval(() => {
            recreateTimeStrings();
        }, 10000); 

        recreateTimeStrings();

        return () => {
            clearInterval(interval);
        };
    }, [game, recreateTimeStrings]);

    const editDateStart = (
        <Accordion sx={{ bgcolor: pageTheme.palette.action.disabled, minWidth: "200px"}}>
            <AccordionSummary sx={{ bgcolor: pageTheme.palette.action.disabledBackground }}
                expandIcon={<GridExpandMoreIcon />}
                aria-controls="edit_start_time-content"
                id="edit_start_time"
            >
                <Typography>Edit start time</Typography>
            </AccordionSummary>
            <AccordionDetails sx={{ px: 1 }}>
                {editGame_enbaleStartTime}
                {editGame_setStartTime}
            </AccordionDetails>
        </Accordion>
    );

    const editDateStop = (
        <Accordion sx={{ bgcolor: pageTheme.palette.action.disabled, minWidth: "200px" }}>
            <AccordionSummary sx={{ bgcolor: pageTheme.palette.action.disabledBackground }}
                expandIcon={<GridExpandMoreIcon />}
                aria-controls="edit_stop_time-content"
                id="edit_stop_time"
            >
                <Typography>Edit stop time</Typography>
            </AccordionSummary>
            <AccordionDetails sx={{ px: 1 }}>
                {editGame_enbaleStopTime}
                {editGame_setStopTime}
            </AccordionDetails>
        </Accordion>
    );

    const onShare = async () => {
        try {
            const shareData = {
                title: 'Share game',
                text: 'Join this game at: ',
                url: shareLink
            };
            
            if (navigator.share) {
                await navigator.share(shareData);
            } else {
                console.log('Web Share API is not supported in your browser.');
            }
        } catch (error) {
            console.log(error);
        }
    };

    const GameSettingsPaper = (
        <Paper 
            square={false} 
            sx={{
                padding: 2, 
                marginBottom: 2,
                minWidth: 'fit-content',
            }}
            onMouseEnter={() => setHoverOverGameSettings(true)} // set isHovered to true on mouse enter
            onMouseLeave={() => setHoverOverGameSettings(false)} // set isHovered to false on mouse leave
            >
            {/* title block */}
            <Box 
                // sx={{
                //     display: 'flex', 
                //     justifyContent: 'space-between', 
                //     alignItems: 'center'
                // }}
                onMouseEnter={() => setHoverOverGameName(true)} // set isHovered to true on mouse enter
                onMouseLeave={() => setHoverOverGameName(false)} // set isHovered to false on mouse leave
            >
                <Box sx={{display: 'flex', justifyContent: 'space-around', alignItems: 'center', flexWrap: 'wrap'}}>
                    <Box sx={{display: 'flex', flexDirection: 'row', alignItems: 'center', flexWrap: 'wrap', justifyContent: 'center'}}>
                        <Typography variant='h4' sx={{mr: 1, py: 1, whiteSpace: 'nowrap'}}>Edit game:</Typography>
                        {editName ? (
                                <>
                                    <Box sx={{display: 'flex', flexDirection: 'row', alignItems: 'center', py: 1, mb: 2}}>
                                        <TextField id="newGameName" label="New game name" variant="outlined" defaultValue={game?.name} sx={{minWidth: 100}} />

                                        <Done sx={{ cursor: 'pointer', ml: 1}} color='success' onClick={() => {
                                            setEditName(false);
                                            updateGameName((document.getElementById('newGameName') as HTMLInputElement).value);
                                        }}>Done</Done>

                                        <EditOff sx={{ cursor: 'pointer', ml: 1}} color='error' onClick={() => setEditName(false)}>Cancel</EditOff>
                                    </Box>
                                </>
                            ) : (
                                <>
                                    <Box sx={{display: 'flex', flexDirection: 'row', alignItems: 'center', py: 2}}>
                                        <Typography variant='h4' sx={{whiteSpace: 'nowrap'}} onClick={() => setEditName(true)}><strong>{game?.name}</strong></Typography>
                                        {hoverOverGameName ? <EditNote sx={{ cursor: 'pointer', ml: 1 }} onClick={() => setEditName(true)}>edit</EditNote> : <Box sx={{width: '1.5rem', ml: 1}}></Box>}
                                    </Box>
                                </>
                        )}
                    </Box>
                    {/* <DefaultCopyField value={"https://" + window.location.host + "/game/" + game.id} label="Share link" disabled  sx={{minWidth: 150, width: 250, ml: 2}}>
                        { "https://" + window.location.host + "/game/" + game.id}
                    </DefaultCopyField> */}
                    <Box sx={{display: 'flex', flexWrap: 'wrap', alignItems: 'center', justifyContent: 'center', width: 'fit-content'}}>
                        <Box sx={{display: 'flex', flexWrap: 'nowarp', alignItems: 'center'}}>
                            <TextField
                                label="Play game | Share link"
                                // value={"https://" + window.location.host + "/game/" + game?.id}
                                value={shareLink}
                                slotProps={{
                                    input: {
                                        readOnly: true,
                                    },
                                }}
                                sx={{minWidth: 100, ml: 2, }}
                            />
                            <Box sx={{alignContent: 'center', display: 'flex', justifyContent: 'center', justifyItems: 'center', height: '100%'}}>
                                <IconButton 
                                    aria-label="copy link"
                                    sx={{
                                        ml: 1,
                                        '&:hover': {
                                        color: pageTheme.palette.primary.main,
                                        },
                                        height: 'fit-content',
                                        py: 'auto',
                                        alignContent: 'center'
                                    }}

                                    onClick={() => {
                                        const link = `https://${window.location.host}/game/${game?.id}`;
                                        navigator.clipboard.writeText(link);
                                        notify.ShowNotification(notify.location.Bottom, notify.Severity.Info, "Copied to clipboard")
                                    }}
                                >
                                    <ContentCopy />
                                </IconButton >
                            </Box>
                        </Box>
                        <Box sx={{alignContent: 'center'}}>
                            <IconButton 
                                aria-label="Share modal"
                                sx={{
                                    '&:hover': {
                                    color: pageTheme.palette.primary.main,
                                    },
                                    height: 'fit-content',
                                    py: 'auto',
                                    m: 1
                                }}

                                onClick={onShare}
                            >
                                <Share />
                            </IconButton >
                            <IconButton 
                                aria-label="Share modal"
                                sx={{
                                    '&:hover': {
                                    color: pageTheme.palette.primary.main,
                                    },
                                    height: 'fit-content',
                                    py: 'auto',
                                    m: 1
                                }}

                                onClick={() => setOpenShareModal(true)}
                            >
                                <QrCode />
                            </IconButton >
                        </Box>
                    </Box>

                    <Button
                        variant="outlined"
                        color="secondary"
                        sx={{ ml: 2, my: 1}}
                        LinkComponent="a"
                        href={`/game/${game?.id}/result`}
                    >
                        Go to Result Page
                    </Button>
                </Box>
            </Box>
            <hr style={{backgroundColor: pageTheme.palette.divider, color: pageTheme.palette.divider, height: '2px', borderWidth: 0}} />
            <Box sx={{display: 'flex', justifyContent: 'space-between', alignItems: 'center', px: 4, flexWrap: 'wrap'}} onClick={() => setHoverOverGameSettings(true)}>
                <Box sx={{py: 1}}>
                    <Typography variant='body1'>Game ID: {game?.id}</Typography>
                    <Typography variant='body1'>Game variant: {game === null ? '' : gameList[game?.game_type].name}</Typography>
                    {game?.settings.active ? 
                        <Typography variant='body1' color={'green'}>Game is <strong>active</strong></Typography>
                        :<Typography variant='body1' color={'red'}>Game is <strong>disabled</strong></Typography>
                    }
                    <Typography variant='body1'>Start time: {startTimeString}</Typography>
                    <Typography variant='body1'>Stop time: {stopTimeString}</Typography>
                </Box>
                <Box>
                    <Typography variant='body1'>Created at: {new Date(game?.created_at ?? '').toLocaleString(navigator.language)}</Typography>
                    <Typography variant='body1'>Updated at: {new Date(game?.date_edit ?? '').toLocaleString(navigator.language)}</Typography>
                    <Typography variant='body1'>Active users: {activeUsers}</Typography>
                </Box>
            </Box>
            {hoverOverGameSettings &&
                <Box sx={{height: 0, width: '100%', position: 'relative'}}>
                    <EditNote sx={{ cursor: 'pointer', ml: 1, position: 'absolute', right: 0, bottom: 0 }} onClick={() => setEditGameSettings(!editGameSettings)}>
                        edit
                    </EditNote>
                </Box>
            }
            <Collapse in={editGameSettings}>
                <hr style={{backgroundColor: pageTheme.palette.divider, color: pageTheme.palette.divider, height: '2px', borderWidth: 0}} />
                {/* <Box sx={{display: 'flex', justifyContent: 'flex-start', alignItems: 'flex-start', flexWrap: 'wrap', px: 4}}> */}
                <Box sx={{display: 'flex', justifyContent: 'space-between', alignItems: 'center', px: 4, flexWrap: 'wrap'}}>
                    <Box sx={{py: 1}}>
                        {editGame_active}
                    </Box>
                    <Box sx={{py: 1}}>
                        {editDateStart}
                        {editDateStop}
                    </Box>
                </Box>	
                <hr style={{backgroundColor: pageTheme.palette.divider, color: pageTheme.palette.divider, height: '2px', borderWidth: 0}} />
                <Box sx={{display: 'flex', justifyContent: 'space-between', mt: 2, px: 2}}>
                    <Button variant='contained' color='info' onClick={()=>setEditGameSettings(false)}>Close</Button>
                    {changesPending && <Typography variant='body1' color={'orange'} marginTop={1} marginX={1}><strong>Changes pending!</strong></Typography>}
                    <Button variant='contained' color='success' disabled={loading || !changesPending} onClick={updateTheGame}>Update settings</Button>
                </Box>
            </Collapse>
        </Paper>
    );

    const copyShareLinkToClipboard = () => {
        // Copy the QR code and the link to the clipboard
        const svgElement = document.querySelector('svg#qr_code');
        if (svgElement) {
            const svgData = new XMLSerializer().serializeToString(svgElement);
            const canvas = document.createElement('canvas');
            const ctx = canvas.getContext('2d');
            const img = new Image();
            img.onload = () => {
                canvas.width = img.width;
                canvas.height = img.height;
                ctx?.drawImage(img, 0, 0);
                canvas.toBlob((blob) => {
                    if (blob) {
                        const item = new ClipboardItem({ 'image/png': blob });
                        navigator.clipboard.write([item]);
                        navigator.clipboard.writeText(shareLink);
                        notify.ShowNotification(notify.location.Bottom, notify.Severity.Info, "QR code and link copied to clipboard");
                    }
                }, 'image/png');
            };
            img.src = 'data:image/svg+xml;base64,' + btoa(svgData);
        }
    }
 
    function shareModal() {
        return (
            <Dialog
                open={openShareModal}
                onClose={() => {setOpenShareModal(false);}}
                aria-describedby="shareModal"
            >
                <DialogTitle id="share_modal">Share game</DialogTitle>
                <Divider />
                <DialogContent sx={{display: 'flex', flexDirection: 'column', alignItems: 'center', justifyContent: 'center'}}>
                    {/* <Box sx={{ background: 'white', p: '16px', display: 'flex', justifyContent: 'center' }}> */}
                        {/* <Box sx={{ width: '100%', maxWidth: '300px', height: 'auto', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                            {/* <QRCode value={shareLink} style={{ width: '100%', height: 'auto', maxWidth: '100%' }}/> */}
                        {/* </Box> */}
                        <QRCode
                            size={256}
                            style={{ height: "auto", maxWidth: "100%", width: "100%" }}
                            value={shareLink}
                            viewBox={`0 0 256 256`}
                            id='qr_code'
                        />
                    {/* <DialogContentText id="share_modal-text" color="textPrimary">
                        Share QR code and links
                    </DialogContentText> */}
                </DialogContent>
                {/* <Typography variant="body2" sx={{ fontStyle: 'italic', px: 1, mt: -2}} color="gray">Share</Typography> */}
                <Divider />
                <DialogActions>
                    <Box sx={{display: 'flex', justifyContent: "space-around", flexWrap: 'wrap', width: '100%', overflowWrap: 'break-word', alignItems: 'center'}}>
                        <IconButton  color='secondary'
                            aria-label="copy qr code"
                            sx={{'&:hover': { color: pageTheme.palette.primary.main, }, height: 'fit-content'}}
                            onClick={copyShareLinkToClipboard}
                        >
                            <ContentCopy  />
                        </IconButton >
                        <IconButton  color='secondary'
                            aria-label="share qr code"
                            sx={{'&:hover': { color: pageTheme.palette.primary.main, }, height: 'fit-content'}}
                            onClick={onShare}
                        >
                            <Share />
                        </IconButton >
                        <Button onClick={() => {setOpenShareModal(false)}}>Close</Button>
                    </Box>
                </DialogActions>
            </Dialog>
        )
    }

    React.useEffect(() => {
        getNewPoints();
    }, [gameID, getNewPoints, windowIsDefined]);

        return(
        <LocalizationProvider dateAdapter={AdapterDayjs} adapterLocale={navigator.language.toLowerCase().split('-')[0]}>
            <ThemeProvider theme={pageTheme}>
                <CssBaseline>
                    {shareModal()}
                    <Suspense fallback={loadingPaper}>
                        <LazyNavbar />
                    </Suspense>
                    { loading && <PageLoader />}
                    <Container maxWidth="xl">
                        {/* Check for game */}
                        {game === null && 
                        <>
                            <Container sx={{display: 'flex', flexDirection: 'column', alignItems: 'center', justifyContent: 'center', height: '100vh'}}>
                                <Typography variant='h4'>Game is loading...</Typography>
                                <LinearProgress sx={{width: '50%', my: 2, minWidth: 200}}/>
                            </Container>                    
                        </>
                        }
                        {/* Display game */}
                        {game !== null && 
                        <> 
                            {GameSettingsPaper}
                            {loading && <LinearProgress />}
                            {points !== null && points !== undefined && (game?.game_type === gameType.Game_1 ? (
                                <Suspense fallback={loadingPaper}>
                                    <Game_1 game={game} points={points} selectedInTable={selectedInTable} />
                                </Suspense>
                            ) : game?.game_type === gameType.Game_2 ? (
                                <Suspense fallback={loadingPaper}>
                                    <Game_2 game={game} />
                                </Suspense>
                            ) : (
                                unknownGameTypePaper                          
                            ))}
                            {points !== null && points !== undefined &&  <PointsTable points={points} setSelectedInTable={setSelectedInTable}/>}
                            {loadingPoints && <>
                                Loading all the points to the tabel
                                <LinearProgress />
                            </>}
                        </>}
                    </Container>
                </CssBaseline>
            </ThemeProvider>
        </LocalizationProvider>
    )
}
