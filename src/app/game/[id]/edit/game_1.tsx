import React, { useMemo } from 'react';
import { AddCircleRounded, Delete, MyLocation, RestoreFromTrashRounded } from '@mui/icons-material';
import { Box, Button, Checkbox, CircularProgress, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, Divider, FormControl, InputLabel, LinearProgress, MenuItem, Paper, Select, SelectChangeEvent, Slide, Stack, TextField, Typography, getContrastRatio } from '@mui/material';
import { currentLocation, GetGeolocation, loadingLocation, locationAge, locationDistance, locationSpeed } from '@/tools/geolocation';
import { MapContainer, TileLayer, Marker, Popup, useMapEvents, Circle } from 'react-leaflet';
import "leaflet/dist/leaflet.css";
import { Icon } from 'leaflet';
import * as notify from '@/app/notify';
import { createGamePoint, deleteGamePoint, updateGamePoint } from '@/tools/api/gamePointsAPI';
import NumericInput from 'react-numeric-input';
import { TransitionProps } from '@mui/material/transitions';
import LoadingButton from '@mui/lab/LoadingButton';
import { colorList, defaultColor, defaultLocation } from '@/tools/gameTypes';
import { LocationData, GameLocationsSettings, GamePoint, MarkerData, customMarker, circleFill, minRadius, defaultLocationSettings, minTimeToActivate } from '@/tools/gameData/game_1_types';
import { pageTheme } from '@/app/theme';
import { selectRow } from '@/app/game/[id]/edit/listPoints';
import { NumericFormat } from 'react-number-format';

const Transition = React.forwardRef(function Transition(
    props: TransitionProps & {
        children: React.ReactElement<any, any>;
    },
    ref: React.Ref<unknown>,
) {
    return <Slide direction="up" ref={ref} {...props} />;
});

const goToLocation = (lat: number, lng: number) => {
    if (lat != null && lng != null && mapRef.current != null){
        // mapRef.current.flyTo([lat, lng], mapRef.current.getZoom());
        // mapRef.current.fitBounds([[lat, lng]]);
        const zoomValue = mapRef.current.getBoundsZoom([[lat, lng], [lat, lng]]);
        mapRef.current.flyTo([lat, lng], zoomValue > 2 ? zoomValue - 2 : zoomValue);
    } 
    else
    {
        console.log('No location found');
    }
}

var mapRef: any;
var circels: LocationData[] = [];
var selectedCircle: LocationData | null;
var selectedCircle_backup: LocationData | null;
var markerInLocation: boolean;
var markerPos: [number, number] | null;
var markerRef: any;
var setCircels: React.Dispatch<React.SetStateAction<LocationData[]>>;
var setSelectedCircle: React.Dispatch<React.SetStateAction<LocationData | null>>;
var setSelectedCircle_backup: React.Dispatch<React.SetStateAction<LocationData | null>>;
var setMarkerInLocation: React.Dispatch<React.SetStateAction<boolean>>;
var setMarkerPos: React.Dispatch<React.SetStateAction<[number, number] | null>>;

// Game_1 function with props, the game and the game points
export default function Game_1({ game, points, selectedInTable }: { game: any, points: any, selectedInTable: number}) {
    const defaultZoom = 13; // default zoom level
    mapRef = React.useRef<any>(null);
    [circels, setCircels] = React.useState<LocationData[]>([]);
    markerRef = React.useRef<any>(null);
    // const selectedCircle = signal<LocationData | null>(null);
    [selectedCircle, setSelectedCircle] = React.useState<LocationData | null>(null);
    [selectedCircle_backup, setSelectedCircle_backup] = React.useState<LocationData | null>(null);
    const [loadingMapData, setLoadingMapData] = React.useState(true);
    const [toMyLocOnStart, setToMyLocOnStart] = React.useState(false);
    const [moveSelected, setMoveSelected] = React.useState(false);    

    var L = require('leaflet');

    // enable get geolocation
    GetGeolocation();

    // if a location is found, set the center of the map to the location, do this only once
    // and add marker to the map 
    
    // TODO: -------------------------------------------------------------- netjes maken --------------------------------------------------------------
    // select a circle on the map
    const selectTheCircle = React.useCallback((pintID: number) => {
        if (typeof window === 'undefined') return;

        // Prevent selecting the same circle
        if (selectedCircle?.data.id === pintID) return;

        // console.log("Select the circle: %d", pintID);
        
        circels.forEach((circle) => {
            // Add drag event to the circle
            if (circle.data.id === pintID) {
                circle.leaflet.on({mousedown: function() {
                    mapRef.current.on('mousemove', function(e: any) {
                        circle.leaflet.setLatLng(e.latlng);
                        setSelectedCircle((prevState) => {
                            if (prevState) {
                                const updatedCircle: LocationData = {
                                    ...prevState,
                                    data: {
                                        ...prevState.data,
                                        latitude: parseFloat(e.latlng.lat.toFixed(6)),
                                        longitude: parseFloat(e.latlng.lng.toFixed(6)),
                                    },
                                    leaflet: prevState.leaflet.setLatLng(e.latlng)
                                };
                                return updatedCircle;
                            }
                            return prevState;
                        });
                    });
                    mapRef.current.dragging.disable();
                    mapRef.current.scrollWheelZoom.disable();
                    mapRef.current.doubleClickZoom.disable();
                    mapRef.current.boxZoom.disable();
                    mapRef.current.keyboard.disable();                    
                }});
                mapRef.current.on('mouseup', function() {
                    mapRef.current.removeEventListener('mousemove');
                    mapRef.current.dragging.enable();
                    mapRef.current.scrollWheelZoom.enable();
                    mapRef.current.doubleClickZoom.enable();
                    mapRef.current.boxZoom.enable();
                    mapRef.current.keyboard.enable();
                });

                const popupDefault = `<div style="text-align: center;">
                        <p>Move this circle?</p>
                        <div style="display: flex; justify-content: space-between;">
                            <button id="moveYes" style="background-color: green; color: white; border: none; border-radius: .3rem; padding: 5px 10px; cursor: pointer; box-shadow: 0px 4px 6px rgba(0, 0, 0, 0.1);">Yes</button>
                            <button id="moveNo" style="background-color: red; color: white; border: none; border-radius: .3rem; padding: 5px 10px; cursor: pointer; box-shadow: 0px 4px 6px rgba(0, 0, 0, 0.1);">No</button>
                        </div>
                    </div>`;

                // Add move circle popup (mainly for mobile).
                const yesAction = () => {
                    setMoveSelected(true);
                    circle.leaflet.setPopupContent(
                        `<div style="text-align: center;">
                            <p>Click on the map where the circle needs to move to</p>
                            <button id="moveNo" style="background-color: red; color: white; border: none; border-radius: .3rem; padding: 5px 10px; cursor: pointer; box-shadow: 0px 4px 6px rgba(0, 0, 0, 0.1);">Cancel</button>
                        </div>`
                    ).openPopup();
                };

                const noAction = () => {
                    const circle = circels.find(c => c.data.id === selectedCircle?.data.id);
                    // mapRef.current.off('click');
                    circle?.leaflet.closePopup();
                    setMoveSelected(false);
                    circle?.leaflet.setPopupContent(popupDefault);
                }
            
                window.addEventListener('click', function(e) {
                    const target = e.target as HTMLElement;
                    if (target && target.id === 'moveNo') {
                        noAction();
                    }
                    if (target && target.id === 'moveYes') {
                        yesAction();
                    }
                });

                circle.leaflet.bindPopup(popupDefault).closePopup();

                // When the backup is not empty, set the selected circle to the backup
                if (selectedCircle_backup) {
                    setSelectedCircle(selectedCircle_backup);
                    selectedCircle?.leaflet.setLatLng([selectedCircle_backup.data.latitude, selectedCircle_backup.data.longitude]);                    
                }

                // Set the selected circle
                setSelectedCircle(circle);
                setSelectedCircle_backup(circle);
                goToLocation(circle.data.latitude, circle.data.longitude);
                setMarkerInLocation(true);
                setMarkerPos(null);

                if (selectedCircle) {
                    // Change the color of the previously selected circle back to its original color
                    selectedCircle.leaflet.setStyle({
                        fillColor: selectedCircle.data.color,
                        fillOpacity: selectedCircle.data.settings?.disabled ? circleFill.notActive : circleFill.default
                    });
                }

                // Change the color of the selected circle to gray
                circle.leaflet.setStyle({
                    fillColor: 'dark-gray',
                    fillOpacity: circleFill.active,
                });

                const marker = markerRef.current;
                if (marker != null) {
                    marker.closePopup();
                }
            }
        });
    }, []);
    
    
    React.useEffect(() => {
        console.log("game_1.tsx - selectIndex: ", selectedInTable);
        selectTheCircle(selectedInTable);
    }, [selectedInTable, selectTheCircle]);

    

    const selectCircle = React.useCallback((circle: LocationData) => {
        // Update the selected circle
        selectTheCircle(circle.data.id);
        selectRow(circle.data.id);
    }, [selectTheCircle]);   

    // add the circles to the circles array and to the map
    const arePointsNotNull = points != null; // Extract 'points != null' into a separate variable
    const isMapRefNotNull = mapRef.current != null; // Extract 'mapRef.current != null' into a separate variable
    const windowsIsLoaded = typeof window !== 'undefined';

    React.useEffect(() => {
        const leaflet = L; // Extract 'L' into a separate variable

        if (isMapRefNotNull) {
            console.log("Update circles");
            setCircels([]);
            setSelectedCircle(null);
            setSelectedCircle_backup(null);
            // Remove all circles from the map
            circels.forEach((circle) => {
                circle.leaflet.remove();
            });

            points.forEach((point: GamePoint) => {
                const pointOnMap = Object.values(mapRef.current._layers).find((layer: any) => {
                    if (layer instanceof leaflet.Circle) { // Use the extracted 'leaflet' variable
                        const circleBounds = layer.getBounds();
                        const pointBounds = leaflet.latLng(point.latitude, point.longitude).toBounds(point.radius);
                        return circleBounds.contains(pointBounds);
                    }
                    return false;
                });

                if (!pointOnMap) {
                    var leafletCircle = leaflet.circle([point.latitude, point.longitude], {
                        color: point.color,
                        fillColor: point.settings?.visible ? point.color : 'black',
                        fillOpacity: point.settings?.disabled ? circleFill.notActive : circleFill.default,
                        radius: point.radius
                    }).addTo(mapRef.current)
                    .on('click', () => selectCircle({ data: point, leaflet: leafletCircle }))
                    setCircels((prevCircels) => [...prevCircels, { data: point, leaflet: leafletCircle }]);
                }
            });

            if (loadingMapData === true) {
                // Zoom map to fit all circles in the view
                if (points.length > 0) {
                    const bounds: L.LatLngBounds = new L.LatLngBounds(points.map((point: GamePoint) => new L.LatLng(point.latitude, point.longitude)));
                    mapRef.current.fitBounds(bounds);
                }
            }

            setLoadingMapData(false);
        }
    }, [L, points, arePointsNotNull, isMapRefNotNull, selectCircle, loadingMapData]);

    // add marker on click position
    [markerPos, setMarkerPos] = React.useState<[number, number] | null>(null);
    [markerInLocation, setMarkerInLocation] = React.useState(false);
    // Remove the following lines
    // const [selectedCircle, setSelectedCircle] = React.useState<any>(null);

    

    const handleMarkerPos = React.useCallback((lat: number, lng: number) => {
        const marker = markerRef.current;
        // check if the marker is placed in a circle
        let foundCircle = null;
        circels.forEach((circle) => {
            if (circle.leaflet.getBounds().contains([lat, lng])) {
                foundCircle = circle;
            }
        })
        
        if (foundCircle === null){
            if (selectedCircle !== null){
                selectedCircle.leaflet.setStyle({
                    fillOpacity: selectedCircle.data.settings?.disabled ? circleFill.notActive : circleFill.default,
                    fillColor: selectedCircle.data.color
                });

                selectedCircle.leaflet.setLatLng([selectedCircle_backup?.data.latitude || selectedCircle.data.latitude, selectedCircle_backup?.data.longitude || selectedCircle.data.longitude]);
                setSelectedCircle(null);
                setSelectedCircle_backup(null);
            }
            setMarkerPos([lat, lng]);
            goToLocation(lat, lng);
            setMarkerInLocation(false);
            if (marker != null){
                marker.openPopup();
            }
        }else{
            setMarkerPos(null);
            selectCircle(foundCircle);            
        }
    }, [selectCircle]);
    
    function HandleEvent(){
        const map = useMapEvents({
            click(e){
                if (moveSelected) {
                    const circle = circels.find(c => c.data.id === selectedCircle?.data.id);
                    setSelectedCircle((prevState) => {
                        if (prevState) {
                            const updatedCircle: LocationData = {
                                ...prevState,
                                data: {
                                    ...prevState.data,
                                    latitude: parseFloat(e.latlng.lat.toFixed(6)),
                                    longitude: parseFloat(e.latlng.lng.toFixed(6)),
                                },
                                leaflet: prevState.leaflet.setLatLng(e.latlng)
                            };
                            return updatedCircle;
                        }
                        return prevState;
                    });
                    circle?.leaflet.setLatLng(e.latlng);
                    setMoveSelected(false);
                    notify.ShowNotification(notify.location.Bottom, notify.Severity.Warning, "Click on 'Update point' to save the changes");
                } else {
                    const lat = e.latlng.lat;
                    const lng = e.latlng.lng;
                    handleMarkerPos(lat, lng);
                }
            },
            moveend(){
                const marker = markerRef.current;
                if (marker != null && !map.getBounds().contains(marker.getLatLng())) {
                    marker.closePopup();
                }
            }
        });
        return null
    }
 
    
    const markerEventHandler = useMemo(() => ({
        dragend(){
            const marker = markerRef.current;
            if (marker != null) {
                const lat = marker.getLatLng()['lat'];
                const lng = marker.getLatLng()['lng'];
                handleMarkerPos(lat, lng);
            }
        },
        onclick(){
            // open popup
            const marker = markerRef.current;
            if (marker != null && markerInLocation === false) {
                marker.openPopup();
            }
        }
    }), [handleMarkerPos]);

    const [enableAdd, setEnableAdd] = React.useState(true);

    const addNewCircle = async () => {
        if (markerPos === null || markerRef.current === null || markerRef.current.getLatLng() === null) {
            console.log("Marker position is null");
            return;
        }

        // Check if the radius is valid
        if (radius < minRadius) {
            notify.ShowNotification(notify.location.Top, notify.Severity.Error, "Radius is too small", "Minimum radius is " + minRadius + " meters");
            return;
        }

        setEnableAdd(false);
        const lat = markerPos[0];
        const lng = markerPos[1];
        console.log("New location on %f, %f with radius %d m", lat, lng, radius);

        var settings:GameLocationsSettings = defaultLocationSettings;

        const resp = await createGamePoint(game.id, lat, lng, radius, defaultColor.color, settings as unknown as string);

        if (resp == null) {
            notify.ShowNotification(notify.location.Top, notify.Severity.Error, "Response is null", "No status");
            return;
        } else if (resp.status === 200) {
            notify.ShowNotification(notify.location.Bottom, notify.Severity.Success, "Location add succesfuly");
        } else {
            notify.ShowNotification(notify.location.Top, notify.Severity.Error, resp['message']['error'], "Error: " + resp.status + " " + resp.text)
        }
        if (markerRef.current != null && markerRef.current.isPopupOpen()) {
            markerRef.current.closePopup();
        }
        setMarkerPos(null);
        setEnableAdd(true);        
    };

    const [radius, setRadius] = React.useState(10);

    const [openAlert_remove, setOpenAlert_remove] = React.useState(false);
    const handleCloseAlert_remove = () => {
        setOpenAlert_remove(false);
    };
    const [openAlert_resetAll, setOpenAlert_resetAll] = React.useState(false);
    const handleCloseAlert_resetAll = () => {
        setOpenAlert_resetAll(false);
    };
    const [openAlert_question, setOpenAlert_question] = React.useState(false);
    const handleCloseAlert_question = () => {
        setOpenAlert_question(false);
        notify.ShowNotification(notify.location.Bottom, notify.Severity.Warning, "Click on 'Update point' to save the changes");
    };

    const [delPoint, setDelPoint] = React.useState<any>(null);
    const HandleDeletePoint = async () => {
        if (selectedCircle !== null){
            console.log("Remove %d", selectedCircle?.data.id);
            setDelPoint(true);

            const resp = await deleteGamePoint(selectedCircle.data.game_id, selectedCircle.data.id);
            if (resp === null) {
                notify.ShowNotification(notify.location.Top, notify.Severity.Error, "Somthing went wrong", "Error")
            } else if (resp.status === 200) {
                notify.ShowNotification(notify.location.Bottom, notify.Severity.Success, "Point succesfuly deleted");
            } else {
                notify.ShowNotification(notify.location.Top, notify.Severity.Error, resp['message']['error'], "Error: " + resp.status + " " + resp.text)
            }
            setDelPoint(false);
            handleCloseAlert_remove();
        }
    }

    const [resetAllPoints, setResetAllPoints] = React.useState<any>(null);
    const [resetAllPoints_color, setResetAllPoints_color] = React.useState<string>(defaultColor.color);
    const HandleResetAllPoints = async () => {
        console.log("Reset all points to color %s and no owner.", resetAllPoints_color);
        setResetAllPoints(true);

        if (resetAllPoints_color === null){
            notify.ShowNotification(notify.location.Bottom, notify.Severity.Error, "Choose a color");
        }
        else {
            points.forEach(async (point: GamePoint) => {
                point.owner = "";
                point.color = resetAllPoints_color;
                const resp = await updateGamePoint(
                    point.game_id,
                    point.id, 
                    point.latitude,
                    point.longitude, 
                    point.radius,
                    point.owner,
                    point.color,
                    point.settings as unknown as string);
                
                if (resp === null) {
                    notify.ShowNotification(notify.location.Top, notify.Severity.Error, "Somthing went wrong", "Error")
                } 
                else if (resp.status === 200) {
                    if (resp.data === null) {
                        notify.ShowNotification(notify.location.Top, notify.Severity.Error, "Somthing went wrong", "Error")
                    }
                    else {
                        notify.ShowNotification(notify.location.Bottom, notify.Severity.Success, "All locations reset succesfuly");
                    }
                }
                else {
                    notify.ShowNotification(notify.location.Top, notify.Severity.Error, resp['message']['error'], "Error: " + resp.status + " " + resp.text)
                }     
            });
        }

        setResetAllPoints(false);
        handleCloseAlert_resetAll();
    }

    function AlertDialogSlide_remove() {
        return (
            <Dialog
                open={openAlert_remove}
                onClose={handleCloseAlert_remove}
                PaperProps={{
                    component: 'form',
                    onSubmit: (event: React.FormEvent<HTMLFormElement>) => {
                        event.preventDefault();
                        HandleDeletePoint();
                    },
                }}
            >
                <DialogTitle><strong>{"Delete point"} {selectedCircle?.data.id}</strong></DialogTitle>
                <DialogContent>
                    <DialogContentText id="alert-dialog-slide-description">
                        <Typography variant="body1">
                            Do you really want to <strong>permanently delete</strong> this point?
                        </Typography>
                    </DialogContentText>
                </DialogContent>
                <DialogActions sx={{ justifyContent: 'space-between', px: 3, mb: 2 }}>
                    <Button variant='outlined' color='error' type="submit" disabled={delPoint}>Agree</Button>
                    <Button variant='contained' color='success' onClick={handleCloseAlert_remove}>Cancel</Button>
                </DialogActions>
            </Dialog>
        );
    }

    const colorArray = [defaultColor, ...Object.values(colorList)];
    
    function AlertDialogSlide_resetAll() {
        return (
            <Dialog
                open={openAlert_resetAll}
                TransitionComponent={Transition}
                keepMounted
                onClose={handleCloseAlert_resetAll}
                aria-describedby="alert-dialog-slide-description"
                PaperProps={{
                    component: 'form',
                    onSubmit: (event: React.FormEvent<HTMLFormElement>) => {
                        event.preventDefault();
                        HandleResetAllPoints();
                    },
                }}
            >
                <DialogTitle><strong>Reset ALL points</strong></DialogTitle>
                <DialogContent>
                    <DialogContentText id="alert-dialog-slide-description">
                        Do you really want to <strong>reset ALL</strong> the points?
                    </DialogContentText>
                    <FormControl fullWidth sx={{ mt: 2 }}>
                        <InputLabel id="color-select-label">Choose color</InputLabel>
                        <Select
                            labelId="color-select-label"
                            id="color-select"
                            value={resetAllPoints_color ? colorArray.findIndex(color => color.color === resetAllPoints_color).toString() : ''}
                            label="Choose color"
                            onChange={(event: SelectChangeEvent) => setResetAllPoints_color(colorArray[parseInt(event.target.value as string)].color)}
                        >
                            {colorArray.map((color, index) => {
                                const contrast = getContrastRatio(color.color, "#fff");
                                const textColor = contrast >= 4.5 ? "#fff" : "#000";
                                return (
                                    <MenuItem key={index} value={index} sx={{ backgroundColor: color.color, color: textColor }}>
                                        {color.name}
                                    </MenuItem>
                                );
                            })}
                        </Select>
                    </FormControl>
                </DialogContent>
                <DialogActions sx={{ justifyContent: 'space-between', px: 3, mb: 2 }}>
                    <Button variant='outlined' color='error' onClick={HandleResetAllPoints} disabled={resetAllPoints}>Agree</Button>
                    <Button variant='contained' color='success' onClick={handleCloseAlert_resetAll}>Cancel</Button>
                </DialogActions>
            </Dialog>
        );
    }
    
    function AlertDialogSlide_question() {
        return (
            <Dialog
                open={openAlert_question}
                onClose={handleCloseAlert_question}
                aria-describedby="alert-dialog-slide-description"
                PaperProps={{
                    component: 'form',
                    onSubmit: (event: React.FormEvent<HTMLFormElement>) => {
                        event.preventDefault();
                        // Check if the question is valid
                        if (!(event.target as HTMLFormElement).question.value) {
                            notify.ShowNotification(notify.location.Bottom, notify.Severity.Error, "Question is required");
                            return;
                        }
                        // Check if there are at least two answers
                        if (answers.length < 2 || answers.every(answer => answer === '')) {
                            notify.ShowNotification(notify.location.Bottom, notify.Severity.Error, "At least two answers are required");
                            return;
                        }
                        // Check if the correct answer is selected
                        if (selectedCircle?.data.settings?.correctAnswer === undefined 
                            || selectedCircle.data.settings.correctAnswer === null 
                            || selectedCircle.data.settings.correctAnswer < 0 
                            || selectedCircle.data.settings.correctAnswer >= answers.length
                        ) {
                            notify.ShowNotification(notify.location.Bottom, notify.Severity.Error, "Correct answer is required");
                            return;
                        }
                        setSelectedCircle((prevState) => {
                            if (prevState) {
                                const updatedCircle: LocationData = {
                                    ...prevState,
                                    data: {
                                        ...prevState.data,
                                        settings: {
                                            ...prevState.data.settings,
                                            enableQuestion: true,
                                            question: (event.target as HTMLFormElement).question.value,
                                            answers: answers,
                                            correctAnswer: prevState.data.settings?.correctAnswer,
                                        }
                                    }
                                };
                                console.log(updatedCircle);
                                return updatedCircle;
                            }
                            return prevState;
                        });
                        handleCloseAlert_question();
                    },
                }}
            >
                <DialogTitle><strong>Add question to point</strong></DialogTitle>
                <Divider />
                <DialogContent>
                    <DialogContentText id="alert-dialog-slide-description" color="text.primary">
                        Enter the question and the answers for the point.
                    </DialogContentText>
                    <TextField
                        defaultValue={selectedCircle?.data.settings?.question || ''}
                        autoFocus
                        margin="dense"
                        id="question"
                        label="Question"
                        type="text"
                        fullWidth
                        required
                        onChange={(e) => {
                            const maxLength = 100;
                            e.target.value = e.target.value.slice(0, maxLength);
                        }}
                        multiline
                    />
                    <Box sx={{ mt: 2 }}>
                        <Typography component="div" variant="body1">Choose the correct answer by checking the checkbox.</Typography>
                        {answers.map((answer, index) => (
                            <Box key={index} sx={{ display: 'flex', alignItems: 'center', mt: 1 }}>
                                <Checkbox
                                    checked={selectedCircle?.data.settings?.correctAnswer === index}
                                    onChange={() => {
                                        setSelectedCircle((prevState) => {
                                            if (prevState) {
                                                const updatedCircle: LocationData = {
                                                    ...prevState,
                                                    data: {
                                                        ...prevState.data,
                                                        settings: {
                                                            ...prevState.data.settings,
                                                            correctAnswer: index,
                                                        }
                                                    }
                                                };
                                                return updatedCircle;
                                            }
                                            console.log(prevState);
                                            return prevState;
                                        });
                                    }}
                                />
                                <TextField
                                    fullWidth
                                    label={`Answer ${index + 1}`}
                                    value={answer}
                                    onChange={(e) => handleAnswerChange(e, index)}
                                    required
                                />
                                <Button
                                    variant="contained"
                                    color="error"
                                    onClick={() => removeAnswer(index)}
                                    sx={{ mx: 1 }}
                                >
                                    <Delete sx={{mx: -1}} />
                                </Button>
                                
                            </Box>
                        ))}
                        {answers.length < 5 && (
                            <Box sx={{ mt: 2, display: 'flex', justifyContent: 'center' }}>
                                <Button
                                    variant="contained"
                                    color="primary"
                                    onClick={addAnswer}
                                    startIcon={<AddCircleRounded />}
                                    component="span"
                                >
                                    Add answer
                                </Button>
                            </Box>
                        )}
                    </Box>
                </DialogContent>
                <Divider />
                <DialogActions>
                    <Button onClick={handleCloseAlert_question} color="primary" sx={{ marginRight: 'auto' }}>
                        Cancel
                    </Button>
                    <Button type="submit" color="secondary" sx={{ marginLeft: 'auto' }}>
                        Save
                    </Button>
                </DialogActions>
            </Dialog>
        );
    }

    const [answers, setAnswers] = React.useState<string[]>(['']);

    const handleAnswerChange = (e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>, index: number) => {
        const newAnswers = [...answers];
        newAnswers[index] = e.target.value.slice(0, 50);
        setAnswers(newAnswers);
    };

    const addAnswer = () => {
        setAnswers([...answers, '']);
    };

    const removeAnswer = (index: number) => {
        const newAnswers = answers.filter((_, i) => i !== index);
        setAnswers(newAnswers);
    };

    const updatePoint = async () => {
        if (selectedCircle !== null){
            console.log("Update %d", selectedCircle?.data.id);
            // check if the radius is valid
            if ((selectedCircle?.data.radius ?? 0) < minRadius) {
                setSelectedCircle((prevState) => {
                    if (prevState) {
                        const updatedCircle: LocationData = {
                            ...prevState,
                            data: {
                                ...prevState.data,
                                radius: minRadius,
                            }
                        };
                        return updatedCircle;
                    }
                    return prevState;
                });
                notify.ShowNotification(notify.location.Bottom, notify.Severity.Error, "Minimum radius is " + minRadius + " meters");
                return;
            }

            // check if time to activate is valid
            if ((selectedCircle?.data.settings?.timeToActivate ?? 0) < minTimeToActivate) {
                setSelectedCircle((prevState) => {
                    if (prevState) {
                        const updatedCircle: LocationData = {
                            ...prevState,
                            data: {
                                ...prevState.data,
                                settings: {
                                    ...prevState.data.settings,
                                    timeToActivate: minTimeToActivate
                                }
                            }
                        };
                        return updatedCircle;
                    }
                    return prevState;
                });
                notify.ShowNotification(notify.location.Bottom, notify.Severity.Error, "Minimum time to activate is " + minTimeToActivate / 1000 + " seconds");
                return;
            }
            const resp = await updateGamePoint(
                selectedCircle.data.game_id,
                selectedCircle.data.id, 
                selectedCircle.data.latitude,
                selectedCircle.data.longitude, 
                selectedCircle.data.radius,
                selectedCircle.data.owner,
                selectedCircle.data.color,
                selectedCircle.data.settings as unknown as string);
                if (resp === null) {
                    notify.ShowNotification(notify.location.Top, notify.Severity.Error, "Somthing went wrong", "Error");
            } else if (resp.status === 200) {
                if (resp.data === null) {
                    notify.ShowNotification(notify.location.Bottom, notify.Severity.Error, "Somthing went wrong");
                } else {
                    notify.ShowNotification(notify.location.Bottom, notify.Severity.Success, "Point update succesfuly");
                }
            }else {
                notify.ShowNotification(notify.location.Top, notify.Severity.Error, resp['message']['error'], "Error: " + resp.status + " " + resp.text);
                return
            }     
        }
    }
    
    const handleSaveSettings = (event: SelectChangeEvent) => {
        if (!selectedCircle) return;

        const updatedCircle = {
            ...selectedCircle,
            data: {
                ...selectedCircle.data,
                settings: {
                    ...selectedCircle.data.settings,
                    [event.target.name]: (event.target as HTMLInputElement).checked,
                }
            }
        };
        console.log(updatedCircle);
        setSelectedCircle(updatedCircle);
    };

    const editItemsDivider = (<Divider sx={{my: 1.5, mx: 0}}/>);

    const editSelected_color = (
        <Box sx={{display: 'flex', flexDirection: 'row', flexWrap: 'wrap', justifyContent: 'flex-start', alignItems: 'center', mb: 1, mx: 1}}>
            <Typography variant='body1' sx={{p: 2, color: selectedCircle ? pageTheme.palette.text.primary : pageTheme.palette.text.disabled}}>Edit color:</Typography>
            <Box sx={{width: 200, display: 'flex', alignItems: 'center'}}>
                <FormControl fullWidth size='small'>
                    <InputLabel id="color-edit-label" sx={{p:0, m: 0}}>Choose color</InputLabel>
                    <Select
                        labelId="color-edit-label"
                        id="color-edit"
                        value={selectedCircle ? colorArray.findIndex(color => color.color === selectedCircle?.data.color).toString() : ''}
                        label="Edit color"
                        disabled={selectedCircle === null}
                        onChange={(event: SelectChangeEvent) => {
                            const selectedColor = colorArray[parseInt(event.target.value as string)].color;
                            setSelectedCircle(prevState => {
                                if (prevState) {
                                    return {
                                        ...prevState,
                                        data: {
                                            ...prevState.data,
                                            color: selectedColor,
                                            fillColor: selectedColor,
                                            fillOpacity: selectedCircle?.data.settings?.disabled ? circleFill.notActive : circleFill.default
                                        },
                                        leaflet: prevState.leaflet
                                    };
                                }
                                return prevState;
                            });
                            selectedCircle?.leaflet.setStyle({
                                color: selectedColor
                            });
                        }}                                
                        >
                        {colorArray.map((color: { color: string; name: string; }, index: number) => {
                            const contrast = getContrastRatio(color.color, "#fff");
                            const textColor = contrast >= 4.5 ? "#fff" : "#000";
                            return (
                                <MenuItem key={index} value={index} sx={{backgroundColor: color.color, color: textColor}}>{color.name}</MenuItem>
                                );
                        })}
                    </Select>
                </FormControl>
                <Box
                    sx={{
                        width: '1.5rem',
                        height: '1.5rem',
                        minWidth: '1.5rem',
                        minHeight: '1.5rem',
                        borderRadius: '50%',
                        backgroundColor: selectedCircle ? selectedCircle.data.color : 'transparent',
                        border: '1px solid black',
                        display: 'inline-block',
                        ml: 1,
                    }}
                />
            </Box>
        </Box>
    );


    const editSelected_visible = (
        <Box sx={{display: 'flex', flexDirection: 'row', flexWrap: 'wrap', justifyContent: 'flex-start', alignItems: 'center', mx: 1}}>
            <Typography variant='body1' sx={{ml: 2, color: selectedCircle ? pageTheme.palette.text.primary : pageTheme.palette.text.disabled}}>Is visible:</Typography>
            <Checkbox
                color="primary"
                inputProps={{ 'aria-label': 'secondary checkbox' }}
                name="visible"
                checked={selectedCircle?.data.settings?.visible || false}
                onChange={handleSaveSettings}
                disabled={selectedCircle === null}
            />
        </Box>
    ); 
    
    const editSelected_disabled = (
        <Box sx={{display: 'flex', flexDirection: 'row', flexWrap: 'wrap', justifyContent: 'flex-start', alignItems: 'center', mx: 1}}>
            <Typography variant='body1' sx={{ml: 2, color: selectedCircle ? pageTheme.palette.text.primary : pageTheme.palette.text.disabled}}>Disabled:</Typography>
            <Checkbox
                color="primary"
                inputProps={{ 'aria-label': 'secondary checkbox' }}
                name="disabled"
                checked={selectedCircle?.data.settings?.disabled || false}
                onChange={handleSaveSettings}
                disabled={selectedCircle === null}
            />
        </Box>
    );

    const editSelected_radius = (
        <Box sx={{display: 'flex', flexDirection: 'row', flexWrap: 'wrap', justifyContent: 'flex-start', alignItems: 'center', mb: 1, mx: 1}}>
            <Typography variant='body1' sx={{p: 2, color: selectedCircle ? pageTheme.palette.text.primary : pageTheme.palette.text.disabled}}>Edit radius:</Typography>
            <Box sx={{width: 200}}>
                <NumericFormat
                    value={selectedCircle?.data.radius || ''}
                    onValueChange={(values: { floatValue: number | undefined }) => {
                        if (selectedCircle) {
                            const updatedCircle = {
                                ...selectedCircle,
                                data: {
                                    ...selectedCircle.data,
                                    radius: values.floatValue || 0,
                                }
                            };
                            setSelectedCircle(updatedCircle);
                        }
                    }}
                    customInput={TextField}
                    min={minRadius}
                    thousandSeparator
                    valueIsNumericString
                    suffix=" m"
                    variant="outlined"
                    size="small"
                    label="Value in meters"
                    disabled={selectedCircle === null}
                    error={(selectedCircle?.data.radius ?? minRadius) < minRadius}
                />
            </Box>
        </Box>
    );
    
    const editSelected_timeToActivate = (
        <Box sx={{display: 'flex', flexDirection: 'row', flexWrap: 'wrap', justifyContent: 'flex-start', alignItems: 'center', mb: 1, mx: 1}}>
            <Typography variant='body1' sx={{p: 2, color: selectedCircle ? pageTheme.palette.text.primary : pageTheme.palette.text.disabled}}>Seconds to activate:</Typography>
            <Stack direction="row" spacing={2}>
                <FormControl variant="standard">
                    <NumericFormat
                        value={(selectedCircle?.data.settings?.timeToActivate || 0) / 1000 || ''}
                        onValueChange={(values: { floatValue: number | undefined }) => {
                            if (selectedCircle) {
                                const updatedCircle = {
                                    ...selectedCircle,
                                    data: {
                                        ...selectedCircle.data,
                                        settings: {
                                            ...selectedCircle.data.settings,
                                            timeToActivate: values.floatValue ? values.floatValue * 1000 : 0,
                                        }
                                    }
                                };
                                setSelectedCircle(updatedCircle);
                            }
                        }}
                        customInput={TextField}
                        min={minTimeToActivate / 1000}
                        thousandSeparator
                        valueIsNumericString
                        suffix="s"
                        variant="outlined"
                        size="small"
                        label="Seconds to activate"
                        disabled={selectedCircle === null}
                        error={(selectedCircle?.data.settings?.timeToActivate || minTimeToActivate) < minTimeToActivate}
                    />
                </FormControl>
            </Stack>
        </Box>
    );

    const editSelected_question = (
        <Box sx={{display: 'flex', flexDirection: 'row', flexWrap: 'wrap', justifyContent: 'flex-start', alignItems: 'center', mb: 1, mx: 1}}>
            <Typography variant='body1' sx={{pl: 2, color: selectedCircle ? pageTheme.palette.text.primary : pageTheme.palette.text.disabled}}>Enable question:</Typography>
            <Checkbox
                color="primary"
                inputProps={{ 'aria-label': 'secondary checkbox' }}
                name="enableQuestion"
                checked={selectedCircle?.data.settings?.enableQuestion || false}
                onChange={
                    (event: React.ChangeEvent<HTMLInputElement>) => {
                        if (selectedCircle) {
                            const updatedCircle = {
                                ...selectedCircle,
                                data: {
                                    ...selectedCircle.data,
                                    settings: {
                                        ...selectedCircle.data.settings,
                                        enableQuestion: event.target.checked,
                                    }
                                }
                            };
                            setSelectedCircle(updatedCircle);
                        }
                    }
                }
                disabled={selectedCircle === null}
            />
            <Button 
                variant='contained' 
                color='primary' 
                onClick={() => {
                    setAnswers(selectedCircle?.data.settings?.answers || ['']);
                    setOpenAlert_question(true);
                }}
                disabled={selectedCircle === null || !selectedCircle.data.settings?.enableQuestion}
            >
                Edit question
            </Button>
        </Box>
    );

    const menuOptions = [
        editSelected_visible,
        editSelected_disabled,
        editSelected_timeToActivate,
        editSelected_color,
        editSelected_radius,
        editSelected_question,
    ]

    const map = (
        <div style={{ 
            minHeight: '400px', 
            height: '60vh', 
            visibility: loadingMapData ? 'hidden' : 'visible',
        }}>
            {windowsIsLoaded && <MapContainer 
                center={points.length > 0 ? [points[0].latitude, points[0].longitude] : defaultLocation}
                zoom={defaultZoom} 
                style={{ height: '100%', width: '100%' }}
                ref={mapRef}
            >
                <TileLayer
                url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
                />
                <HandleEvent/>
                    <Marker 
                        draggable 
                        eventHandlers={markerEventHandler} 
                        ref={markerRef} 
                        icon={customMarker} 
                        position={markerPos === null ? [0, 0] : [markerPos[0], markerPos[1]]}
                        opacity={markerPos === null ? 0 : 1}
                        
                    >
                        <Popup minWidth={90} offset={[0, -30]}>
                            <Box>
                                <Typography variant="h6"><strong>Add a new location</strong></Typography>
                            </Box>

                            <Box sx={{display: 'flex', flexDirection: 'column', flexWrap: 'wrap', alignContent: 'center', alignItems: 'center'}}>
                                <div>
                                    <Typography variant="caption">Radius in meters:</Typography>
                                    <NumericInput 
                                        className="form-control" 
                                        min={minRadius} 
                                        maxLength={8} 
                                        step={10}
                                        mobile
                                        required
                                        inputMode="numeric"
                                        size={7}
                                        value={Number(radius)} // Convert radius to a number
                                        onChange={(value) => setRadius(value ?? 0)} // Set radius to the value, or 0 if value is null
                                        disabled={!enableAdd}
                                        onKeyDown={(event) => {
                                            if (event.key === 'Enter') {
                                                addNewCircle();
                                            }
                                        }}
                                    />
                                </div>
                                <Button disableElevation={!enableAdd} variant={enableAdd ? 'contained' : 'outlined'} color='success' sx={{mt:2}} onClick={addNewCircle}>Add</Button>
                            </Box>
                        </Popup>
                    </Marker>
            </MapContainer>}
        </div>
    );
    
    const mapPaper = (
        <Paper square={false} sx={{marginBottom: 2, overflow: 'hidden'}}>
            {/* Loading box */}
            {loadingMapData === true && <Box 
                sx={{
                    display: 'flex', 
                    justifyContent: 'center', 
                    alignItems: 'center', 
                    height: '60vh',
                    width: '100%',
                    position: 'absolute',
                    flexDirection: 'column',
                }}>
                    <Typography variant='h5'>Loading map...</Typography>
                    <LinearProgress sx={{width: '300px', maxWidth: '80vw'}}/>
                </Box> 
            }
            {/* Map */}
            {map}
            
            
            {/* Buttons */}
            <Box sx={{m: 2, display: 'flex', justifyContent: 'space-between'}}>
                <Button variant='outlined' color='error' onClick={() => setOpenAlert_resetAll(true)} sx={{mr: 1}}>Reset all locations</Button>
                
                <LoadingButton
                    sx={{ml: 1}}
                    loadingIndicator={
                        <>
                            <Box sx={{display: 'flex', flexDirection: 'row'}}>
                                <CircularProgress color="inherit" size={16} sx={{mr: 1}}/>
                                <Typography variant='caption'>Fetching location...</Typography>
                            </Box> 
                        </>
                    }
                    endIcon={<MyLocation />}
                    variant="contained"
                    onClick={() => goToLocation(currentLocation.lat, currentLocation.lng)} 
                    loading={currentLocation == null}
                >
                   Go to my location
                </LoadingButton>
            </Box>
        </Paper>
    );

    const locationSettings = (
        <Paper square={false} sx={{padding: 2, marginBottom: 2}}>
            <Box>
                <Typography variant="h5" sx={{ fontWeight: 'bold', color: selectedCircle ? pageTheme.palette.text.primary : pageTheme.palette.text.disabled }}>Location settings</Typography>
            </Box>

            <hr style={{backgroundColor: pageTheme.palette.divider, color: pageTheme.palette.divider, height: '2px', borderWidth: 0, marginBottom: '15px'}} />
            <Box sx={{display: 'flex', justifyContent: 'space-evenly', alignItems: 'center', px: 4, flexWrap: 'wrap'}}>
                {menuOptions.map((option, index) => (
                    <Box key={index} sx={{py: 0}}>
                        {option}
                        {editItemsDivider}
                    </Box>
                ))}
            </Box>	
                <Box sx={{display: 'flex', flexDirection: 'row', flexWrap: 'wrap', justifyContent: 'space-around', my: 2}}>
                    <Typography variant='caption' sx={{mx: 1, color: selectedCircle ? pageTheme.palette.text.primary : pageTheme.palette.text.disabled}}>Circle selected: {selectedCircle?.data.index}</Typography>
                    <Typography variant='caption' sx={{mx: 1, color: selectedCircle ? pageTheme.palette.text.primary : pageTheme.palette.text.disabled}}>Owner: {selectedCircle?.data.owner}</Typography>
                    <Typography variant='caption' sx={{mx: 1, color: selectedCircle ? pageTheme.palette.text.primary : pageTheme.palette.text.disabled}}>Latitude: {selectedCircle?.data.latitude}</Typography>
                    <Typography variant='caption' sx={{mx: 1, color: selectedCircle ? pageTheme.palette.text.primary : pageTheme.palette.text.disabled}}>Longitude: {selectedCircle?.data.longitude}</Typography>
                </Box>

            <hr style={{backgroundColor: pageTheme.palette.divider, color: pageTheme.palette.divider, height: '2px', borderWidth: 0, marginBottom: '15px'}} />

            <Box sx={{display: 'flex', justifyContent: 'space-around'}}>
                {/* Remove point button */}
                <Button 
                    variant='outlined' 
                    disabled={selectedCircle === null} 
                    color='error' 
                    onClick={() => setOpenAlert_remove(true)}
                >
                    Remove point
                </Button>
                {/* Show reset button if selectedCircle is different to selectedCircle_backup */}
                <Button 
                    variant='outlined' 
                    disabled={selectedCircle === null || selectedCircle === selectedCircle_backup} 
                    color='warning' 
                    onClick={() => {
                    setSelectedCircle(selectedCircle_backup);
                    selectedCircle?.leaflet.setLatLng([selectedCircle_backup?.data.latitude || selectedCircle.data.latitude, selectedCircle_backup?.data.longitude || selectedCircle.data.longitude]);
                    }}
                    sx={{mx: 1}}
                >
                    Reset
                </Button>
                {/* Update point button */}
                <Button 
                    variant={selectedCircle !== null ? 'contained' : 'outlined'} 
                    disabled={selectedCircle === null || selectedCircle === selectedCircle_backup}
                    color='info' 
                    onClick={updatePoint}
                >
                    Update point
                </Button>
            </Box>
        </Paper>
    );
    
    return(
        <>
            {AlertDialogSlide_remove()}
            {AlertDialogSlide_resetAll()}
            {AlertDialogSlide_question()}
            {/* Map container */}
            {mapPaper}
            {/* Point settings */}
            {points !== null ? locationSettings : <LinearProgress sx={{width: '100%', height: '5px'}}/>}
        </>
    )
}