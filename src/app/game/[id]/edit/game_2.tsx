import React from 'react';
import Typography from '@mui/material/Typography';
import Card from '@mui/material/Card';

export default function Game_2(game: any) {
    console.log(game.game)
    return(
        <>
            <strong>Game_2</strong>
            <p>Game name: {game.game.name}</p>
            
            <Card sx={{ my: 4, p: 2 }}>
                <Typography variant="h3">
                    Why did you create this game?
                </Typography>
                <Typography variant="h5">
                    There is nothing here yet.
                </Typography>
            </Card>
        </>
    )
}