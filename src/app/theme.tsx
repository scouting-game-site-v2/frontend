"use client"
import { createTheme, ThemeOptions } from '@mui/material/styles';
import type {} from '@mui/x-data-grid/themeAugmentation';

export const themeOptions: ThemeOptions = {
  palette: {
    mode: 'dark',
    primary: {
      main: '#28a746',
    },
    secondary: {
      main: '#38acd5',
    },
  },
  components: {
    MuiDataGrid: {
        styleOverrides: {
            root: {
                '& .MuiDataGrid-cell:focus': {
                    outline: 'none'
                },
                '& .MuiDataGrid-cell:focus-within': {
                    outline: 'none'
                }
            },
        }
    },
}
};

export const pageTheme = createTheme(themeOptions);
