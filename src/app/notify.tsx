"use client"
import React from 'react';
import { ThemeProvider } from '@mui/material/styles';
import { Button, Container, Slide } from '@mui/material';
import Snackbar from '@mui/material/Snackbar';
import MuiAlert, { AlertProps } from '@mui/material/Alert';
import Alert from '@mui/material/Alert';
import Box from '@mui/material/Box';
import Collapse from '@mui/material/Collapse';
import IconButton from '@mui/material/IconButton';
import CloseIcon from '@mui/icons-material/Close';
import AlertTitle from '@mui/material/AlertTitle';

export enum location {
    Top = "top",
    Bottom = "bottom"
}

export enum Severity {
    Success = "Success",
    Info = "Info",
    Warning = "Warning",
    Error = "Error"
}

let openTop = false;
let setOpenTop: React.Dispatch<React.SetStateAction<boolean>>;
let openBottom = false;
let setOpenBottom: React.Dispatch<React.SetStateAction<boolean>>;
var severityData = Severity.Error;
var messageData = "No message data";
var titleData = "No data";
let durationData = 6000;
let positionData = location.Top;

export async function CloseNotification() {
    if (positionData == location.Top) {
        while (openTop) {
            await new Promise(r => setTimeout(r, 100));
            setOpenTop(false);
        }
    }
    else if (positionData == location.Bottom) {
        while (openTop) {
            await new Promise(r => setTimeout(r, 100));
            setOpenBottom(false);
        }
    }
}

export async function ShowNotification(positon: location ,severity: Severity, message: string, title: string = "", duration: number = 6000) {
    if (typeof window === 'undefined') return;
    
    if (positionData == location.Top) {
        while (openTop) {
            await new Promise(r => setTimeout(r, 100));
            setOpenTop(false);
        }
    }
    else if (positionData == location.Bottom) {
        while (openBottom) {
            await new Promise(r => setTimeout(r, 100));
            setOpenBottom(false);
        }
    }

    console.log("showing notification");


    positionData = positon;
    messageData = message;
    severityData = severity;
    titleData = title;
    durationData = duration;

    if (positionData == location.Top) {
        while (!openTop) {
            await new Promise(r => setTimeout(r, 100));
            setOpenTop(true);
        }
    }
    else if (positionData == location.Bottom) {
        while (!openBottom) {
            await new Promise(r => setTimeout(r, 100));
            setOpenBottom(true);
        }
    }
}

const AlertMesage = React.forwardRef<HTMLDivElement, AlertProps>(function Alert(
    props,
    ref,
) {
    return <MuiAlert elevation={6} ref={ref} variant="filled" {...props} />;
});


export function BottomNotification() {
    [openBottom, setOpenBottom] = React.useState(false);



    const handleClose = (event?: React.SyntheticEvent | Event, reason?: string) => {
        if (reason === 'clickaway') {
            return;
        }

        setOpenBottom(false);
    };
    return (
        <Snackbar open={openBottom} autoHideDuration={durationData != 0 ? durationData : 1000000}  onClose={handleClose} >
            <AlertMesage onClose={handleClose} severity={severityData.toLowerCase() as any} sx={{ width: '100%' }}>
                {messageData}
            </AlertMesage>
        </Snackbar>
    )
}



export function TopNotification() {
    [openTop, setOpenTop] = React.useState(false);
    const stickyRef = React.useRef(null);
    const [isSticky, setIsSticky] = React.useState(false);
    const [stickyOffset, setStickyOffset] = React.useState(0);
    
    React.useEffect(() => {
        const handleScroll = () => {
            if (!stickyRef.current) return;

            const shouldStick = window.scrollY >= stickyOffset;
            setIsSticky(shouldStick);
        };

        window.addEventListener('scroll', handleScroll);
        return () => window.removeEventListener('scroll', handleScroll);
    }, [setIsSticky, stickyOffset, stickyRef]);


    return (
        <Box sx={{ position: isSticky ? 'sticky' : 'relative', top: 1, zIndex: 1001, display: 'flex'}} ref={stickyRef}>
            <Container maxWidth="sm" sx={{mt: 2}}>
                <Collapse in={openTop}>
                <Alert 
                    severity={severityData.toLowerCase() as any}
                    variant="filled"
                    action={
                        <IconButton
                        aria-label="close"
                        color="inherit"
                        size="small"
                        onClick={() => {
                            setOpenTop(false);
                        }}
                        >
                        <CloseIcon fontSize="inherit" />
                    </IconButton>
                    }
                    sx={{ mb: 2 }}
                    >
                    <AlertTitle>{titleData}</AlertTitle>
                    {messageData}
                </Alert>
                </Collapse>
            </Container>
        </Box>
    );
  }
