import React from 'react';
import { ThemeProvider } from '@mui/material/styles';
import { pageTheme } from "@/app/theme";
import NavBar from '@/app/navbar';
import CssBaseline from '@mui/material/CssBaseline';
import { Container, Divider, LinearProgress, Paper, Typography } from '@mui/material';

export default async function LoadingGameWindow() {
    return (
        <ThemeProvider theme={pageTheme}>
            <CssBaseline />
            <NavBar />
            <Container maxWidth="sm">
                <Paper square={false}  sx={{p: 2}}>
                    <Typography variant='h5' sx={{padding: 1, display: 'flex', justifyContent: 'center'}}>
                        Loading the game...
                    </Typography>
                    <Divider sx={{width: '100%', m: 2}}/>
                    <Typography variant='body1'sx={{padding: 1, display: 'flex', justifyContent: 'center'}}>
                        The current game is loading in the background, please wait.
                    </Typography>
                    <LinearProgress/>
                </Paper>
            </Container>
        </ThemeProvider>
    )
}