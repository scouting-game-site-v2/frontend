import React from 'react';
import { ThemeProvider } from '@mui/material/styles';
import { pageTheme } from "@/app/theme";
import NavBar from '@/app/navbar';
import CssBaseline from '@mui/material/CssBaseline';
import Backdrop from '@mui/material/Backdrop';
import CircularProgress from '@mui/material/CircularProgress';

export default async function App() {
    return (
        <ThemeProvider theme={pageTheme}>
            <CssBaseline />
            <NavBar />
            <Backdrop
                sx={{ color: '#fff', zIndex: 'pageTheme.zIndex.drawer + 1' }}
                open={true}
            >
                <CircularProgress color="inherit" />
            </Backdrop>
        </ThemeProvider>
    )
}