"use client"
import React, { Suspense } from 'react';
import { ThemeProvider } from '@mui/material/styles';
import { Button, Slide } from '@mui/material';
import Container from '@mui/material/Container';
import AlertTitle from '@mui/material/AlertTitle';
import CssBaseline from '@mui/material/CssBaseline';
import { pageTheme } from '@/app/theme';
import { Box } from '@mui/system';
import Collapse from '@mui/material/Collapse';
import CloseIcon from '@mui/icons-material/Close';
import IconButton from '@mui/material/IconButton';
import Snackbar from '@mui/material/Snackbar';
import MuiAlert, { AlertProps } from '@mui/material/Alert';
import Alert from '@mui/material/Alert';
import Stack from '@mui/material/Stack';
import NavBar from '@/app/navbar';
import Link from 'next/link';
import { useRouter, useSearchParams } from 'next/navigation';

const AlertMesage = React.forwardRef<HTMLDivElement, AlertProps>(function Alert(
  props,
  ref,
) {
  return <MuiAlert elevation={6} ref={ref} variant="filled" {...props} />;
});

function CustomizedSnackbars() {
  const [open, setOpen] = React.useState(false);

  const handleClick = () => {
    setOpen(true);
  };

  const handleClose = (event?: React.SyntheticEvent | Event, reason?: string) => {
    if (reason === 'clickaway') {
      return;
    }

    setOpen(false);
  };
  return (
    <div>
      <Button variant="outlined" onClick={handleClick}>
      Open success snackbar
      </Button>
      <Snackbar open={open} autoHideDuration={6000} onClose={handleClose} >
      <AlertMesage onClose={handleClose} severity="success" sx={{ width: '100%' }}>
      This is a success message!
      </AlertMesage>
      </Snackbar>
      </div>
  )
}


function TransitionAlerts() {
    const [open, setOpen] = React.useState(true);
  
    return (
      <Box sx={{ width: '80%' }}>
        <Collapse in={open}>
          <Alert 
            severity="info" 
            variant="filled"
            action={
              <IconButton
                aria-label="close"
                color="inherit"
                size="small"
                onClick={() => {
                  setOpen(false);
                }}
              >
                <CloseIcon fontSize="inherit" />
              </IconButton>
            }
            sx={{ mb: 2 }}
            >
            <AlertTitle>Success</AlertTitle>
            Close me!
            This is a success alert — <strong>check it out!</strong>
          </Alert>
        </Collapse>
        <Button
          disabled={open}
          variant="outlined"
          onClick={() => {
            setOpen(true);
          }}
        >
          Re-open
        </Button>
      </Box>
    );
  }

export default function Home() {
  const searchParams = useSearchParams();
  const router = useRouter();
  const param = new URLSearchParams(useSearchParams());
  param.set("test", "1234");
  console.log(searchParams.get("test"));
  console.log(searchParams.get("test4567"));
    return (
        <>   
          <NavBar/>
            <ThemeProvider theme={pageTheme}>
                <Box
                sx={{
                    marginTop: 2,
                    display: 'flex',
                    flexDirection: 'column',
                    alignItems: 'center'
                }}
                >
                    <TransitionAlerts/>

                <Alert severity="error" onClose={() =>{console.log('w')}}>
                <AlertTitle>Error</AlertTitle>
                This is an error alert — <strong>check it out!</strong>
                </Alert>
                <Alert variant="outlined" severity="warning">
                <AlertTitle>Warning</AlertTitle>
                This is a warning alert — <strong>check it out!</strong>
                </Alert>
                <Alert severity="info">
                <AlertTitle>Info</AlertTitle>
                This is an info alert — <strong>check it out!</strong>
                </Alert>
                <Alert severity="success">
                <AlertTitle>Success</AlertTitle>
                This is a success alert — <strong>check it out!</strong>
                </Alert>
                
                <Link href={{
                    pathname:'/test',
                    query:{
                      test: "123",
                      test2: "456"
                    }
                }}
                >test ?par</Link>
                <Button onClick={() => router.replace(`/?${param}`)}>
                  Router button
                </Button>

                <CssBaseline>
                    <Container component="main" maxWidth="xs">
                        <Button variant="contained">Hello World</Button>
                        <Stack spacing={2} sx={{ width: '100%' }}>
                            <Button variant="text">Text</Button>
                            <Button variant="contained">Contained</Button>
                            <Button variant="outlined">Outlined</Button>
                        </Stack>
                    </Container>
                </CssBaseline>

                <CustomizedSnackbars/>

                </Box>
            </ThemeProvider>
        </>
        );
    }


 