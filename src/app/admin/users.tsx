"use client"
import React, { use } from 'react';
import Card from '@mui/material/Card';
import Typography from '@mui/material/Typography';
import Paper from '@mui/material/Paper';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TablePagination from '@mui/material/TablePagination';
import TableRow from '@mui/material/TableRow';
import styled from '@mui/material/styles/styled';
import TableCell, { tableCellClasses } from '@mui/material/TableCell';
import { pageTheme } from '@/app/theme';
import Button from '@mui/material/Button';
import Box from '@mui/material/Box';
import * as Icon from '@mui/icons-material';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import Slide from '@mui/material/Slide';
import { TransitionProps } from '@mui/material/transitions';
import { getMembers } from '@/tools/api/memberAPI';
import { PageLoader } from '@/app/loader';
import * as notify from '@/app/notify';
import CssBaseline from '@mui/material/CssBaseline';
import { ThemeProvider } from '@mui/material/styles';
import { useNow, useFormatter, useTimeZone } from 'next-intl';
import { CircularProgress, LinearProgress } from '@mui/material';
import { signal } from '@preact/signals';
import dayjs from 'dayjs';
import { DataGrid, GridColDef } from '@mui/x-data-grid';


const Transition = React.forwardRef(function Transition(
    props: TransitionProps & {
      children: React.ReactElement<any, any>;
    },
    ref: React.Ref<unknown>,
  ) {
    return <Slide direction="up" ref={ref} {...props} />;
  });

  
const StyledGridOverlay = styled('div')(({ theme }) => ({
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    height: '100%',
    '& .ant-empty-img-1': {
        fill: theme.palette.mode === 'light' ? '#aeb8c2' : '#262626',
    },
    '& .ant-empty-img-2': {
        fill: theme.palette.mode === 'light' ? '#f5f5f7' : '#595959',
    },
    '& .ant-empty-img-3': {
        fill: theme.palette.mode === 'light' ? '#dce0e6' : '#434343',
    },
    '& .ant-empty-img-4': {
        fill: theme.palette.mode === 'light' ? '#fff' : '#1c1c1c',
    },
    '& .ant-empty-img-5': {
        fillOpacity: theme.palette.mode === 'light' ? '0.8' : '0.08',
        fill: theme.palette.mode === 'light' ? '#f5f5f5' : '#fff',
    },
}));


  function CustomNoRowsOverlay() {
    return (
        <StyledGridOverlay>
            <svg
                style={{ flexShrink: 0 }}
                width="240"
                height="200"
                viewBox="0 0 184 152"
                aria-hidden
                focusable="false"
            >
                <g fill="none" fillRule="evenodd">
                    <g transform="translate(24 31.67)">
                        <ellipse
                            className="ant-empty-img-5"
                            cx="67.797"
                            cy="106.89"
                            rx="67.797"
                            ry="12.668"
                        />
                        <path
                            className="ant-empty-img-1"
                            d="M122.034 69.674L98.109 40.229c-1.148-1.386-2.826-2.225-4.593-2.225h-51.44c-1.766 0-3.444.839-4.592 2.225L13.56 69.674v15.383h108.475V69.674z"
                        />
                        <path
                            className="ant-empty-img-2"
                            d="M33.83 0h67.933a4 4 0 0 1 4 4v93.344a4 4 0 0 1-4 4H33.83a4 4 0 0 1-4-4V4a4 4 0 0 1 4-4z"
                        />
                        <path
                            className="ant-empty-img-3"
                            d="M42.678 9.953h50.237a2 2 0 0 1 2 2V36.91a2 2 0 0 1-2 2H42.678a2 2 0 0 1-2-2V11.953a2 2 0 0 1 2-2zM42.94 49.767h49.713a2.262 2.262 0 1 1 0 4.524H42.94a2.262 2.262 0 0 1 0-4.524zM42.94 61.53h49.713a2.262 2.262 0 1 1 0 4.525H42.94a2.262 2.262 0 0 1 0-4.525zM121.813 105.032c-.775 3.071-3.497 5.36-6.735 5.36H20.515c-3.238 0-5.96-2.29-6.734-5.36a7.309 7.309 0 0 1-.222-1.79V69.675h26.318c2.907 0 5.25 2.448 5.25 5.42v.04c0 2.971 2.37 5.37 5.277 5.37h34.785c2.907 0 5.277-2.421 5.277-5.393V75.1c0-2.972 2.343-5.426 5.25-5.426h26.318v33.569c0 .617-.077 1.216-.221 1.789z"
                        />
                    </g>
                    <path
                        className="ant-empty-img-3"
                        d="M149.121 33.292l-6.83 2.65a1 1 0 0 1-1.317-1.23l1.937-6.207c-2.589-2.944-4.109-6.534-4.109-10.408C138.802 8.102 148.92 0 161.402 0 173.881 0 184 8.102 184 18.097c0 9.995-10.118 18.097-22.599 18.097-4.528 0-8.744-1.066-12.28-2.902z"
                    />
                    <g className="ant-empty-img-4" transform="translate(149.65 15.383)">
                        <ellipse cx="20.654" cy="3.167" rx="2.849" ry="2.815" />
                        <path d="M5.698 5.63H0L2.898.704zM9.259.704h4.985V5.63H9.259z" />
                    </g>
                </g>
            </svg>
            <Box sx={{ mt: 1 }}>No locations</Box>
        </StyledGridOverlay>
    );
}


const column: GridColDef[] = [
    {
        field: "edit",
        headerName: "Edit",
        headerAlign: 'center',
        align: 'center',
        filterable: false,
        disableColumnMenu: true,
        sortable: false,
        width: 90,
        renderCell: (params) => { 
            return (
                <Button 
                    variant="contained" 
                    color='info'
                    sx={{
                        width: '10px',
                        mx: 1,
                    }}
                    href={'/account/' + params.row.username + '?next_page=/admin'}
                    >
                    <Icon.Edit/>
                </Button>
            );
        }
    },
    { 
        field: 'id_num',     
        headerName: 'ID',            
        type: 'number',
        minWidth: 100,
        maxWidth: 200,
        flex: 1,
        align: 'center',
        headerAlign: 'center',
    },
    { 
        field: 'owned_games',
        headerName: 'Game count',   
        type: 'number',
        minWidth: 120,
        maxWidth: 200,
        flex: 1,
        align: 'center',
        headerAlign: 'center',
    },
    { 
        field: 'username',   
        headerName: 'Username',      
        type: 'string',
        minWidth: 170,
        flex: 1,
        align: 'center',
        headerAlign: 'center',
    },
    { 
        field: 'fullname',  
        headerName: 'Full name',     
        type: 'string',
        minWidth: 100,
        flex: 1,
        align: 'center',
        headerAlign: 'center',
    },
    { 
        field: 'admin',      
        headerName: 'Admin',         
        type: 'boolean',
        minWidth: 100,
        maxWidth: 200,
        flex: 1,
        align: 'center',
        headerAlign: 'center',
        renderCell: (params) => {
            return (
                <div className={params.value ? "admin-cell" : "non-admin-cell"}>
                    {params.value ? <span style={{ color: "orange" }}><Icon.CheckCircle/></span> : <span style={{ color: "green" }}><Icon.Cancel/></span>}
                </div>
            );
        },
    },
    { 
        field: 'create_at',  
        headerName: 'Create at',     
        type: 'dateTime',
        headerAlign: 'left',
        width: 200,
        align: 'left',
    },
    {
        field: 'time_until',
        headerName: '',
        width: 150,
        align: 'center',
    },
];

interface TabelData {
    id: number,
    id_num: number,
    username: string,
    fullname: string,
    admin: boolean,
    create_at: Date,
    time_until: string,
    owned_games: number,
}

function addTabelData(
    id: number,
    id_num: number,
    username: string,
    firstname: string,
    lastname: string,
    admin_bool: boolean,
    create_at: Date,
    time_until: string,
    owned_games: number,
): TabelData {
    const data: TabelData = {
        id: id,
        id_num: id_num,
        username: username,
        fullname: `${firstname} ${lastname}`,
        admin: admin_bool,
        create_at: create_at,
        time_until,
        owned_games: owned_games,
    };
    return data;    
}

var userCount: number = 0;
var gameCount: number = 0;

export async function getUserCount() {
    // wait til the usercount is loaded
    while (userCount === 0) {
        await new Promise(resolve => setTimeout(resolve, 100));
    } 
    return userCount;
}

export async function getGameCount() {
    // wait til the usercount is loaded
    while (gameCount === 0) {
        await new Promise(resolve => setTimeout(resolve, 100));
    } 
    return gameCount;
}

export default function UserList() {
    const [rows, setRows] = React.useState<TabelData[]>([]);
    const formatter = useFormatter();
    const now = useNow();
    const timeZone = useTimeZone();


    const [open, setOpen] = React.useState(false);
    const handleClose = () => {
        setOpen(false);
    }; 
    const [item, setItem] = React.useState("");

    const [page, setPage] = React.useState(0);
    const [rowsPerPage, setRowsPerPage] = React.useState(10);

    const handleChangePage = (event: unknown, newPage: number) => {
        setPage(newPage);
    };

    const handleChangeRowsPerPage = (event: React.ChangeEvent<HTMLInputElement>) => {
        setRowsPerPage(+event.target.value);
        setPage(0);
    };

    function handleConfirm(item: string) {
        setItem(item);
        setOpen(true);
    }


    function AlertDialogSlide() {
        return (
          <div>
            <Dialog
              open={open}
              TransitionComponent={Transition}
              keepMounted
              onClose={handleClose}
              aria-describedby="alert-dialog-slide-description"
            >
              <DialogTitle><strong>{"Remove"} {item}</strong></DialogTitle>
              <DialogContent>
                <DialogContentText id="alert-dialog-slide-description">
                  Do you want to remove <strong>{item}</strong>?
                </DialogContentText>
              </DialogContent>
              <DialogActions sx={{justifyContent: 'space-between', px:2}}>
                <Button variant='contained' color='success' onClick={handleClose}>Cancel</Button>
                <Button variant='outlined' color='error' onClick={() => {console.log("remove " + item)}}>Agree</Button>
              </DialogActions>
            </Dialog>
          </div>
        );
    }
    var setUserCount: React.Dispatch<React.SetStateAction<number>>;
    [userCount, setUserCount] = React.useState<number>(0);
    var setGameCount: React.Dispatch<React.SetStateAction<number>>;
    [gameCount, setGameCount] = React.useState<number>(0);

      // get the current user from the server
    const [users, setUsers] = React.useState<null | any>(null);
    const [loadingData, setLoadingData] = React.useState<boolean>(true);
    const localstorageActive = signal(false);
    React.useEffect(() => {
        async function fetchData() {
            const resp = await getMembers();

            if (resp === null) {
                notify.ShowNotification(notify.location.Top, notify.Severity.Error, "Somthing went wrong", "Error");
                return;
            }

            if (resp.status === 200) {
                if (resp.data === null) {
                    notify.ShowNotification(notify.location.Top, notify.Severity.Error, "Somthing went wrong", "Error");
                    return;
                } else {
                    setUsers(resp.data);
                    setLoadingData(false);
                    setUserCount(resp.data.length);
                    return;
                }
            } else {
                notify.ShowNotification(notify.location.Top, notify.Severity.Error, resp["message"]["error"], "Error: " + resp.status + " " + resp.text);
                return;
            }
        }
        fetchData();
    }, [localstorageActive.value, setUserCount]);

    
    if (typeof localStorage !== "undefined"){
        localstorageActive.value = true;
    }

    React.useEffect(() => {
        if (!loadingData) {
            let gameCount = 0;
            setRows([]);
            console.log(users)
            users.forEach((user: any) => {
                // format the date
                const timeDate = new Date(user.created_at);
                // const dateTime = timeDate.toLocaleString(navigator.language);
                const timeUntil = formatter.relativeTime(dayjs(timeDate.toLocaleString("en-US")).toDate(), now);
                gameCount += user.owned_games;
                setRows((rows) => [...rows, 
                    addTabelData(
                        rows.length,
                        user.id, 
                        user.username, 
                        user.firstname, 
                        user.lastname, 
                        user.admin, 
                        timeDate,
                        timeUntil, 
                        user.owned_games
                    )
                ]);
            });

            setGameCount(gameCount);
        }
    }, [loadingData, users, formatter, now, setGameCount]);

    if (!loadingData) {
        return (
            <>
                {AlertDialogSlide()}
                <Card sx={{ p: 1, m: 1}}>
                    <Typography variant="h5" fontWeight={'bold'}>Users</Typography>
                    <Paper sx={{ 
                        width: '100%', 
                        overflow: 'hidden', 
                        mt: 2 
                    }}>
                        <DataGrid
                            slots={{
                                noRowsOverlay: CustomNoRowsOverlay,
                                columnSortedDescendingIcon: () => <Icon.ExpandMore className="icon" />,
                                columnSortedAscendingIcon: () => <Icon.ExpandLess className="icon" />,
                                columnUnsortedIcon: () => <Icon.Sort className="icon" />,
                            }}
                            rows={rows}
                            columns={column}
                            initialState={{
                                pagination: {
                                    paginationModel: {
                                        pageSize: 10
                                    }
                                },
                                sorting: {
                                    sortModel: [{ field: 'id_num', sort: 'desc' }],
                                }
                            }}
                            sx={{ '--DataGrid-overlayHeight': '400px', height: rows.length == 0 ? '400px' : '' }}
                            pageSizeOptions={[10, 25, 100]}
                            hideFooterSelectedRowCount
                            // onRowSelectionModelChange={(ids: any) => onRowsSelectionHandler(ids)}
                            // rowSelectionModel={selected}
                            // TODO: go to te page where the row is selected
                        />
                    </Paper>
                </Card>
            </>
        );
    } 
    else {
        return (
            <ThemeProvider theme={pageTheme}>
                <CssBaseline />
                <p>Loading data in tabel</p>
                <LinearProgress />
            </ThemeProvider>
        )
    }
}