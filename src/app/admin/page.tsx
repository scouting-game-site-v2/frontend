"use client"
import React, { lazy, Suspense } from 'react';
import { ThemeProvider } from '@mui/material/styles';
import { pageTheme } from "@/app/theme";
import Container from "@mui/material/Container";
// import NavBar from '@/app/navbar';
import CssBaseline from '@mui/material/CssBaseline';
import Typography from '@mui/material/Typography';
import UserList, { getUserCount, getGameCount } from '@/app/admin/users';
import Card from '@mui/material/Card';
import Button from '@mui/material/Button';
import * as notify from '@/app/notify';
import { getMembers } from '@/tools/api/memberAPI';
import { PageLoader } from '@/app/loader';
import { Grid2 } from '@mui/material';
import { useSearchParams } from 'next/navigation'
import { CircularProgress } from '@mui/material';
import { QueryNotifications } from '@/tools/notification';
import { signal } from '@preact/signals';
import useWebSocket, { ReadyState } from 'react-use-websocket';
import { WebSocketMessage } from "@/tools/websocketTypes";

const LazyNavBar = lazy(() => import('@/app/navbar'));


export default function App() {
    // TODO: check if authenticated with admin

    const [firstname, setFirstname] = React.useState("");
    const [lastname, setLastname] = React.useState("");
    const [userCount, setUserCount] = React.useState<number>(0);
    const [activeUserCount, setActiveUserCount] = React.useState<number>(0);
    const [game, setGame] = React.useState<number>(0);
    React.useEffect(() => {
        getUserCount().then((count) => {
            setUserCount(count);
        });
        getGameCount().then((count) => {
            setGame(count);
        });
        setFirstname(localStorage.getItem("firstname") || "");
        setLastname(localStorage.getItem("lastname") || "");
    }, []);

    const [socketUrl, setSocketUrl] = React.useState<string | null>(null);

    React.useEffect(() => {
        if (typeof window !== 'undefined') {
            const protocol = window.location.protocol === 'https:' ? 'wss' : 'ws';
            setSocketUrl(protocol + '://' + window.location.host + '/ws');
        }
    }, []);

    const { sendMessage, lastMessage, readyState } = useWebSocket(socketUrl);
    const connectionStatus = {
        [ReadyState.CONNECTING]: 'Connecting',
        [ReadyState.OPEN]: 'Connected',
        [ReadyState.CLOSING]: 'Closing',
        [ReadyState.CLOSED]: 'Closed',
        [ReadyState.UNINSTANTIATED]: 'Uninstantiated',
    }[readyState];

    React.useEffect(() => {
        if (lastMessage !== null) {
            const message: WebSocketMessage = JSON.parse(lastMessage.data);
            websocketHandler(message);            
        }
    }, [lastMessage]);

    React.useEffect(() => {
        console.log("Current websocket state: ", connectionStatus);
        
        const retryInterval = setInterval(() => {
            if (readyState !== ReadyState.CONNECTING && readyState !== ReadyState.OPEN) {
                const protocol = window.location.protocol === 'https:' ? 'wss' : 'ws';
                setSocketUrl(protocol + '://' + window.location.host + '/ws');
                notify.ShowNotification(notify.location.Bottom, notify.Severity.Info, "Connecting websocket", "", 2000);
            }
        }, 10000);
        
        if (readyState === ReadyState.CLOSED) {
            notify.ShowNotification(notify.location.Bottom, notify.Severity.Warning, "Websocket connection is closed");
        }

        return () => clearInterval(retryInterval);
    }, [connectionStatus, readyState]);


    function websocketHandler(message: WebSocketMessage){
        console.log(message);
        
        switch (message.action) {
            case 'active_user_count':
                setActiveUserCount(message.data);
                break;

            default:
                // handle unknown message
                break;
        }
    }

    const sendGetActiveUserCount = React.useCallback(() => {
        if (readyState === ReadyState.OPEN) {
            const message: WebSocketMessage = {
                action: 'get_active_user_count',
                data: null
            };
            sendMessage(JSON.stringify(message));
        }
    }, [readyState, sendMessage]);
    
    const isLocalStorageDefined = typeof localStorage !== "undefined";
    
    React.useEffect(() => {
        const handleLeaveGame = () => {
            if (readyState === ReadyState.OPEN) {
                const message: WebSocketMessage = {
                    action: 'close_conn',
                    data: null
                };
                sendMessage(JSON.stringify(message));
            }
        };
        if (game !== null && isLocalStorageDefined) {
            sendGetActiveUserCount();

            // Add event listener for beforeunload event
            window.addEventListener('beforeunload', handleLeaveGame);
            
            // Remove event listener on cleanup
            return () => {
                window.removeEventListener('beforeunload', handleLeaveGame);
            };
        }
    }, [readyState, isLocalStorageDefined, game, sendMessage, sendGetActiveUserCount]);



    // send on the websocket, get the active user count in a interval - 10s
    React.useEffect(() => {
        const timer = setInterval(() => {
            sendGetActiveUserCount();
        }, 10000);

        return () => {
            clearInterval(timer);
        };
    }, [readyState, sendGetActiveUserCount]);

    return (
        <>
            <ThemeProvider theme={pageTheme}>
                <CssBaseline>
                    <Suspense fallback={<PageLoader />}>
                        <LazyNavBar />
                    </Suspense>
                    <Container maxWidth="xl">
                        <Card sx={{ p: 1, m: 1 }}>
                            <Typography variant="h4">Welcome <strong>{firstname} {lastname}</strong></Typography>
                            <Typography variant="h6">This is the powerfull <strong>Admin</strong> page!</Typography>
                            <Typography variant="caption">Here you can manage all user accounts</Typography>
                        </Card>
                        {/* sx={{ p: 1, m: 1, display: 'flex', justifyContent: 'space-evenly' }}> */}
                        <Grid2 container spacing={2} sx={{ px: 1, my: 1, display: 'flex', justifyContent: 'space-evenly' }}> 
                            <Grid2>
                                <Card sx={{ p: 3, m: 1, display: 'block', textAlign: 'center' }}>
                                    <Typography variant="h5">User count:</Typography>
                                    <Typography variant="h3">{userCount != 0 ? userCount : <CircularProgress/>}</Typography>
                                </Card>
                            </Grid2>
                            <Grid2>
                                <Card sx={{ p: 3, m: 1, display: 'block', textAlign: 'center' }}>
                                    <Typography variant="h5">Game count:</Typography>
                                    <Typography variant="h3">{userCount != 0 ? game : <CircularProgress/>}</Typography>
                                </Card>
                            </Grid2>
                            <Grid2>
                                <Card sx={{ p: 3, m: 1, display: 'block', textAlign: 'center' }}>
                                    <Typography variant="h5">Active users:</Typography>
                                    <Typography variant="h3">{readyState === ReadyState.OPEN ? activeUserCount : <CircularProgress/>}</Typography>
                                </Card>
                            </Grid2>
                        </Grid2>
                        <UserList />
                    </Container>
                </CssBaseline>
            </ThemeProvider>
        </>
    )
}
