import { Icon } from "leaflet";

export interface MarkerData {	
    position: [number, number];
    popup: string;
}

export interface LocationData { 
    data: GamePoint;
    leaflet: L.Circle;
}

export interface GamePoint {
    color: string;
    game_id: number;
    id: number;
    index: number;
    latitude: number;
    longitude: number;
    owner: string;
    radius: number;
    settings: GameLocationsSettings;
}

export interface GameLocationsSettings {
    visible: boolean;
    disabled: boolean;
    timeToActivate: number;
    enableQuestion?: boolean;
    question?: string;
    answers?: string[];
    correctAnswer?: number;
}

export const customMarker = new Icon({
    iconUrl: '/static/marker-icon.png',
    iconSize: [38, 38],
    iconAnchor: [19, 38],
    
});

export const circleFill = {
    default: 0.5,
    active: 0.7,
    notActive: 0.1
};

export const minRadius = 10;
export const minTimeToActivate = 1000;

export const defaultLocationSettings: GameLocationsSettings = {
    visible: true,
    disabled: false,
    timeToActivate: minTimeToActivate
};