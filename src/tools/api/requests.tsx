export async function post(path: string, jsonData: Object, jwt_token: string) {
    try {
        const res = await fetch(path, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'x-jwt-token' : jwt_token
            },
            body: JSON.stringify(jsonData)
        })
        return res
    } catch (err) {
        console.log(err)
    }
}

export async function postWithoutAuth(path: string, jsonData: Object) {
    try {
        const res = await fetch(path, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(jsonData)
        })
        return res
    } catch (err) {
        console.log(err)
    }
}

export async function get(path: string, jwt_token: string) {
    try {
        const res = await fetch(path, {
            method: 'GET',
            headers: {
                'x-jwt-token' : jwt_token
            }
        })
        return res
        } catch (err) {
            console.log(err)
        }
}

export async function getWithoutAuth(path: string) {
    try {
        const res = await fetch(path, {
            method: 'GET'
        })
        return res
        } catch (err) {
            console.log(err)
        }
}

export async function put(path: string, jsonData: Object, jwt_token: string) {
    try {    
        const res = await fetch(path, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                'x-jwt-token' : jwt_token
            },
            body: JSON.stringify(jsonData)
        })
        return res
    } catch (err) {
        console.log(err)
    }
}

export async function del(path: string, jwt_token: string) {
    try {
        const res = await fetch(path, {
            method: 'DELETE',
            headers: {
                'x-jwt-token' : jwt_token
            }
        })
        return res
    } catch (err) {
        console.log(err)
    }
}