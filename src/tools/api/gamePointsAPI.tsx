import * as requests from "@/tools/api/requests";	

export async function getGamePoints(gameID: number) {
    const resp = await requests.getWithoutAuth('/api/games/' + gameID + '/locations')
    if (resp == null || !(resp instanceof Response)) {
        return null
    }

    const data = await resp.json()
    if (resp.status === 200) {
        return {status: resp.status, data: data}
    }

    return { status: resp.status, text: resp.statusText, message: data }
}

export async function createGamePoint(gameID: number, latitude: number, longitude: number, radius: number, color: string, settings: string) {
    const resp = await requests.post('/api/games/' + gameID + '/locations', {latitude, longitude, radius, color, settings}, localStorage.getItem('jwt_token') as string)

    if (resp == null || !(resp instanceof Response)) {
        return null
    }

    const data = await resp.json()
    if (resp.status === 200) {
        return {status: resp.status, data: data}
    }

    return { status: resp.status, text: resp.statusText, message: data }
}


export async function updateGamePoint(gameID: number, id: number, latitude: number, longitude: number, radius: number, owner: string, color: string, settings: string) {
    const resp = await requests.put('/api/games/' + gameID + '/locations/' + id, {latitude, longitude, radius, owner, color, settings}, localStorage.getItem('jwt_token') as string)

    if (resp == null || !(resp instanceof Response)) {
        return null
    }

    const data = await resp.json()
    if (resp.status === 200) {
        return {status: resp.status, data: data}
    }

    return { status: resp.status, text: resp.statusText, message: data }
}


export async function deleteGamePoint(gameID: number, id: number) {
    const resp = await requests.del('/api/games/' + gameID + '/locations/' + id, localStorage.getItem('jwt_token') as string)

    if (resp == null || !(resp instanceof Response)) {
        return null
    }

    const data = await resp.json()
    if (resp.status === 200) {
        return {status: resp.status, data: data}
    }

    return { status: resp.status, text: resp.statusText, message: data }
}