import * as requests from "@/tools/api/requests";	

export async function login(username: string, password: string) {
    const resp = await requests.postWithoutAuth('/api/login', { username, password })

    if (resp == null || !(resp instanceof Response)) {
        return null
    }
    
    const data = await resp.json()
    if (resp.status === 200) {
        localStorage.setItem('jwt_token', data['token'] as string)
        localStorage.setItem('username', data['account']['username'] as string)
        localStorage.setItem('game_username', data['account']['username'] as string)
        localStorage.setItem('firstname', data['account']['firstname'] as string)
        localStorage.setItem('lastname', data['account']['lastname'] as string)
        localStorage.setItem('admin', data['account']['admin'])
        return true
    }

    return { status: resp.status, text: resp.statusText, message: data }
}

export async function signup(username: string, password: string, firstName: string, lastName: string) {
    const resp = await requests.postWithoutAuth('/api/accounts', { username, password, firstName, lastName })

    if (resp == null || !(resp instanceof Response)) {
        return null
    }

    const data = await resp.json()
    if (resp.status === 200) {
        localStorage.setItem('jwt_token', data['token'] as string)
        localStorage.setItem('username', data['account']['username'] as string)
        localStorage.setItem('game_username', data['account']['username'] as string)
        localStorage.setItem('firstname', data['account']['firstname'] as string)
        localStorage.setItem('lastname', data['account']['lastname'] as string)
        localStorage.setItem('admin', data['account']['admin'])
        return true
    }

    return { status: resp.status, text: resp.statusText, message: data }
}

export async function getMember(username: string) {
    const resp = await requests.get('/api/accounts/' + username, localStorage.getItem('jwt_token') as string)

    if (resp == null || !(resp instanceof Response)) {
        return null
    }

    const data = await resp.json()
    if (resp.status === 200) {
        return {status: resp.status, data: data}
    }

    return { status: resp.status, text: resp.statusText, message: data }
}

export async function getMembers() {
    if (localStorage.getItem('admin') === 'false'){
        return { status: '403: Forbidden', message: 'No admin previliges'};
    }

    const resp = await requests.get('/api/accounts', localStorage.getItem('jwt_token') as string)

    if (resp == null || !(resp instanceof Response)) {
        return null
    }

    const data = await resp.json()
    if (resp.status === 200) {
        return {status: resp.status, data: data}
    }

    return { status: resp.status, text: resp.statusText, message: data }
}

// put request to update the member
export async function updateMember(username: string, password: string, firstName: string, lastName: string, admin: boolean) {
    const resp = await requests.put('/api/accounts/' + username, { password, admin, firstName, lastName}, localStorage.getItem('jwt_token') as string)

    if (resp == null || !(resp instanceof Response)) {
        return null
    }

    const data = await resp.json()
    if (resp.status === 200) {
        return {status: resp.status, data: data}
    }
        
    return { status: resp.status, text: resp.statusText, message: data }
}

export async function deleteMember(username: string) {
    const resp = await requests.del('/api/accounts/' + username, localStorage.getItem('jwt_token') as string)

    if (resp == null || !(resp instanceof Response)) {
        return null
    }
    const data = await resp.json()
    return { status: resp.status, text: resp.statusText, message: data }
}