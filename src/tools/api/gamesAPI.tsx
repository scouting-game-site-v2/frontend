import * as requests from "@/tools/api/requests";	

export async function getGames() {
    const resp = await requests.get('/api/games', localStorage.getItem('jwt_token') as string)

    if (resp == null || !(resp instanceof Response)) {
        return null
    }

    const data = await resp.json()
    if (resp.status === 200) {
        return {status: resp.status, data: data}
    }

    return { status: resp.status, text: resp.statusText, message: data }
}

export async function getGameByID(id: number) {
    const resp = await requests.getWithoutAuth('/api/games/' + id)

    if (resp == null || !(resp instanceof Response)) {
        return null
    }

    const data = await resp.json()
    if (resp.status === 200) {
        return {status: resp.status, data: data}
    }

    return { status: resp.status, text: resp.statusText, message: data }
}

export async function createGame(name: string, game_type: number) {
    const resp = await requests.post('/api/games', {name, game_type}, localStorage.getItem('jwt_token') as string)

    if (resp == null || !(resp instanceof Response)) {
        return null
    }

    const data = await resp.json()
    if (resp.status === 200) {
        return {status: resp.status, data: data}
    }

    return { status: resp.status, text: resp.statusText, message: data }
}

export async function updateGame(id: number, name: string, settings: string) {
    const resp = await requests.put('/api/games/' + id, {name, settings},localStorage.getItem('jwt_token') as string)

    if (resp == null || !(resp instanceof Response)) {
        return null
    }

    const data = await resp.json()
    if (resp.status === 200) {
        return {status: resp.status, data: data}
    }

    return { status: resp.status, text: resp.statusText, message: data }
}

export async function deleteGame(id: number) {
    const resp = await requests.del('/api/games/' + id, localStorage.getItem('jwt_token') as string)

    if (resp == null || !(resp instanceof Response)) {
        return null
    }

    const data = await resp.json()
    if (resp.status === 200) {
        return {status: resp.status, data: data}
    }

    return { status: resp.status, text: resp.statusText, message: data }
}