
// enum for game types
export enum gameType {
    Game_1 = 0,
    Game_2 = 1,
    Game_3 = 2,
}

// create list of games with id, name and description
export const gameList: {[key: number]: {type: gameType, name: string, description: string}} = {
    0: {type: gameType.Game_1, name: "Claim circles", description: "The player with the most circles wins by claiming and holding circles on the map."},
    1: {type: gameType.Game_2, name: "Game 2", description: "Game 2 - Not yet implemented."},
    2: {type: gameType.Game_3, name: "Game 3", description: "Game 3 - Not yet implemented."},
}

export const colorList: {[key: number]: {color: string, name: string}} = {
    0: {color: "#FF0000", name: "Red"},
    1: {color: "#00FF00", name: "Green"},
    2: {color: "#0000FF", name: "Blue"},
    3: {color: "#FFFF00", name: "Yellow"},
    4: {color: "#FF00FF", name: "Magenta"},
    5: {color: "#00FFFF", name: "Cyan"},
    6: {color: "#000000", name: "Black"},
    7: {color: "#FFFFFF", name: "White"},
    8: {color: "#808080", name: "Gray"},
    9: {color: "#800000", name: "Maroon"},
    10: {color: "#808000", name: "Olive"},
    11: {color: "#008000", name: "Green"},
    12: {color: "#800080", name: "Purple"},
    13: {color: "#008080", name: "Teal"},
    14: {color: "#000080", name: "Navy"},
}

export const defaultColor = {color: "#123456", name: "Default"}
export const defaultLocation = {lat: 52.093, lng: 5.119}

export interface GameData {
    id: number
    name: string
    game_type: number
    active_users: string
    created_at: string
    date_edit: string
    owner_name: string
    settings: GameDataSettings
}

export interface GameDataSettings {
    active: boolean
    start_time: string
    enable_start_time: boolean
    stop_time: string
    enable_stop_time: boolean
    settings: any
}
