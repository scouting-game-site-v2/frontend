import React from 'react';
import * as notify from '@/app/notify';

export var currentLocation: any = null;
export var loadingLocation: boolean = true;
export var locationTimestamp: number = 0;
export var locationAge: number = -1;
export var locationDistance: number = -1;
export var locationSpeed: number = 0;
var setCurrentLocation: React.Dispatch<React.SetStateAction<null | any>>;
var setLoading: React.Dispatch<React.SetStateAction<boolean>>;
var setLocationTimestamp: React.Dispatch<React.SetStateAction<number>>;
var setLocationAge: React.Dispatch<React.SetStateAction<number>>;
var setlocationDistance: React.Dispatch<React.SetStateAction<number>>;
var setLocationSpeed: React.Dispatch<React.SetStateAction<number>>;

export function GetGeolocation(){
    [currentLocation, setCurrentLocation] = React.useState<null |  any>(null);
    [loadingLocation, setLoading] = React.useState(true);
    [locationTimestamp, setLocationTimestamp] = React.useState(0);
    [locationAge, setLocationAge] = React.useState(-1);
    [locationDistance, setlocationDistance] = React.useState(-1);
    [locationSpeed, setLocationSpeed] = React.useState(0);
    const [error, setError] = React.useState<null | string>(null);
    var oldTimeStamp = React.useRef(0);
    var oldLocation = React.useRef<any>(null);

    function calculateDistance(lat1: number, lon1: number,lat2: number,lon2: number){
        const toRadian = (n: number) => (n * Math.PI) / 180

        let R = 6371e3;  // km
        let x1 = lat2 - lat1;
        let dLat = toRadian(x1);
        let x2 = lon2 - lon1;
        let dLon = toRadian(x2);
        let a =
            Math.sin(dLat / 2) * Math.sin(dLat / 2) +
            Math.cos(toRadian(lat1)) * Math.cos(toRadian(lat2)) * 
            Math.sin(dLon / 2) * Math.sin(dLon / 2);
        let c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        let d = R * c;
        return d
    }

    
    const onChange = React.useCallback(({ coords }: any) => {
        setCurrentLocation({lat: coords.latitude, lng: coords.longitude});
        const timestamp = Date.now();
        if (locationTimestamp != 0 && oldTimeStamp.current != 0){
            const distance = calculateDistance(coords.latitude, coords.longitude, oldLocation.current.latitude, oldLocation.current.longitude);
            const age = timestamp - oldTimeStamp.current;
            setLocationAge(age);
            setlocationDistance(distance);
            setLocationSpeed(distance / (age / 1000));
        }
        setLocationTimestamp(timestamp);
        oldTimeStamp.current = timestamp;
        oldLocation.current = coords;
        // console.log(coords);	
        setLoading(false);
    }, []);

    const onError = (error: any) => {
        setError(error.message);
        // console.error(error);
        console.log(error);
        if (error.code === 1){
            notify.ShowNotification(notify.location.Top, notify.Severity.Error, "The page didn't have the permission to get your geolocation. Please allow the geolocation in your settings!", "No geolocation permission");
        }else if (error.code === 2){
            notify.ShowNotification(notify.location.Top, notify.Severity.Warning, "Failed to fetch your geolocation, please check your network", "Network error");
        } else if (error.code === 3){
            notify.ShowNotification(notify.location.Bottom, notify.Severity.Warning, "Geolocation information was not obtained in the allowed time, please wait or refresh", "Geolocation timeout", 12000);
        } 
    };

    // get the live location
    React.useEffect(() => {
        const geo = navigator.geolocation;
        if (!geo) {
            setError('Geolocation is not supported');
            notify.ShowNotification(notify.location.Top, notify.Severity.Error, "Geolocation is not supported, use a other browser!", "Geolocation is not supported");
            return;
        }
        const watcher = geo.watchPosition(onChange, onError, { enableHighAccuracy: true, maximumAge: 5000, timeout: 20000});
        return () => geo.clearWatch(watcher);
    }, [setError, onChange]);
}