import { useState, useEffect } from 'react';
import useWebSocket, { ReadyState } from 'react-use-websocket';

interface WebSocketMessage {
    action: string;
    data: any;
}

interface JoinGame {
    game_id: number;
    color: string;
    game_username: string;
}

export const ConnectWebsocket = (url: string) => {
    const [socketUrl, setSocketUrl] = useState(url);
    const { sendMessage, lastMessage, readyState } = useWebSocket(socketUrl);
    const connectionStatus = {
        [ReadyState.CONNECTING]: 'Connecting',
        [ReadyState.OPEN]: 'Connected',
        [ReadyState.CLOSING]: 'Closing',
        [ReadyState.CLOSED]: 'Closed',
        [ReadyState.UNINSTANTIATED]: 'Uninstantiated',
    }[readyState];

    useEffect(() => {
        console.log('Current websocket state: ', connectionStatus);
    }, [connectionStatus]);

    return { sendMessage, lastMessage, readyState };
};
