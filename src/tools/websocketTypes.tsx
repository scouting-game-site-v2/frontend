export interface WebSocketMessage {
    action: string;
    data: any;
}

export interface JoinGame {
    game_id: Number;
    color: string;
    game_username: string;
}