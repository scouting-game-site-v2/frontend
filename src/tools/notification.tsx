import React from 'react';
import { useSearchParams } from 'next/navigation';
import * as notify from '@/app/notify';



export async function QueryNotifications(searchParams: URLSearchParams) {
    notify.CloseNotification();
    if (searchParams.get('message') != null) {
        var severity = searchParams.get('severity') as notify.Severity;
        var location = searchParams.get('location') as notify.location;
        var title = searchParams.get('title') as string;
        var message = searchParams.get('message') as string;
        if (title == null) {
            title = searchParams.get('severity') as string;
        }
        if (severity == null || location == null || message == null) {
            severity = notify.Severity.Error;
            location = notify.location.Top;
            title = "Error";
            message = "Something went wrong with the notification";
        }
        notify.ShowNotification(location, severity, message, title);

        // replace url
        window.history.replaceState({}, "", window.location.pathname);
    }
}